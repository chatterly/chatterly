﻿using Chatterly.Core.Commands;
using Chatterly.Server.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Chatterly.Server.Tests
{
    [TestClass]
    public class ChatServerTest
    {
        readonly IPEndPoint EndPoint = new IPEndPoint(IPAddress.Loopback, Port);
        readonly static int Port = 7000;
        static ChatServer Server;
        readonly TcpClient FirstClient = new TcpClient();

        [ClassInitialize]
        public static void InitializeTest(TestContext context)
        {
            (new ServerContext()).EnsureCreated();

            Server = new ChatServer(Port);

            Assert.AreEqual(IPAddress.Any, Server.EndPoint.Address, "Expected IP Addresds \"Any\".");
            Assert.AreEqual(Port, Server.EndPoint.Port, "Expected configured port.");
            Assert.IsFalse(Server.IsRunning, "Expected to not be running.");

            Server.Start();

            Assert.IsTrue(Server.IsRunning, "Expected to be running.");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Server.Stop();
            Assert.IsFalse(Server.IsRunning, "Expected to not be running.");
        }

        [TestMethod]
        public void ChatServer()
        {
            var clientCount = Server.Clients.Count;

            FirstClient.Connect(EndPoint);

            Thread.Sleep(100);

            var session = Server.Clients.Single().Key;

            var clients = Enumerable.Range(0, 5).Select(x => new TcpClient()).ToList();
            clients.ForEach(client => client.Connect(EndPoint));

            clients.Insert(0, FirstClient);

            Thread.Sleep(100);

            Assert.AreEqual(clientCount + clients.Count, Server.Clients.Count, "Expected a connected client.");

            var response = new Response { Message = "Some message" };

            Server.Broadcast(response);

            Thread.Sleep(100);

            foreach (var client in clients)
            {
                Assert.IsTrue(client.Available > 0, "Expected some available bytes.");
                client.GetStream().Read(new byte[client.Available], 0, client.Available);
                Assert.AreEqual(0, client.Available, "Expected an empty stream.");
            }

            Server.Broadcast(response, (comm) => comm.Session == session);

            Thread.Sleep(100);

            var clientsWithPending = 0;

            for (int i = 0; i < clients.Count; ++i)
            {
                if (clients[i].Available > 0)
                {
                    ++clientsWithPending;
                }
            }

            Assert.AreEqual(1, clientsWithPending, "Expected only one client with pending bytes.");

            var connected = Server.Clients.Count;

            FirstClient.Close();

            Thread.Sleep(1100);

            Assert.AreEqual(connected - 1, Server.Clients.Count, "Expected one client to have disconnected.");

            foreach (var client in Server.Clients.Values)
            {
                Assert.AreNotEqual(session, client.Session, "Expected the other clients to remain connected.");
            }
        }
    }
}
