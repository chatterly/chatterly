﻿using Chatterly.Core;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Chatterly.Core.Tests.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Threading;

namespace Chatterly.Server.Tests
{
    internal class TestHelper
    {
        public ManualResetEvent WaitEvent { get; private set; } = new ManualResetEvent(false);
        public Process ServerProcess { get; set; }
        public Communicator Comm { get; set; }

        public void InitializeServerTest(TestContext context, int port)
        {
            Setup(context, port);

            var client = new TcpClient();

            Comm = new Communicator(client);

            client.Connect(new IPEndPoint(IPAddress.Loopback, port));

            Comm.Start();
        }

        public void InitializeServerTest<T>(TestContext context, int port)
            where T : Communicator
        {
            Setup(context, port);

            var client = new TcpClient();

            var type = typeof(T);
            var constructor = type.GetConstructor(new[] { typeof(TcpClient) });
            Comm = constructor.Invoke(new object[] { client }) as Communicator;

            client.Connect(new IPEndPoint(IPAddress.Loopback, port));

            Comm.Start();

        }

        void Setup(TestContext context, int port)
        {
            string key = null;
            using (var rsa = new RSACryptoServiceProvider())
            {
                key = rsa.ToXmlString(true);
            }

            User.Encryptor = new RsaCrypto(key);

            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");

            ServerProcess = Process.Start(serverPath, string.Format("port {0} key \"{1}\"", port, key));

            Thread.Sleep(1000);

            Services.RegisterService(new CommandHandlerRegistry());

            var db = new TestDbContext();
            db.EnsureCreated();

            Services.RegisterService(new LogManager(db));
        }

        public void CleanupServerTest()
        {
            Comm.Stop();
            ServerProcess.Kill();
            ServerProcess.Close();
            ServerProcess.Dispose();
        }
    }
}
