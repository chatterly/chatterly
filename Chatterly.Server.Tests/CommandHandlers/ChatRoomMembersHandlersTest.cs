﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class ChatRoomMembersHandlersTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static ChatRoom ChatRoom { get; set; }
        static string Nickname = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7003);

            Test.WaitEvent.Reset();

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, "lol"), r =>
            {
                Test.Comm.Send(CreateChatRoom.CreateCommand(Guid.NewGuid().ToString(), "Test Chatroom"),
                    rtn =>
                    {
                        ChatRoom = (rtn as CreateChatRoom).ChatRoom;
                        Test.WaitEvent.Set();
                    });
            });

            Test.WaitEvent.WaitOne();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void GetChatRoomMembersHandler_ModifyChatRoomMembershipHandler_Tests()
        {
            Test.WaitEvent.Reset();

            GetChatRoomMembers get = null;

            Test.Comm.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomMembers;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(get);
            Assert.AreEqual(0, get.Members.Count);

            // Admin is expected to always exist
            var nickname = "Admin";

            Test.Comm.Send(ModifyChatRoomMembership.AddMember(ChatRoom.Id, nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomMembers;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, get.Members.Count);
            Assert.AreEqual(nickname, get.Members[0].NickName);

            Test.Comm.Send(ModifyChatRoomMembership.AddMember(ChatRoom.Id, Nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomMembers;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(2, get.Members.Count);
            Assert.AreEqual(nickname, get.Members[0].NickName);
            Assert.AreEqual(Nickname, get.Members[1].NickName);

            Test.Comm.Send(ModifyChatRoomMembership.RemoveMember(ChatRoom.Id, nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomMembers;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, get.Members.Count);
            Assert.AreEqual(Nickname, get.Members[0].NickName);

            Test.Comm.Send(ModifyChatRoomMembership.RemoveMember(ChatRoom.Id, Nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomMembers;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(0, get.Members.Count);
        }
    }
}
