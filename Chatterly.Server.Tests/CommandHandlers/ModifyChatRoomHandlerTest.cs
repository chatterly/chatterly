﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class ModifyChatRoomHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static ChatRoom ChatRoom { get; set; }
        static string Nickname = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7311);

            Test.WaitEvent.Reset();

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, "lol"), r =>
            {
                Test.Comm.Send(CreateChatRoom.CreateCommand(Guid.NewGuid().ToString(), "Test Chatroom"),
                rtn =>
                {
                    ChatRoom = (rtn as CreateChatRoom).ChatRoom;
                    Test.WaitEvent.Set();
                });
            });

            Test.WaitEvent.WaitOne();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void ModifyChatRoomHandler_Test()
        {
            Test.WaitEvent.Reset();

            GetChatRoom get = null;

            Test.Comm.Send(GetChatRoom.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(ChatRoom.Id, get.ChatRoom.Id);
            Assert.AreEqual(ChatRoom.Name, get.ChatRoom.Name);
            Assert.AreEqual(ChatRoom.Description, get.ChatRoom.Description);
            Assert.AreEqual(ChatRoom.IsPrivate, get.ChatRoom.IsPrivate);
            Assert.AreEqual(ChatRoom.Created, get.ChatRoom.Created);

            ChatRoom.Name = "changed name";
            ChatRoom.Description = "changed desc";
            ChatRoom.IsPrivate = true;

            Test.Comm.Send(new ModifyChatRoom { ChatRoom = ChatRoom }, rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoom.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(ChatRoom.Id, get.ChatRoom.Id);
            Assert.AreEqual(ChatRoom.Name, get.ChatRoom.Name);
            Assert.AreEqual(ChatRoom.Description, get.ChatRoom.Description);
            Assert.AreEqual(ChatRoom.IsPrivate, get.ChatRoom.IsPrivate);
            Assert.AreEqual(ChatRoom.Created, get.ChatRoom.Created);
        }
    }
}
