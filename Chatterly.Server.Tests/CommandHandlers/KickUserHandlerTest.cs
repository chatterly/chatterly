﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class KickUserHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static Communicator OtherComm { get; set; }
        static ChatRoom ChatRoom { get; set; }
        static string OwnerNickname = Guid.NewGuid().ToString();
        static string Nickname = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            var port = 7100;

            Test.InitializeServerTest(context, port);

            Test.WaitEvent.Reset();

            var client = new TcpClient();

            OtherComm = new Communicator(client);

            client.Connect(new IPEndPoint(IPAddress.Loopback, port));

            OtherComm.Start();

            Test.Comm.Send(RegisterUser.CreateCommand(OwnerNickname, "lol"), r =>
            {
                Test.Comm.Send(CreateChatRoom.CreateCommand(Guid.NewGuid().ToString(), "Test Chatroom"),
                rtn =>
                {
                    ChatRoom = (rtn as CreateChatRoom).ChatRoom;
                    OtherComm.Send(RegisterUser.CreateCommand(Nickname, "lol"), r1 =>
                    {
                        OtherComm.Send(JoinChatRoom.CreateCommand(ChatRoom.Id), r2 =>
                        {
                            Test.Comm.Send(AuthenticateUser.CreateCommand(OwnerNickname, "lol"), r3 => Test.WaitEvent.Set());
                        });
                    });
                });
            });

            Test.WaitEvent.WaitOne();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            OtherComm.Stop();
            Test.CleanupServerTest();
        }

        [TestMethod]
        [Timeout(10000)]
        public void KickUserHandler_Test()
        {
            Test.WaitEvent.Reset();

            ICollection<User> users = null;

            Test.Comm.Send(GetChatRoom.CreateCommand(ChatRoom.Id), rtn =>
            {
                users = (rtn as GetChatRoom).Users;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, users.Count);

            Test.Comm.Send(new KickUser { ChatRoomId = ChatRoom.Id, Nickname = Nickname }, rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoom.CreateCommand(ChatRoom.Id), rtn =>
            {
                users = (rtn as GetChatRoom).Users;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(0, users.Count);
        }
    }
}
