﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class CreateChatRoomHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7003);
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void CreateChatRoomHandler_Test()
        {
            Test.WaitEvent.Reset();

            var name = Guid.NewGuid().ToString();
            var desc = "Test chatroom!";

            var create = CreateChatRoom.CreateCommand(name, desc);

            CreateChatRoom createResponse = null;

            Test.Comm.Send(create, rtn =>
            {
                createResponse = rtn as CreateChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(createResponse, "Expected a response object.");
            Assert.IsFalse(createResponse.IsCreated, "Expected chatroom to not be created.");

            var nick = Guid.NewGuid().ToString();
            var pass = "lol";

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(nick, pass), rtn =>
            {
                reg = rtn as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsTrue(reg.IsRegistered, "Expected to register new user.");

            Test.Comm.Send(create, rtn =>
            {
                createResponse = rtn as CreateChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(createResponse, "Expected a response object.");
            Assert.IsTrue(createResponse.IsCreated, "Expected chatroom to be created.");

            GetChatRooms getResponse = null;

            Test.Comm.Send(new GetChatRooms(), rtn =>
            {
                getResponse = rtn as GetChatRooms;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(getResponse, "Expected valid GetChatRooms object response.");

            var newChatroom = getResponse.ChatRooms.SingleOrDefault(c => c.ChatRoom.Name == name)?.ChatRoom;

            Assert.IsNotNull(newChatroom, "Expected a valid ChatRoom object.");
            Assert.AreEqual(newChatroom.Name, name, "Name expected to be the same.");
            Assert.AreEqual(newChatroom.Description, desc, "Description expected to be the same.");
            Assert.IsTrue(DateTime.UtcNow.Subtract(newChatroom.Created).TotalSeconds < 1,
                "Expected chatroom to have been created very recently.");

            GetChatRoomStaff staffResonse = null;

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(newChatroom.Id), rtn =>
            {
                staffResonse = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(staffResonse, "Expected valid IsChatRoomStaff response object.");
            Assert.AreEqual(1, staffResonse.Staff.Count, "Expected only one chat room staff member.");

            var staff = staffResonse.Staff.FirstOrDefault();

            Assert.IsNotNull(staff, "Expected a valid ChatRoomStaff object from the staff response.");
            Assert.IsNotNull(staff.User, "Expected a valid user object from the staff response.");
            Assert.AreEqual(nick, staff.User.NickName, "Expected to be about the same user.");
            Assert.AreEqual(ChatRoomRank.Owner, staff.Rank, "Expected user to be owner.");

            Test.Comm.Send(create, rtn =>
            {
                createResponse = rtn as CreateChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsNotNull(createResponse, "Expected a response object.");
            Assert.IsFalse(createResponse.IsCreated, "Expected chatroom to not be created.");
        }
    }
}
