﻿using Chatterly.Core.Commands;
using Chatterly.Server.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class JoinChatRoomHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7000);
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }
        [TestMethod, Timeout(3000)]
        public void JoinChatRoomHandler_Test()
        {
            var nickname = Guid.NewGuid().ToString();
            var password = "lol";

            var db = new ServerContext();
            db.EnsureCreated();

            var chatRoomId = db.ChatRooms.First().Id;

            Test.WaitEvent.Reset();

            JoinChatRoom join = null;

            Test.Comm.Send(JoinChatRoom.CreateCommand(chatRoomId), cmd =>
            {
                join = cmd as JoinChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsFalse(join.Joined, "Expected to have not joined.");

            Test.WaitEvent.Reset();

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(nickname, password), cmd =>
            {
                reg = cmd as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to have registered.");

            Test.WaitEvent.Reset();

            Test.Comm.Send(JoinChatRoom.CreateCommand(chatRoomId), cmd =>
            {
                join = cmd as JoinChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(join.Joined, "Expected to have joined.");
        }
    }
}
