﻿using Chatterly.Core.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class AuthenticateUserHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static string Nickname { get; set; }
        static string Password { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7003);

            Nickname = Guid.NewGuid().ToString();
            Password = "lol";

            Test.WaitEvent.Reset();

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, Password), cmd =>
            {
                reg = cmd as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to register new user.");

            Test.Comm.Stop();

            Thread.Sleep(100);

            Test.Comm.Start();

            Thread.Sleep(100);

            Assert.IsTrue(Test.Comm.Poll(), "Expected the connection to be made.");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void AuthenticateUserHandler_Test()
        {
            Test.WaitEvent.Reset();

            AuthenticateUser auth = null;

            var NewNickname = Guid.NewGuid().ToString();

            Test.Comm.Send(AuthenticateUser.CreateCommand(NewNickname, "lol"), cmd =>
            {
                auth = cmd as AuthenticateUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsFalse(auth.IsAuthenticated, "Expected to not be authenticated.");
            Assert.AreEqual(NewNickname, auth.User.NickName, "Expected same nickname.");

            Test.WaitEvent.Reset();

            Test.Comm.Send(AuthenticateUser.CreateCommand(Nickname, Guid.NewGuid().ToString()), cmd =>
            {
                auth = cmd as AuthenticateUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsFalse(auth.IsAuthenticated, "Expected to not be authenticated.");
            Assert.AreEqual(Nickname, auth.User.NickName, "Expected same nickname.");

            Test.WaitEvent.Reset();

            Test.Comm.Send(AuthenticateUser.CreateCommand(Nickname, Password), cmd =>
            {
                auth = cmd as AuthenticateUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(auth.IsAuthenticated, "Expected to not be authenticated.");
            Assert.AreEqual(Nickname, auth.User.NickName, "Expected same nickname.");
            Assert.IsTrue(auth.User.Id > 0, "Expected some user ID");
        }
    }
}
