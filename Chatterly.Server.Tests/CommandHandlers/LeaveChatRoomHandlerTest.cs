﻿using Chatterly.Core.Commands;
using Chatterly.Server.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class LeaveChatRoomHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7002);
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }
        [TestMethod, Timeout(10000)]
        public void LeaveChatRoomHandler_Test()
        {
            var nickname = Guid.NewGuid().ToString();
            var password = "lol";

            var db = new ServerContext();
            db.EnsureCreated();

            var chatRoomId = db.ChatRooms.First().Id;

            Test.WaitEvent.Reset();

            LeaveChatRoom leave = null;

            Test.Comm.Send(LeaveChatRoom.CreateCommand(chatRoomId), cmd =>
            {
                leave = cmd as LeaveChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsFalse(leave.Left, "Expected to have not left.");

            Test.WaitEvent.Reset();

            RegisterUser reg = null;
            JoinChatRoom join = null;


            Test.Comm.Send(RegisterUser.CreateCommand(nickname, password), cmd =>
            {
                reg = cmd as RegisterUser;

                Test.Comm.Send(JoinChatRoom.CreateCommand(chatRoomId), cmd1 =>
                {
                    join = cmd1 as JoinChatRoom;
                    Test.WaitEvent.Set();
                });
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to have registered.");
            Assert.IsTrue(join.Joined, "Expected to have joined.");

            Test.WaitEvent.Reset();

            Test.Comm.Send(LeaveChatRoom.CreateCommand(chatRoomId), cmd =>
            {
                leave = cmd as LeaveChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(leave.Left, "Expected to have left.");
        }
    }
}
