﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class BansHandlerTests
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static ChatRoom ChatRoom { get; set; }
        static string Nickname = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7011);

            Test.WaitEvent.Reset();

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, "lol"), r =>
            {
                Test.Comm.Send(RegisterUser.CreateCommand(Guid.NewGuid().ToString(), "lol"), r1 =>
                {
                    Test.Comm.Send(CreateChatRoom.CreateCommand(Guid.NewGuid().ToString(), "Test Chatroom"),
                    rtn =>
                    {
                        ChatRoom = (rtn as CreateChatRoom).ChatRoom;
                        Test.WaitEvent.Set();
                    });
                });
            });

            Test.WaitEvent.WaitOne();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod]
        [Timeout(10000)]
        public void GetChatRoomBansHandler_ModifyBansHandler_Test()
        {
            Test.WaitEvent.Reset();

            GetChatRoomBans get = null;

            Test.Comm.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomBans;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(get);
            Assert.AreEqual(0, get.Bans.Count);

            // Admin is expected to always exist
            var nickname = "Admin";

            Test.Comm.Send(ModifyBan.BanUser(ChatRoom.Id, nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomBans;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, get.Bans.Count);
            Assert.AreEqual(nickname, get.Bans[0].User.NickName);

            Test.Comm.Send(ModifyBan.BanUser(ChatRoom.Id, Nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomBans;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(2, get.Bans.Count);
            Assert.AreEqual(nickname, get.Bans[0].User.NickName);
            Assert.AreEqual(Nickname, get.Bans[1].User.NickName);

            Test.Comm.Send(ModifyBan.UnbanUser(ChatRoom.Id, nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomBans;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, get.Bans.Count);
            Assert.AreEqual(Nickname, get.Bans[0].User.NickName);

            Test.Comm.Send(ModifyBan.UnbanUser(ChatRoom.Id, Nickname), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomBans;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(0, get.Bans.Count);
        }
    }
}
