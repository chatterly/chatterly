﻿using Chatterly.Core.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class GetChatRoomsHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static string Nickname { get; set; }
        static string Password { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7003);

            Nickname = Guid.NewGuid().ToString();
            Password = "lol";

            Test.WaitEvent.Reset();

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, Password), cmd =>
            {
                reg = cmd as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to register new user.");

            Test.Comm.Stop();

            Thread.Sleep(100);

            Test.Comm.Start();

            Thread.Sleep(100);

            Assert.IsTrue(Test.Comm.Poll(), "Expected the connection to be made.");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void GetChatRoomsHandler_Test()
        {
            Test.WaitEvent.Reset();

            GetChatRooms GetRooms = null;

            //This command will be processed by the handler in the server
            Test.Comm.Send(new GetChatRooms(), cmd =>
            {
                GetRooms = cmd as GetChatRooms;
                Test.WaitEvent.Set();
            });
            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            //Check to see if ChatRooms data was fetched successfully
            Assert.IsNotNull(GetRooms.ChatRooms);

        }
    }
}
