﻿using Chatterly.Core.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class GetChatRoomHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static string Nickname { get; set; }
        static string Password { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7003);

            Nickname = Guid.NewGuid().ToString();
            Password = "lol";

            Test.WaitEvent.Reset();

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(Nickname, Password), cmd =>
            {
                reg = cmd as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to register new user.");

            Test.Comm.Stop();

            Thread.Sleep(100);

            Test.Comm.Start();

            Thread.Sleep(100);

            Assert.IsTrue(Test.Comm.Poll(), "Expected the connection to be made.");

        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void GetChatRoomHandler_Test()
        {
            Test.WaitEvent.Reset();

            GetChatRoom GetChat = null;

            //This command will be processed by the handler in the server
            Test.Comm.Send(GetChatRoom.CreateCommand(1), cmd =>
            {
                GetChat = cmd as GetChatRoom;
                Test.WaitEvent.Set();
            });
            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            //Check to see if ChatRoom data was fetched successfully
            Assert.IsNotNull(GetChat);
            Assert.IsNotNull(GetChat.ChatRoom);
            Assert.IsNotNull(GetChat.Users);

            //Send a request for a room that doesn't exist
            Test.Comm.Send(GetChatRoom.CreateCommand(9999), cmd =>
            {
                GetChat = cmd as GetChatRoom;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            //Room 9999 probably doesn't exist, so we should get back a null for chatroom
            Assert.IsNull(GetChat.ChatRoom);

        }
    }
}
