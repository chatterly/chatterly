﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class ChatRoomStaffHandlersTest
    {
        static TestHelper Test { get; set; } = new TestHelper();
        static ChatRoom ChatRoom { get; set; }
        static string OwnerNickname = Guid.NewGuid().ToString();
        static string NewNickname = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7010);

            Test.WaitEvent.Reset();

            Test.Comm.Send(RegisterUser.CreateCommand(NewNickname, "lol"), r =>
            {
                Test.Comm.Send(RegisterUser.CreateCommand(OwnerNickname, "lol"), r1 =>
                {
                    Test.Comm.Send(CreateChatRoom.CreateCommand(Guid.NewGuid().ToString(), "Test Chatroom"),
                    rtn =>
                    {
                        ChatRoom = (rtn as CreateChatRoom).ChatRoom;
                        Test.WaitEvent.Set();
                    });
                });
            });

            Test.WaitEvent.WaitOne();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod]
        public void GetChatRoomStaffHandler_ModifyChatRoomStaffHandler_Test()
        {
            Test.WaitEvent.Reset();

            GetChatRoomStaff get = null;

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.IsNotNull(get);
            Assert.AreEqual(1, get.Staff.Count);
            Assert.AreEqual(OwnerNickname, get.Staff[0].User.NickName);
            Assert.AreEqual(ChatRoomRank.Owner, get.Staff[0].Rank);

            // Admin is expected to always exist
            var nickname = "Admin";

            var modRank = ChatRoomRank.Mod;
            var adminRank = ChatRoomRank.Mod;

            Test.Comm.Send(ModifyChatRoomStaff.CreateCommand(ChatRoom.Id, nickname, modRank), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(2, get.Staff.Count);
            Assert.AreEqual(nickname, get.Staff[1].User.NickName);
            Assert.AreEqual(modRank, get.Staff[1].Rank);
            Assert.AreEqual(nickname, get.Staff[1].User.NickName);
            Assert.AreEqual(modRank, get.Staff[1].Rank);

            Test.Comm.Send(ModifyChatRoomStaff.CreateCommand(ChatRoom.Id, NewNickname, adminRank),
                rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(3, get.Staff.Count);
            Assert.AreEqual(OwnerNickname, get.Staff[0].User.NickName);
            Assert.AreEqual(ChatRoomRank.Owner, get.Staff[0].Rank);
            Assert.AreEqual(nickname, get.Staff[1].User.NickName);
            Assert.AreEqual(modRank, get.Staff[1].Rank);
            Assert.AreEqual(NewNickname, get.Staff[2].User.NickName);
            Assert.AreEqual(adminRank, get.Staff[2].Rank);

            Test.Comm.Send(ModifyChatRoomStaff.CreateCommand(ChatRoom.Id, NewNickname, null), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(2, get.Staff.Count);
            Assert.AreEqual(OwnerNickname, get.Staff[0].User.NickName);
            Assert.AreEqual(ChatRoomRank.Owner, get.Staff[0].Rank);
            Assert.AreEqual(nickname, get.Staff[1].User.NickName);
            Assert.AreEqual(modRank, get.Staff[1].Rank);

            Test.Comm.Send(ModifyChatRoomStaff.CreateCommand(ChatRoom.Id, nickname, null), rtn => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Test.Comm.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), rtn =>
            {
                get = rtn as GetChatRoomStaff;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Assert.AreEqual(1, get.Staff.Count);
            Assert.AreEqual(OwnerNickname, get.Staff[0].User.NickName);
            Assert.AreEqual(ChatRoomRank.Owner, get.Staff[0].Rank);
        }
    }
}
