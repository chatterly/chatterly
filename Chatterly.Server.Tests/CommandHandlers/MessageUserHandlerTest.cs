﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class MessageUserHandlerTest
    {
        /// <summary>
        /// client communicator.
        /// </summary>
        public class ClientCommunicator : Communicator
        {
            /// <summary>
            /// Constructs the communicator.
            /// </summary>
            /// <param name="client"></param>
            public ClientCommunicator(TcpClient client)
                : base(client)
            {
            }

            /// <summary>
            /// Registers a <see cref="ServerResponseNotifier"/> to handle event-driven network interaction.
            /// </summary>
            /// <param name="command"></param>
            protected override void HandleCommand(ICommand command)
            {
                base.HandleCommand(command);

                Services.GetService<ServerResponseNotifier>()?.NotifyServerResponse(command);
            }
        }

        static TestHelper Test { get; set; } = new TestHelper();
        static int UserId { get; set; }
        MessageUser Message { get; set; }

        [ClassInitialize]
        [Timeout(20000)]
        public static void Initialize(TestContext context)
        {
            Console.WriteLine("Registering server response notifier.");

            Services.RegisterService(new ServerResponseNotifier());

            Test.InitializeServerTest<ClientCommunicator>(context, 7301);

            Test.WaitEvent.Reset();

            var nick = Guid.NewGuid().ToString();
            var pass = "lol";

            Console.WriteLine("Sending register.");

            Test.Comm.Send(RegisterUser.CreateCommand(nick, pass), r => Test.WaitEvent.Set());

            Test.WaitEvent.WaitOne();
            Test.WaitEvent.Reset();

            Console.WriteLine("Sending authentication to get user ID.");

            Test.Comm.Send(AuthenticateUser.CreateCommand(nick, pass), r =>
            {
                UserId = (r as AuthenticateUser).User.Id;

                Debug.WriteLine("Received User ID: " + UserId);

                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Console.WriteLine("Done with init.");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod]
        [Timeout(5000)]
        public void MessageUserHandler_Test()
        {
            Console.WriteLine("Starting test...");

            Test.WaitEvent.Reset();

            var testString = "Test Message";

            Console.WriteLine("Subscribing to MessageUser");

            Services.GetService<ServerResponseNotifier>().Subscribe<MessageUser>(Response);

            Console.WriteLine("Sending message");

            Test.Comm.Send(MessageUser.CreateCommand(UserId, testString));

            Test.WaitEvent.WaitOne();

            Console.WriteLine("Message receive signal received!");

            Assert.AreEqual(UserId, Message.Message.User.Id);
            Assert.AreEqual(UserId, Message.RecipientUserId);
            Assert.AreEqual(testString, Message.Message.Content);
        }

        void Response(object sender, SeverResponseEventArgs e)
        {
            Message = e.Command as MessageUser;
            Console.WriteLine(Message.Message.Content);
            Test.WaitEvent.Set();
        }
    }
}
