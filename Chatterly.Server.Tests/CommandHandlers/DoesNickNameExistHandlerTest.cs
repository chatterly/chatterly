﻿using Chatterly.Core.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.Server.Tests.CommandHandlers
{
    [TestClass]
    public class DoesNickNameExistHandlerTest
    {
        static TestHelper Test { get; set; } = new TestHelper();

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Test.InitializeServerTest(context, 7001);
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Test.CleanupServerTest();
        }

        [TestMethod, Timeout(10000)]
        public void DoesNickNameExistHandler_Test()
        {
            var nickname = Guid.NewGuid().ToString();
            var password = "lol";

            Test.WaitEvent.Reset();

            DoesNickNameExist does = null;

            Test.Comm.Send(DoesNickNameExist.CreateCommand(nickname), cmd =>
            {
                does = cmd as DoesNickNameExist;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.AreEqual(nickname, does.NickName, "nickname should be same.");
            Assert.IsFalse(does.NickNameExists, "Expected nickname to not exist.");

            Test.WaitEvent.Reset();

            RegisterUser reg = null;

            Test.Comm.Send(RegisterUser.CreateCommand(nickname, password), cmd =>
            {
                reg = cmd as RegisterUser;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.IsTrue(reg.IsRegistered, "Expected to register new user.");

            Test.WaitEvent.Reset();

            Test.Comm.Send(DoesNickNameExist.CreateCommand(nickname), cmd =>
            {
                does = cmd as DoesNickNameExist;
                Test.WaitEvent.Set();
            });

            Test.WaitEvent.WaitOne();

            Assert.AreEqual(nickname, does.NickName, "nickname should be same.");
            Assert.IsTrue(does.NickNameExists, "Expected nickname to exist.");
        }
    }
}
