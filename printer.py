# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 00:01:00 2016

@author: john
"""
from pygments import highlight
from pygments.lexers import CSharpLexer
from pygments.formatters import HtmlFormatter
import os, re

root = r'C:\Users\john\Desktop\Chatterly\chatterly'

bad_dirs = ['\\obj', '\\bin', '\\properties', '\\assemblymerger']

def convert_dir(root, bad_dirs):
    namespace_regex = 'namespace (.+)'
    
    with open('source.htm', 'w') as f:
        f.write('<html><head><style text="text/css">')
        f.write(HtmlFormatter().get_style_defs('.highlight'))
        f.write('span { font-size: 8px }')
        f.write('</style></head><body>')
        for subdir, dirs, files in os.walk(root):
            for file in files:
                if not any(x in subdir.lower() for x in bad_dirs):
                    split_file = os.path.splitext(file)
                    extension = split_file[1]
                    if extension == '.cs':
                        file_path = os.path.join(subdir, file)
                        with open(file_path) as s:
                            source = s.read()
                            m = re.search(namespace_regex, source)
                            name = '{0}.{1}'.format(m.group(1), split_file[0])
                            f.write('<h3>{0}</h3>'.format(name))
                            f.write(convert(source))
        f.write('</body></html>')
                        
                        

def convert(source):
    return highlight(source, CSharpLexer(unicodelevel='none'),
                     HtmlFormatter())

if __name__ == '__main__':
    convert_dir(root, bad_dirs)