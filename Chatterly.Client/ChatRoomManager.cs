﻿using Chatterly.Core.Models;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Chatterly.Client
{
    /// <summary>
    /// Delegate to respond to a chat room update event.
    /// </summary>
    /// <param name="room"></param>
    public delegate void ChatRoomUpdate(ChatRoomManager.Room room);

    /// <summary>
    /// Object to manage chat rooms.
    /// May not be used...
    /// </summary>
    public class ChatRoomManager
    {
        /// <summary>
        /// Notifiers subscribers of chat room updates.
        /// </summary>
        public event ChatRoomUpdate Updated;

        /// <summary>
        /// Notify of an update to the given room.
        /// </summary>
        /// <param name="room"></param>
        void Notify(Room room)
        {
            Updated?.Invoke(room);
        }

        /// <summary>
        /// Represents a chat room.
        /// </summary>
        public class Room
        {
            /// <summary>
            /// Chat room data.
            /// </summary>
            public ChatRoom ChatRoom { get; set; }

            /// <summary>
            /// User count.
            /// </summary>
            public int UserCount { get; set; }

            /// <summary>
            /// Chat room messages.
            /// </summary>
            public ObservableCollection<Message> Messages { get; set; } = new ObservableCollection<Message>();
        }

        /// <summary>
        /// Mapping of chat room IDs to <see cref="Room"/> objects.
        /// </summary>
        ConcurrentDictionary<int, Room> Rooms { get; set; } = new ConcurrentDictionary<int, Room>();

        /// <summary>
        /// Ensure a <see cref="Room"/> object exists in <see cref="Rooms"/>.
        /// </summary>
        /// <param name="chatRoomId"></param>
        void EnsureChatRoom(int chatRoomId)
        {
            if (!Rooms.ContainsKey(chatRoomId))
            {
                Rooms.TryAdd(chatRoomId, new Room());
            }
        }

        /// <summary>
        /// Update the given chat room.
        /// </summary>
        /// <param name="room"></param>
        /// <param name="userCount"></param>
        public void UpdateRoom(ChatRoom room, int userCount = 0)
        {
            EnsureChatRoom(room.Id);
            Rooms[room.Id].ChatRoom = room;
            Rooms[room.Id].UserCount = userCount;
            Notify(Rooms[room.Id]);
        }

        /// <summary>
        /// Get all existing rooms.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Room> GetRooms()
        {
            return Rooms.Values;
        }

        /// <summary>
        /// Add a message to the given chat room.
        /// </summary>
        /// <param name="chatRoomId"></param>
        /// <param name="msg"></param>
        public void AddMessage(int chatRoomId, Message msg)
        {
            EnsureChatRoom(chatRoomId);

            Rooms[chatRoomId].Messages.Add(msg);

            Notify(Rooms[chatRoomId]);
        }

        /// <summary>
        /// Get the room with the given ID.
        /// </summary>
        /// <param name="chatRoomId"></param>
        /// <returns></returns>
        public Room GetRoom(int chatRoomId)
        {
            EnsureChatRoom(chatRoomId);

            return Rooms[chatRoomId];
        }

        /// <summary>
        /// Get all messages in the given room.
        /// </summary>
        /// <param name="chatRoomId"></param>
        /// <returns></returns>
        public ObservableCollection<Message> GetMessages(int chatRoomId)
        {
            return GetRoom(chatRoomId).Messages;
        }
    }
}
