﻿using Chatterly.Core;
using Chatterly.Core.Protocol;
using System.Net.Sockets;

namespace Chatterly.Client.Protocol
{
    /// <summary>
    /// client communicator.
    /// </summary>
    public class ClientCommunicator : Communicator
    {
        /// <summary>
        /// Constructs the communicator.
        /// </summary>
        /// <param name="client"></param>
        public ClientCommunicator(TcpClient client)
            : base(client)
        { }

        /// <summary>
        /// Registers a <see cref="ServerResponseNotifier"/> to handle event-driven network interaction.
        /// </summary>
        /// <param name="command"></param>
        protected override void HandleCommand(ICommand command)
        {
            base.HandleCommand(command);

            Services.GetService<ServerResponseNotifier>()?.NotifyServerResponse(command);
        }
    }
}
