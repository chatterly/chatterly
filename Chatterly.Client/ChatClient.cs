﻿using Chatterly.Client.Database;
using Chatterly.Client.Protocol;
using Chatterly.Core;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;
using System.Net.Sockets;

namespace Chatterly.Client
{
    /// <summary>
    /// Object to manage the client's network communication.
    /// </summary>
    public class ChatClient
    {
        /// <summary>
        /// If non-null, is the user we're authenticated as.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// If true, we are connected to the server.
        /// </summary>
        public bool IsConnected { get; private set; }

        /// <summary>
        /// Connection configuration we are using to connect to the server.
        /// </summary>
        public ConnectionConfiguration Config { get; set; }

        /// <summary>
        /// Client object with which we're communicating with the server.
        /// </summary>
        protected TcpClient Client { get; set; }

        /// <summary>
        /// Communicator to handle the network communication.
        /// </summary>
        protected Communicator Communicator { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public ChatClient() { }

        /// <summary>
        /// Construct with a server connection configuration.
        /// </summary>
        /// <param name="config">Server connection configuration.</param>
        public ChatClient(ConnectionConfiguration config)
        {
            Config = config;
        }

        /// <summary>
        /// Start the network communication.
        /// </summary>
        /// <returns>True if the we were successful.</returns>
        public bool Start()
        {
            if (Config == null)
            {
                throw new InvalidOperationException(
                    "A connection configuration must be provided before a connection can be started.");
            }

            try
            {
                Client = new TcpClient();
                Communicator = new ClientCommunicator(Client);
                Client.Connect(Config.ToIpEndpoint());
                IsConnected = true;
                Communicator.Start();
                return true;
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("Error starting ChatClient", e);
            }

            return false;
        }

        /// <summary>
        /// Stop the network communication.
        /// </summary>
        public void Stop()
        {
            try
            {
                IsConnected = false;
                Communicator.Stop();
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("Error stopping ChatClient", e);
            }

            try
            {
                Client.GetStream().Close();
                Client.Close();
            }
            catch { }

            Communicator = null;
            Client = null;
        }

        /// <summary>
        /// Send an <see cref="ICommand"/> to the server.
        /// </summary>
        /// <param name="cmd">Command to send.</param>
        /// <param name="callback">Optional callback to execute on response.</param>
        public void Send(ICommand cmd, Action<ICommand> callback = null)
        {
            Communicator.Send(cmd, callback);
        }
    }
}
