﻿using Chatterly.Core.Protocol;
using System;
using System.Collections.Concurrent;

namespace Chatterly.Client
{
    /// <summary>
    /// Describes a server response.
    /// </summary>
    public class SeverResponseEventArgs : EventArgs
    {
        public ICommand Command { get; set; }

        public SeverResponseEventArgs(ICommand cmd)
        {
            Command = cmd;
        }
    }

    /// <summary>
    /// Handles a server response event notification.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ServerResponseEventHandler(object sender, SeverResponseEventArgs e);

    /// <summary>
    /// Manages server responses.
    /// </summary>
    public class ServerResponseNotifier
    {
        /// <summary>
        /// Server response subscriptions, maps <see cref="ICommand"/> type implementation to the callback.
        /// </summary>
        readonly ConcurrentDictionary<Type, Delegate> _Subscriptions = new ConcurrentDictionary<Type, Delegate>();

        /// <summary>
        /// Ensure that the given type implements <see cref="ICommand"/>.
        /// </summary>
        /// <param name="t"></param>
        /// <exception cref="InvalidOperationException">Must subscribe to an ICommand implementation.</exception>
        void EnforceType(Type t)
        {
            if (!(typeof(ICommand).IsAssignableFrom(t)))
            {
                throw new InvalidOperationException("Must subscribe to an ICommand implementation.");
            }
        }

        /// <summary>
        /// Subscribe to the given <see cref="ICommand"/> server response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="callback"></param>
        public void Subscribe<T>(ServerResponseEventHandler callback)
            where T : ICommand
        {
            Subscribe(typeof(T), callback);
        }

        /// <summary>
        /// Subscribe to the given <see cref="ICommand"/> server response.
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="callback"></param>
        public void Subscribe(Type cmdType, ServerResponseEventHandler callback)
        {
            EnforceType(cmdType);
            _Subscriptions.AddOrUpdate(cmdType,
                callback,
                (key, old) => (ServerResponseEventHandler)old + callback);
        }

        /// <summary>
        /// Unsubscribe to the given <see cref="ICommand"/> server response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="callback"></param>
        public void Unsubscribe<T>(ServerResponseEventHandler callback)
            where T : ICommand
        {
            Unsubscribe(typeof(T), callback);
        }

        /// <summary>
        /// Unsubscribe to the given <see cref="ICommand"/> server response.
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="callback"></param>
        public void Unsubscribe(Type cmdType, ServerResponseEventHandler callback)
        {
            EnforceType(cmdType);
            _Subscriptions.AddOrUpdate(cmdType,
                callback,
                (key, old) => (ServerResponseEventHandler)old - callback);
        }

        /// <summary>
        /// Notify subscribers of a server response.
        /// </summary>
        /// <param name="cmd"></param>
        public void NotifyServerResponse(ICommand cmd)
        {
            Delegate callback;
            if (_Subscriptions.TryGetValue(cmd.GetType(), out callback))
            {
                ((ServerResponseEventHandler)callback)?.Invoke(this, new SeverResponseEventArgs(cmd));
            }
        }
    }
}
