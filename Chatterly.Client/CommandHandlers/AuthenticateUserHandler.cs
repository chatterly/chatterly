﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;

namespace Chatterly.Client.CommandHandlers
{
    /// <summary>
    /// Handles an <see cref="AuthenticateUser"/> command message from the server.
    /// </summary>
    public class AuthenticateUserHandler : ICommandHandler
    {
        /// <summary>
        /// Gives the user object to the <see cref="ChatClient"/>.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var auth = command as AuthenticateUser;
            Services.GetService<ChatClient>().User = auth.User;
        }
    }
}
