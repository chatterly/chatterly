﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;

namespace Chatterly.Client.CommandHandlers
{
    /// <summary>
    /// Handles <see cref="MessageChatRoom"/> commands from the server.
    /// </summary>
    public class MessageChatRoomHandler : ICommandHandler
    {
        /// <summary>
        /// Updates the <see cref="ChatRoomManager"/>.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var msg = command as MessageChatRoom;
            Services.GetService<ChatRoomManager>().AddMessage(msg.ChatRoomId, msg.Message);
        }
    }
}
