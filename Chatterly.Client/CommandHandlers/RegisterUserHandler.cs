﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;

namespace Chatterly.Client.CommandHandlers
{
    /// <summary>
    /// Handles <see cref="RegisterUser"/> commands from the server.
    /// </summary>
    public class RegisterUserHandler : ICommandHandler
    {
        /// <summary>
        /// Doesn't really do anything yet...
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var reg = command as RegisterUser;

            if (reg.IsRegistered)
            {
                // TODO:
                // send back that user is registered
            }
            else
            {
                // TODO:
                // send error message somewhere
                var error = reg.ErrorMessage;
            }
        }
    }
}
