﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;

namespace Chatterly.Client.CommandHandlers
{
    /// <summary>
    /// Handles <see cref="GetChatRooms"/> commands from the server.
    /// </summary>
    public class GetChatRoomsHandler : ICommandHandler
    {
        /// <summary>
        /// Updates the room in the <see cref="ChatRoomManager"/>.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var get = command as GetChatRooms;
            foreach (var room in get.ChatRooms)
            {
                Services.GetService<ChatRoomManager>().UpdateRoom(room.ChatRoom, room.UserCount);
            }
        }
    }
}
