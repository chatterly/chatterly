﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Chatterly.Client.Database
{
    /// <summary>
    /// Represents a description of a server connection.
    /// </summary>
    public class ConnectionConfiguration
    {
        /// <summary>
        /// Name of this configuration.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Port to which we want to connect.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Byte array of the IP address to which we want to connect.
        /// </summary>
        public byte[] IpAddressData { get; set; }

        /// <summary>
        /// IP address to which we want to connect.
        /// Constructed from <see cref="IpAddressData"/>.
        /// </summary>
        [NotMapped]
        public IPAddress Address
        {
            get
            {
                return new IPAddress(IpAddressData);
            }

            set
            {
                IpAddressData = value.GetAddressBytes();
            }
        }

        /// <summary>
        /// Creates an end point to which to connect.
        /// </summary>
        /// <returns></returns>
        public IPEndPoint ToIpEndpoint()
        {
            return new IPEndPoint(Address, Port);
        }
    }
}
