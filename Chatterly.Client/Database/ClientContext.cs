﻿using Chatterly.Core.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

namespace Chatterly.Client.Database
{
    /// <summary>
    /// Client database context.
    /// </summary>
    public class ClientContext : ChatterlyContext
    {
        /// <summary>
        /// Reduce multi-threaded errors.
        /// </summary>
        static readonly object _DbLock = new object();

        /// <summary>
        /// <see cref="ConnectionConfiguration"/> table.
        /// </summary>
        public DbSet<ConnectionConfiguration> ConnectionConfigurations { get; set; }

        /// <summary>
        /// Configures database connection.
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./Chatterly.Client.db");
        }

        /// <summary>
        /// Handles model to table creation.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConnectionConfiguration>().HasKey(c => c.Name);
        }

        /// <summary>
        /// Ensure that database is created and adds a default <see cref="ConnectionConfiguration"/>.
        /// </summary>
        public override void EnsureCreated()
        {
            lock (_DbLock)
            {
                base.EnsureCreated();

                if (ConnectionConfigurations.Count() == 0)
                {
                    ConnectionConfigurations.Add(new ConnectionConfiguration
                    {
                        Name = "Development Server",
                        Address = IPAddress.Parse("127.0.0.1"),
                        Port = 7777
                    });

                    SaveChanges();
                }
            }
        }
    }
}
