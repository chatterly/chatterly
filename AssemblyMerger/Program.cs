﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Server;
using ILMerging;

namespace AssemblyMerger
{
    class Program
    {
        static void Main(string[] args)
        {
            var core = typeof(Services).Assembly.Location;
            var client = typeof(ChatClient).Assembly.Location;
            var clientApp = typeof(Chatterly.ClientApp.MainWindow).Assembly.Location;
            var serverApp = typeof(Chatterly.ServerApp.MainWindow).Assembly.Location;
            var server = typeof(ChatServer).Assembly.Location;

            var merge = new ILMerge();
            merge.SetInputAssemblies(new[] { core, client, clientApp, serverApp, server });
            merge.OutputFile = "ChatterlyMerged.dll";
            //merge.SetTargetPlatform("v4", typeof(Console).Assembly.Location);
            merge.Merge();
        }
    }
}
