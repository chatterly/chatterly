﻿using Chatterly.Client;
using Chatterly.Client.CommandHandlers;
using Chatterly.Client.Database;
using Chatterly.ClientApp.ViewModels;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Data;

namespace Chatterly.ClientApp
{
    public class MainViewModel : BaseViewModel
    {
        readonly object _ViewModelOptionsLock = new object();
        readonly object _ActiveViewModelOptionsLock = new object();

        public System.Windows.Input.ICommand AddActiveViewModelCommand
        {
            get
            {
                return _AddActiveViewModelCommand;
            }
        }
        readonly System.Windows.Input.ICommand _AddActiveViewModelCommand;

        public System.Windows.Input.ICommand RemoveActiveViewModelCommand
        {
            get
            {
                return _RemoveActiveViewModelCommand;
            }
        }
        readonly System.Windows.Input.ICommand _RemoveActiveViewModelCommand;

        public ObservableCollection<IViewModel> ViewModelOptions
        {
            get
            {
                return _ViewModelOptions;
            }
        }
        readonly ObservableCollection<IViewModel> _ViewModelOptions = new ObservableCollection<IViewModel>();

        public ObservableCollection<IViewModel> ActiveViewModels
        {
            get
            {
                return _ActiveViewModels;
            }
        }
        readonly ObservableCollection<IViewModel> _ActiveViewModels = new ObservableCollection<IViewModel>();

        public IViewModel ActiveViewModel
        {
            get
            {
                return _ActiveViewModel;
            }

            set
            {
                if (_ActiveViewModel != value)
                {
                    _ActiveViewModel = value;
                    RaisePropertyChanged();
                }
            }
        }
        IViewModel _ActiveViewModel;

        /// <summary>
        /// Dictionary of SenderUserId to instance of PrivateMessageViewModel for viewing private messages
        /// </summary>
        public Dictionary<int, PrivateMessageViewModel> PrivateMessages = new Dictionary<int, PrivateMessageViewModel>();

        public MainViewModel()
        {
            InitializeApp();

            BindingOperations.EnableCollectionSynchronization(_ViewModelOptions, _ViewModelOptionsLock);
            BindingOperations.EnableCollectionSynchronization(_ActiveViewModels, _ActiveViewModelOptionsLock);

            Name = "Chatterly";

            ViewModelOptions.Add(new ServerSelectionViewModel(this));
            AddActiveViewModel(ViewModelOptions[0], true);

            _AddActiveViewModelCommand = new RelayCommand(
                vm => AddActiveViewModel(vm as IViewModel, true),
                vm => vm is IViewModel);

            _RemoveActiveViewModelCommand = new RelayCommand(
                vm => RemoveActiveViewModel(vm as IViewModel),
                vm => vm is IViewModel);
        }

        void InitializeApp()
        {
            var keyName = string.Format("{0}.Resources.Key.xml", GetType().Namespace);
            var keyStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(keyName);
            var keyXml = (new StreamReader(keyStream)).ReadToEnd();
            User.Encryptor = new RsaCrypto(keyXml);

            var cmdr = new CommandHandlerRegistry();
            cmdr.RegisterHandler<RegisterUser>(new RegisterUserHandler());
            cmdr.RegisterHandler<AuthenticateUser>(new AuthenticateUserHandler());
            cmdr.RegisterHandler<GetChatRooms>(new GetChatRoomsHandler());
            cmdr.RegisterHandler<MessageChatRoom>(new MessageChatRoomHandler());
            Services.RegisterService(cmdr);

            Services.RegisterService(new ChatClient());
            Services.RegisterService(new ServerResponseNotifier());
            Services.RegisterService(new LogManager(new ClientContext()));
            Services.RegisterService(new ChatRoomManager());

            Services.GetService<ServerResponseNotifier>().Subscribe<MessageUser>(PrivateSubscription);
        }

        /// <summary>
        /// Called when get a response from server (what's handlers for then?)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PrivateSubscription(object sender, SeverResponseEventArgs e)
        {
            var PrivateMessage = e.Command as MessageUser;

            PrivateMessageViewModel PrivateViewModel;
            PrivateMessages.TryGetValue(PrivateMessage.Message.UserId, out PrivateViewModel);

            //If there's no private window opened for this user, make a new one and focus it
            if (PrivateViewModel == null)
            {
                PrivateViewModel = new PrivateMessageViewModel(this, PrivateMessage.Message.User);
                PrivateMessages.Add(PrivateMessage.Message.UserId, PrivateViewModel);
                PrivateViewModel.AddPrivateMessage(PrivateMessage.Message);
                AddActiveViewModel(PrivateViewModel, true);
            }
            //Otherwise, add the new message to the existing window
            else
            {
                PrivateViewModel.AddPrivateMessage(PrivateMessage.Message);
            }
        }

        public bool AddActiveViewModel(IViewModel vm, bool setActive = false)
        {
            var result = false;

            if (!ActiveViewModels.Contains(vm))
            {
                ActiveViewModels.Add(vm);
                vm.OnActive();

                result = true;
            }

            if (setActive)
            {
                ActiveViewModel = vm;
            }

            return result;
        }

        public bool RemoveActiveViewModel(IViewModel vm)
        {
            var removed = ActiveViewModels.Remove(vm);

            if (removed)
            {
                vm.OnRemove();

                if (ActiveViewModel?.Id == vm.Id)
                {
                    ActiveViewModel = ActiveViewModels.Count == 0 ? null : ActiveViewModels[0];
                }
            }

            return removed;
        }

        public bool RemoveActiveViewModel(Guid id)
        {
            var viewModel = ActiveViewModels.FirstOrDefault(vm => vm.Id == id);

            if (viewModel == null)
            {
                return false;
            }

            return ActiveViewModels.Remove(viewModel);
        }
    }
}
