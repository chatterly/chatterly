﻿using Chatterly.Core.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Chatterly.ClientApp.Converters
{
    public class IsMod : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var rank = (ChatRoomRank?)value;

            return rank.HasValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
