﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Chatterly.ClientApp.Converters
{
    public class LocalFromUtc : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime))
            {
                return null;
            }

            return ((DateTime)value).ToLocalTime();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime))
            {
                return null;
            }

            return ((DateTime)value).ToUniversalTime();
        }
    }
}
