﻿using Chatterly.Core.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Chatterly.ClientApp.Converters
{
    public class IsAdmin : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var rank = (ChatRoomRank?)value;

            return rank.HasValue && (rank.Value == ChatRoomRank.Admin || rank.Value == ChatRoomRank.Owner);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
