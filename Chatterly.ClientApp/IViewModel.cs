﻿using System;

namespace Chatterly.ClientApp
{
    public interface IViewModel
    {
        Guid Id { get; }
        string Name { get; }
        void OnActive();
        void OnRemove();
    }
}
