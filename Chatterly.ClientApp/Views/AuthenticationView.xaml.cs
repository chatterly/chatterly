﻿using Chatterly.ClientApp.ViewModels;
using Chatterly.Core.Models;
using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Chatterly.ClientApp.Views
{
    /// <summary>
    /// Interaction logic for AuthenticationPrompt.xaml
    /// </summary>
    public partial class AuthenticationView : UserControl
    {
        public AuthenticationView()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ((AuthenticationViewModel)DataContext).ShowErrorEvent += AuthenticationView_ShowErrorEvent;
            //((AuthenticationViewModel)DataContext).CloseEvent += AuthenticationView_CloseEvent;
        }

        //private void AuthenticationView_CloseEvent(object sender, EventArgs e)
        //{
        //    //new ChatRoomSelectionView(((ChatRoomSelectionViewModel)DataContext).SelectedChatRoom.ChatRoom).Show();
        //    Close();
        //}

        private void AuthenticationView_ShowErrorEvent(object sender, EventArgs e)
        {
            MessageBox.Show("Login Error");
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as AuthenticationViewModel;
            if (vm != null)
            {
                vm.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes((sender as PasswordBox)?.Password)));
            }
        }

        private void TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            (sender as UIElement).Focus();
        }
    }
}
