﻿using System.Windows.Controls;

namespace Chatterly.ClientApp.Views
{
    /// <summary>
    /// Interaction logic for ChatRoomSelectionView.xaml
    /// </summary>
    public partial class ChatRoomSelectionView : UserControl
    {
        public ChatRoomSelectionView()
        {
            InitializeComponent();
        }

        //private void ChatRoomSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        //{
        //    ((ChatRoomSelectionViewModel)DataContext).ShowWindowEvent += ChatRoomSelectionView_ShowWindowEvent;
        //}

        //private void ChatRoomSelectionView_ShowWindowEvent(object sender, EventArgs e)
        //{
        //    //A chat room needs to know which chatroom to open...
        //    new ChatRoomView(((ChatRoomSelectionViewModel)DataContext).SelectedChatRoom.ChatRoom).Show();
        //}
    }
}
