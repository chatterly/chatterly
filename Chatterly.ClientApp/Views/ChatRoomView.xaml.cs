﻿using Chatterly.ClientApp.Converters;
using Chatterly.ClientApp.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Chatterly.ClientApp.Views
{
    /// <summary>
    /// Interaction logic for ChatRoomView.xaml
    /// </summary>
    public partial class ChatRoomView : UserControl
    {
        ChatRoomViewModel ViewModel
        {
            get
            {
                return DataContext as ChatRoomViewModel;
            }
        }

        public ChatRoomView()
        {
            InitializeComponent();
        }

        void MessageBox_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            (sender as UIElement).Focus();
        }

        MenuItem CreateBanUserMenuItem(ListBoxItem item)
        {
            var banUser = new MenuItem
            {
                Header = "Ban User"
            };
            banUser.SetBinding(VisibilityProperty, new Binding
            {
                Converter = new ValueConverterChain { new IsMod(), new VisibilityFromBool() },
                Source = ViewModel.UserRank
            });
            // command parameter needs to be bound before the command
            banUser.SetBinding(MenuItem.CommandParameterProperty, new Binding
            {
                Source = item.DataContext
            });
            banUser.SetBinding(MenuItem.CommandProperty, new Binding
            {
                Source = ViewModel.BanUser
            });

            return banUser;
        }

        MenuItem CreateMessageUserMenuItem(ListBoxItem item)
        {
            var messageUser = new MenuItem
            {
                Header = "Message User"
            };

            messageUser.SetBinding(MenuItem.CommandParameterProperty, new Binding
            {
                Source = item.DataContext
            });
            messageUser.SetBinding(MenuItem.CommandProperty, new Binding
            {
                Source = ViewModel.SendPrivateMessage
            });

            return messageUser;
        }

        void ListBoxItem_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as ChatRoomViewModel;
            var item = sender as ListBoxItem;
            item.ContextMenu = new ContextMenu();
            item.ContextMenu.Items.Add(CreateBanUserMenuItem(item));
            item.ContextMenu.Items.Add(CreateMessageUserMenuItem(item));
        }
    }
}
