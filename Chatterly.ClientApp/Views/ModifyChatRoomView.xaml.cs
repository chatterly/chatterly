﻿using Chatterly.ClientApp.ViewModels;
using Chatterly.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;

namespace Chatterly.ClientApp.Views
{
    /// <summary>
    /// Interaction logic for ChatRoomView.xaml
    /// </summary>
    public partial class ModifyChatRoomView : UserControl
    {
        public class RankModel : ObservableObject
        {
            public ChatRoomRank Rank { get; set; }

            public bool IsEnabled
            {
                get
                {
                    return _IsEnabled;
                }
                set
                {
                    if (_IsEnabled != value)
                    {
                        _IsEnabled = value;
                        RaisePropertyChanged();
                    }
                }
            }
            bool _IsEnabled = true;
        }

        public IEnumerable<RankModel> StaffRanks { get; private set; }

        ModifyChatRoomViewModel ViewModel
        {
            get
            {
                return DataContext as ModifyChatRoomViewModel;
            }
        }

        public ModifyChatRoomView()
        {
            var staffRanks = new Collection<RankModel>();

            foreach (ChatRoomRank rank in Enum.GetValues(typeof(ChatRoomRank)))
            {
                staffRanks.Add(new RankModel
                {
                    Rank = rank,
                    IsEnabled = true
                });
            }

            StaffRanks = staffRanks;

            InitializeComponent();
        }

        void StackPanel_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.PropertyChanged += new PropertyChangedEventHandler(UpdateAvailableRanks);
        }

        void UpdateAvailableRanks(object sender, PropertyChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                if (e.PropertyName == nameof(ViewModel.UserRank))
                {
                    foreach (var rank in StaffRanks)
                    {
                        if (!ViewModel.UserRank.HasValue || ViewModel.UserRank.Value <= rank.Rank)
                        {
                            rank.IsEnabled = false;
                        }
                        else
                        {
                            rank.IsEnabled = true;
                        }
                    }
                }
            }
        }
    }
}
