﻿using Chatterly.Client.Database;
using System;
using System.Windows;

namespace Chatterly.ClientApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                (new ClientContext()).EnsureCreated();

                var main = new MainWindow();
                main.DataContext = new MainViewModel();
                main.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Encountered a fatal error: " + ex.Message);
            }
        }
    }
}
