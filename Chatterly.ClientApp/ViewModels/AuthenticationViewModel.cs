﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using System;
using System.Linq;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        public event EventHandler ShowErrorEvent;

        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        public string Nickname
        {
            get
            {
                return _Nickname;
            }
            set
            {
                _Nickname = value;
                RaisePropertyChanged();
                _LoginCommand?.RaiseCanExecuteChanged();
                _RegisterCommand?.RaiseCanExecuteChanged();

            }
        }
        string _Nickname;

        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
                RaisePropertyChanged();
                _LoginCommand?.RaiseCanExecuteChanged();
                _RegisterCommand?.RaiseCanExecuteChanged();
            }
        }
        string _Password;

        public ICommand Login
        {
            get
            {
                return _LoginCommand;
            }
        }
        readonly RelayCommand _LoginCommand;

        public ICommand Register
        {
            get
            {
                return _RegisterCommand;
            }
        }
        readonly RelayCommand _RegisterCommand;

        public ICommand LoginOrRegister
        {
            get
            {
                return _LoginOrRegisterCommand;
            }
        }
        readonly RelayCommand _LoginOrRegisterCommand;

        public AuthenticationViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
            Name = "Log In";
            _LoginCommand = new RelayCommand(LoginToServer, CanSubmitData);
            _RegisterCommand = new RelayCommand(RegisterWithServer, CanSubmitData);
            _LoginOrRegisterCommand = new RelayCommand(LoginOrRegisterWithServer, CanSubmitData);
        }

        void LoginOrRegisterWithServer(object o = null)
        {
            Services.GetService<ChatClient>().Send(DoesNickNameExist.CreateCommand(Nickname), cmd =>
            {
                var does = cmd as DoesNickNameExist;

                if (does.NickNameExists)
                {
                    LoginToServer();
                }
                else
                {
                    RegisterWithServer();
                }
            });
        }

        void LoginToServer(object o = null)
        {
            Services.GetService<ChatClient>().Send(AuthenticateUser.CreateCommand(Nickname, Password, false), cmd =>
            {
                var login = cmd as AuthenticateUser;

                if (login.IsAuthenticated)
                {
                    OpenChatRoomSelection();
                }
                else
                {
                    RaiseShowErrorEvent();
                }
            });
        }

        void RegisterWithServer(object o = null)
        {
            Services.GetService<ChatClient>().Send(RegisterUser.CreateCommand(Nickname, Password, false), cmd =>
            {
                var reg = cmd as RegisterUser;

                if (reg.IsRegistered)
                {
                    if (Services.GetService<ChatClient>().User != null)
                    {
                        OpenChatRoomSelection();
                    }
                    else
                    {
                        LoginToServer();
                    }
                }
                else
                {
                    RaiseShowErrorEvent();
                }
            });
        }

        void OpenChatRoomSelection()
        {
            var chatRoomSelection = MainViewModel.ViewModelOptions.FirstOrDefault(
                vm => vm.GetType() == typeof(ChatRoomSelectionViewModel));

            if (chatRoomSelection == null)
            {
                chatRoomSelection = new ChatRoomSelectionViewModel(MainViewModel);
                MainViewModel.ViewModelOptions.Add(chatRoomSelection);
            }

            MainViewModel.AddActiveViewModel(chatRoomSelection, true);
            MainViewModel.RemoveActiveViewModel(this);
        }

        bool CanSubmitData(object o)
        {
            return !string.IsNullOrWhiteSpace(Nickname) && !string.IsNullOrWhiteSpace(Password);
        }

        //Raises event that a view is subscribed to which will notify the user of the login error
        void RaiseShowErrorEvent()
        {
            ShowErrorEvent?.Invoke(this, null);
        }
    }
}