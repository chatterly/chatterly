﻿using Chatterly.Client;
using Chatterly.ClientApp.ViewModels.ChatRoomEdit;
using Chatterly.Core;
using Chatterly.Core.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using static Chatterly.Core.Commands.GetChatRooms;

namespace Chatterly.ClientApp.ViewModels
{
    public class ChatRoomSelectionViewModel : BaseViewModel
    {
        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        public ChatRoomCreationViewModel CreateViewModel
        {
            get
            {
                return _CreateViewModel;
            }
        }
        ChatRoomCreationViewModel _CreateViewModel;

        public ChatRoomMeta SelectedChatRoom

        {
            get
            {
                return _SelectedChatRoom;
            }
            set
            {
                _SelectedChatRoom = value;
                RaisePropertyChanged();
                _JoinCommand.RaiseCanExecuteChanged();
            }
        }
        ChatRoomMeta _SelectedChatRoom;

        public List<ChatRoomMeta> ChatRooms
        {
            get
            {
                return _Rooms;
            }

            set
            {
                if (value != _Rooms)
                {
                    _Rooms = value;
                    RaisePropertyChanged();
                }
            }
        }
        List<ChatRoomMeta> _Rooms;

        public ICommand Join
        {
            get
            {
                return _JoinCommand;
            }
        }
        readonly RelayCommand _JoinCommand;

        public ICommand Refresh
        {
            get
            {
                return _RefreshCommand;
            }
        }
        readonly RelayCommand _RefreshCommand;

        public string ChatRoomMessage
        {
            get
            {
                return _ChatRoomMessage;
            }

            set
            {
                if (_ChatRoomMessage != value)
                {
                    _ChatRoomMessage = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _ChatRoomMessage;

        public ChatRoomSelectionViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
            _CreateViewModel = new ChatRoomCreationViewModel();

            _CreateViewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName == nameof(CreateViewModel.ChatRoomMessage))
                {
                    RefreshChatRooms();
                }
            };

            Name = "Chat Rooms";
            _JoinCommand = new RelayCommand(DoJoin, obj => SelectedChatRoom != null);
            _RefreshCommand = new RelayCommand(RefreshChatRooms, null);
        }

        public override void OnActive()
        {
            if (!Services.GetService<ChatClient>().IsConnected)
            {
                // raise message to user to connect to server
                var serverSelection = MainViewModel.ViewModelOptions.FirstOrDefault(
                    vm => vm.GetType() == typeof(ServerSelectionViewModel)) ?? new ServerSelectionViewModel(MainViewModel);
                MainViewModel.AddActiveViewModel(serverSelection, true);
                MainViewModel.RemoveActiveViewModel(this);
                return;
            }

            RefreshChatRooms();
        }

        private void DoJoin(object o = null)
        {
            ChatRoomMessage = string.Empty;

            var chatRoomVm = MainViewModel.ActiveViewModels.FirstOrDefault(vm =>
            {
                if (vm is ChatRoomViewModel)
                {
                    return ((vm as ChatRoomViewModel).ChatRoom?.Id == SelectedChatRoom.ChatRoom.Id);
                }

                return false;
            });

            if (chatRoomVm == null)
            {
                Services.GetService<ChatClient>().Send(JoinChatRoom.CreateCommand(SelectedChatRoom.ChatRoom.Id), cmd =>
                {
                    if (cmd is JoinChatRoom)
                    {
                        var join = cmd as JoinChatRoom;

                        if (join.Joined)
                        {
                            MainViewModel.AddActiveViewModel(new ChatRoomViewModel(MainViewModel, join.ChatRoom), true);
                            RefreshChatRooms();
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() => ChatRoomMessage = "Could not join the chatroom.");
                        }
                    }
                    else if (cmd is BoolResponse)
                    {
                        var response = cmd as BoolResponse;
                        Application.Current.Dispatcher.Invoke(() => ChatRoomMessage = response.Message);
                    }
                });
            }
            else
            {
                MainViewModel.ActiveViewModel = chatRoomVm;
            }
        }

        private void RefreshChatRooms(object o = null)
        {
            ChatRoomMessage = string.Empty;

            Services.GetService<ChatClient>().Send(new GetChatRooms(), cmd =>
            {
                if (cmd is GetChatRooms)
                {
                    var get = cmd as GetChatRooms;
                    ChatRooms = get.ChatRooms.ToList();
                }
            });
        }
    }
}