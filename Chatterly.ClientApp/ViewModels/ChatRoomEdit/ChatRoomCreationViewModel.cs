﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using System.Windows;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels.ChatRoomEdit
{
    public class ChatRoomCreationViewModel : ChatRoomEditViewModel
    {
        public ChatRoomCreationViewModel()
        {
            _ActionCommand = new RelayCommand(CreateChatRoomCommand,
                obj => !string.IsNullOrWhiteSpace(ChatRoomName) && !string.IsNullOrWhiteSpace(ChatRoomDescription));
        }

        void CreateChatRoomCommand(object o = null)
        {
            ChatRoomMessage = string.Empty;

            Services.GetService<ChatClient>().Send(
                CreateChatRoom.CreateCommand(ChatRoomName, ChatRoomDescription, IsChatRoomPrivate),
                rtn =>
            {
                var cmd = rtn as CreateChatRoom;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    ChatRoomMessage = cmd.Message;

                    if (cmd.IsCreated)
                    {
                        ChatRoomName = string.Empty;
                        ChatRoomDescription = string.Empty;
                        IsChatRoomPrivate = false;
                    }
                });
            });
        }
    }
}
