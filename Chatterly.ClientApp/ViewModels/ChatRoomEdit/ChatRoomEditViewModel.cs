﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels.ChatRoomEdit
{
    public abstract class ChatRoomEditViewModel : BaseViewModel
    {
        public ICommand Action
        {
            get
            {
                return _ActionCommand;
            }
        }
        protected RelayCommand _ActionCommand;


        public virtual string ChatRoomName
        {
            get
            {
                return _ChatRoomName;
            }

            set
            {
                if (_ChatRoomName != value)
                {
                    _ChatRoomName = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }
        string _ChatRoomName = string.Empty;

        public virtual string ChatRoomDescription
        {
            get
            {
                return _ChatRoomDescription;
            }

            set
            {
                if (_ChatRoomDescription != value)
                {
                    _ChatRoomDescription = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }
        string _ChatRoomDescription = string.Empty;

        public virtual bool IsChatRoomPrivate
        {
            get
            {
                return _IsChatRoomPrivate;
            }

            set
            {
                if (_IsChatRoomPrivate != value)
                {
                    _IsChatRoomPrivate = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }
        bool _IsChatRoomPrivate;

        public string ChatRoomMessage
        {
            get
            {
                return _ChatRoomMessage;
            }

            set
            {
                if (_ChatRoomMessage != value)
                {
                    _ChatRoomMessage = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }
        string _ChatRoomMessage;
    }
}
