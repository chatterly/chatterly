﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using System.Windows;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels.ChatRoomEdit
{
    public class ChatRoomModifyViewModel : ChatRoomEditViewModel
    {
        public ChatRoom ChatRoom { get; private set; }

        public override string ChatRoomName
        {
            get
            {
                return ChatRoom.Name;
            }

            set
            {
                if (ChatRoom.Name != value)
                {
                    ChatRoom.Name = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public override string ChatRoomDescription
        {
            get
            {
                return ChatRoom.Description;
            }

            set
            {
                if (ChatRoom.Description != value)
                {
                    ChatRoom.Description = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public override bool IsChatRoomPrivate
        {
            get
            {
                return ChatRoom.IsPrivate;
            }

            set
            {
                if (ChatRoom.IsPrivate != value)
                {
                    ChatRoom.IsPrivate = value;
                    RaisePropertyChanged();
                    _ActionCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public ChatRoomModifyViewModel(ChatRoom chatroom)
        {
            ChatRoom = chatroom;

            _ActionCommand = new RelayCommand(ModifyChatRoomCommand,
                obj => !string.IsNullOrWhiteSpace(ChatRoomName) && !string.IsNullOrWhiteSpace(ChatRoomDescription));
        }

        void ModifyChatRoomCommand(object o = null)
        {
            ChatRoomMessage = string.Empty;

            Services.GetService<ChatClient>().Send(
                new ModifyChatRoom { ChatRoom = ChatRoom },
                rtn =>
            {
                var response = rtn as BoolResponse;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    ChatRoomMessage = response.Message;
                });
            });
        }
    }
}
