﻿using Chatterly.Client;
using Chatterly.ClientApp.ViewModels.ChatRoomEdit;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels
{
    public class ModifyChatRoomViewModel : BaseViewModel
    {
        readonly object _StaffLock = new object();
        readonly object _BansLock = new object();
        readonly object _MembersLock = new object();
        System.Timers.Timer _UpdateTimer;

        public ICommand UpdateRank
        {
            get
            {
                return _UpdateRank;
            }
        }
        RelayCommand _UpdateRank;

        public ICommand RemoveRank
        {
            get
            {
                return _RemoveRank;
            }
        }
        RelayCommand _RemoveRank;

        public ICommand AddStaff
        {
            get
            {
                return _AddStaff;
            }
        }
        RelayCommand _AddStaff;

        public ICommand AddBan
        {
            get
            {
                return _AddBan;
            }
        }
        RelayCommand _AddBan;

        public ICommand RemoveBan
        {
            get
            {
                return _RemoveBan;
            }
        }
        RelayCommand _RemoveBan;

        public ICommand AddMember
        {
            get
            {
                return _AddMember;
            }
        }
        RelayCommand _AddMember;

        public ICommand RemoveMember
        {
            get
            {
                return _RemoveMember;
            }
        }
        RelayCommand _RemoveMember;

        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        public ChatRoomModifyViewModel ModifyViewModel
        {
            get
            {
                return _ModifyViewModel;
            }
        }
        ChatRoomModifyViewModel _ModifyViewModel;

        public ObservableCollection<ChatRoomStaff> Staff
        {
            get
            {
                return _Staff;
            }
        }
        readonly ObservableCollection<ChatRoomStaff> _Staff = new ObservableCollection<ChatRoomStaff>();

        public ObservableCollection<ChatRoomBan> Bans
        {
            get
            {
                return _Bans;
            }
        }
        readonly ObservableCollection<ChatRoomBan> _Bans = new ObservableCollection<ChatRoomBan>();

        public ObservableCollection<User> Members
        {
            get
            {
                return _Members;
            }
        }
        readonly ObservableCollection<User> _Members = new ObservableCollection<User>();

        public ChatRoom ChatRoom
        {
            get
            {
                return _ChatRoom;
            }

            set
            {
                if (_ChatRoom != value)
                {
                    _ChatRoom = value;
                    RaisePropertyChanged();
                }
            }
        }
        ChatRoom _ChatRoom;

        public ChatRoomRank? UserRank
        {
            get
            {
                return _UserRank;
            }

            set
            {
                if (_UserRank != value)
                {
                    _UserRank = value;
                    RaisePropertyChanged();
                }
            }
        }
        ChatRoomRank? _UserRank = null;

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                if (_Message != value)
                {
                    _Message = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _Message;

        public IEnumerable<User> UserAutoCompletion
        {
            get
            {
                return _Users;
            }
        }
        IList<User> _Users;

        public User NewStaffUser
        {
            get
            {
                return _NewStaffUser;
            }

            set
            {
                if (_NewStaffUser != value)
                {
                    _NewStaffUser = value;
                    RaisePropertyChanged();
                    _AddStaff?.RaiseCanExecuteChanged();
                }
            }
        }
        User _NewStaffUser;

        public ChatRoomRank NewStaffRank { get; set; }

        public User NewBanUser
        {
            get
            {
                return _NewBanUser;
            }

            set
            {
                if (_NewBanUser != value)
                {
                    _NewBanUser = value;
                    RaisePropertyChanged();
                    _AddBan?.RaiseCanExecuteChanged();
                }
            }
        }
        User _NewBanUser;

        public User NewMemberUser
        {
            get
            {
                return _NewMemberUser;
            }

            set
            {
                if (_NewMemberUser != value)
                {
                    _NewMemberUser = value;
                    RaisePropertyChanged();
                    _AddMember?.RaiseCanExecuteChanged();
                }
            }
        }
        User _NewMemberUser;

        public ModifyChatRoomViewModel(MainViewModel mainViewModel, ChatRoom chatRoom)
        {
            BindingOperations.EnableCollectionSynchronization(_Staff, _StaffLock);
            BindingOperations.EnableCollectionSynchronization(_Bans, _BansLock);
            BindingOperations.EnableCollectionSynchronization(_Members, _MembersLock);

            ChatRoom = chatRoom;
            _MainViewModel = mainViewModel;
            _ModifyViewModel = new ChatRoomModifyViewModel(chatRoom);

            Name = string.Format("Modify Chatroom: {0}", ChatRoom.Name);

            _UpdateRank = new RelayCommand(DoUpdateRank, null);
            _RemoveRank = new RelayCommand(DoRemoveRank, null);
            _AddStaff = new RelayCommand(DoAddStaff, o => NewStaffUser != null);
            _AddBan = new RelayCommand(DoAddBan, o => NewBanUser != null);
            _RemoveBan = new RelayCommand(DoRemoveBan, null);
            _AddMember = new RelayCommand(DoAddMember, o => NewMemberUser != null);
            _RemoveMember = new RelayCommand(DoRemoveMember, null);

            ManualResetEvent wait = new ManualResetEvent(false);

            Services.GetService<ChatClient>().Send(GetUsers.CreateCommand(), rtn =>
            {
                var get = rtn as GetUsers;
                _Users = get.Users;
                wait.Set();
            });

            wait.WaitOne();

            GetStaffUpdate();
            GetBansUpdate();
            GetMemberUpdate();
        }

        public override void OnActive()
        {
            _UpdateTimer = new System.Timers.Timer(5000);
            _UpdateTimer.Elapsed += (sender, e) =>
            {
                GetStaffUpdate();
                GetBansUpdate();
                GetMemberUpdate();
            };
            _UpdateTimer.Start();
        }

        public override void OnRemove()
        {
            _UpdateTimer.Stop();
            _UpdateTimer = null;
        }

        void DoUpdateRank(object o)
        {
            var staff = o as ChatRoomStaff;

            Services.GetService<ChatClient>().Send(
                ModifyChatRoomStaff.CreateCommand(staff.ChatRoomId, staff.User.NickName, staff.Rank),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() => Message = response.Message);
                    GetStaffUpdate();
                });
        }

        void DoRemoveRank(object o)
        {
            var staff = o as ChatRoomStaff;

            Services.GetService<ChatClient>().Send(
                ModifyChatRoomStaff.CreateCommand(staff.ChatRoomId, staff.User.NickName, null),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() => Message = response.Message);
                    GetStaffUpdate();
                });
        }

        void DoAddStaff(object o = null)
        {
            var staff = o as ChatRoomStaff;

            Services.GetService<ChatClient>().Send(
                ModifyChatRoomStaff.CreateCommand(ChatRoom.Id, NewStaffUser.NickName, NewStaffRank),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Message = response.Message;
                        NewStaffUser = null;
                    });
                    GetStaffUpdate();
                });
        }

        void DoAddBan(object o = null)
        {
            Services.GetService<ChatClient>().Send(
                ModifyBan.BanUser(ChatRoom.Id, NewBanUser.NickName),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Message = response.Message;
                        NewBanUser = null;
                    });
                    GetBansUpdate();
                });
        }

        void DoRemoveBan(object o)
        {
            var ban = o as ChatRoomBan;

            Services.GetService<ChatClient>().Send(
                ModifyBan.UnbanUser(ban.ChatRoomId, ban.User.NickName),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() => Message = response.Message);
                    GetBansUpdate();
                });
        }

        void DoAddMember(object o)
        {
            Services.GetService<ChatClient>().Send(
                ModifyChatRoomMembership.AddMember(ChatRoom.Id, NewMemberUser.NickName),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Message = response.Message;
                        NewMemberUser = null;
                    });
                    GetMemberUpdate();
                });
        }

        void DoRemoveMember(object o)
        {
            var user = o as User;

            Services.GetService<ChatClient>().Send(
                ModifyChatRoomMembership.RemoveMember(ChatRoom.Id, user.NickName),
                rtn =>
                {
                    var response = rtn as BoolResponse;
                    Application.Current.Dispatcher.Invoke(() => Message = response.Message);
                    GetMemberUpdate();
                });
        }

        void GetStaffUpdate()
        {
            var client = Services.GetService<ChatClient>();
            client.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), cmd =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var staff = cmd as GetChatRoomStaff;

                    var user = Services.GetService<ChatClient>().User;

                    UserRank = null;

                    Staff.Clear();
                    staff.Staff.ForEach(s =>
                    {
                        Staff.Add(s);

                        if (s.UserId == user.Id)
                        {
                            UserRank = s.Rank;

                        }
                    });

                    if (!UserRank.HasValue)
                    {
                        // our rank has been removed!
                        MainViewModel.RemoveActiveViewModel(this);
                    }
                });
            });
        }

        void GetBansUpdate()
        {
            var client = Services.GetService<ChatClient>();
            client.Send(GetChatRoomBans.CreateCommand(ChatRoom.Id), cmd =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var bans = cmd as GetChatRoomBans;
                    Bans.Clear();
                    bans.Bans.ForEach(m => Bans.Add(m));
                });
            });
        }

        void GetMemberUpdate()
        {
            var client = Services.GetService<ChatClient>();
            client.Send(GetChatRoomMembers.CreateCommand(ChatRoom.Id), cmd =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var members = cmd as GetChatRoomMembers;
                    Members.Clear();
                    members.Members.ForEach(m => Members.Add(m));
                });
            });
        }
    }
}