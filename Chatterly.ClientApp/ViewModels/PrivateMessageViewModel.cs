﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels
{
    public class PrivateMessageViewModel : BaseViewModel
    {
        readonly object _MessagesLock = new object();

        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        /// <summary>
        /// Messages sent to the chatroom since the user connected.
        /// </summary>
        public ObservableCollection<Message> Messages
        {
            get
            {
                return _Messages;
            }
        }
        readonly ObservableCollection<Message> _Messages = new ObservableCollection<Message>();

        /// <summary>
        /// Message that client sends to the chatroom.
        /// </summary>
        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                if (value != _Message)
                {
                    _Message = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _Message = string.Empty;

        /// <summary>
        /// Command that sends client's message to the chatroom.
        /// </summary>
        public ICommand SendMessage
        {
            get
            {
                return _SendMessage;
            }
        }
        readonly RelayCommand _SendMessage;

        /// <summary>
        /// The user receiving the private messages
        /// </summary>
        private User OtherUser;

        public PrivateMessageViewModel(MainViewModel mainViewModel, User user)
        {
            BindingOperations.EnableCollectionSynchronization(_Messages, _MessagesLock);

            _MainViewModel = mainViewModel;
            OtherUser = user;

            Name = OtherUser.NickName;
            
            _SendMessage = new RelayCommand(SendPrivateMessage, null);
        }

        void SendPrivateMessage(object o = null)
        {
            var client = Services.GetService<ChatClient>();
            client.Send((MessageUser.CreateCommand(OtherUser.Id, Message)));

            var LocalMessage = new Message
            {
                Content = Message,
                Time = DateTime.UtcNow,
                User = client.User
            };

            AddPrivateMessage(LocalMessage);
            Message = string.Empty;
        }

        public void AddPrivateMessage(Message message)
        {
            Application.Current.Dispatcher.Invoke(() =>
                    Messages.Add(message));
        }

        public override void OnRemove()
        {
            var ThisKey = MainViewModel.PrivateMessages
                .Where(v => v.Value.Equals(this))
                .Select(v => v.Key)
                .FirstOrDefault();

            MainViewModel.PrivateMessages.Remove(ThisKey);
        }
    }
}
