﻿using Chatterly.Client;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels
{
    public class ChatRoomViewModel : BaseViewModel
    {
        readonly object _MessagesLock = new object();
        readonly object _UsersLock = new object();
        Timer _UpdateTimer;

        public ChatRoom ChatRoom
        {
            get
            {
                return _ChatRoom;
            }

            set
            {
                if (value != _ChatRoom)
                {
                    _ChatRoom = value;
                    RaisePropertyChanged();
                }
            }
        }
        ChatRoom _ChatRoom;

        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        /// <summary>
        /// Users connect to this chat room.
        /// </summary>
        public ObservableCollection<User> Users
        {
            get
            {
                return _Users;
            }

            private set
            {
                if (value != _Users)
                {
                    _Users = value;
                    RaisePropertyChanged();
                }
            }
        }
        ObservableCollection<User> _Users = new ObservableCollection<User>();

        /// <summary>
        /// Messages sent to the chatroom since the user connected.
        /// </summary>
        public ObservableCollection<Message> Messages
        {
            get
            {
                return _Messages;
            }
        }
        readonly ObservableCollection<Message> _Messages = new ObservableCollection<Message>();

        readonly IDictionary<int, ChatRoomRank> _StaffCache = new Dictionary<int, ChatRoomRank>();

        /// <summary>
        /// Message that client sends to the chatroom.
        /// </summary>
        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                if (value != _Message)
                {
                    _Message = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _Message = string.Empty;

        /// <summary>
        /// Command that sends client's message to a selected user.
        /// </summary>
        public ICommand SendPrivateMessage
        {
            get
            {
                return _SendPrivateMessage;
            }
        }
        readonly RelayCommand _SendPrivateMessage;

        /// <summary>
        /// Command that sends client's message to the chatroom.
        /// </summary>
        public ICommand SendMessage
        {
            get
            {
                return _SendMessage;
            }
        }
        readonly RelayCommand _SendMessage;

        public ICommand BanUser
        {
            get
            {
                return _BanUser;
            }
        }
        readonly RelayCommand _BanUser;

        public ICommand OpemModify
        {
            get
            {
                return _OpenModify;
            }
        }
        readonly RelayCommand _OpenModify;

        public ChatRoomRank? UserRank
        {
            get
            {
                return _UserRank;
            }

            set
            {
                if (_UserRank != value)
                {
                    _UserRank = value;
                    RaisePropertyChanged();
                    _BanUser?.RaiseCanExecuteChanged();
                    _OpenModify?.RaiseCanExecuteChanged();
                }
            }
        }
        ChatRoomRank? _UserRank = null;

        public string ServerMessage
        {
            get
            {
                return _ServerMessage;
            }

            set
            {
                if (_ServerMessage != value)
                {
                    _ServerMessage = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _ServerMessage = null;

        public ChatRoomViewModel(MainViewModel mainViewModel, ChatRoom chatRoom)
        {
            BindingOperations.EnableCollectionSynchronization(_Messages, _MessagesLock);
            BindingOperations.EnableCollectionSynchronization(_Users, _UsersLock);

            Name = string.Format("Chat Room: {0}", chatRoom.Name);
            _MainViewModel = mainViewModel;
            ChatRoom = chatRoom;

            _SendMessage = new RelayCommand(SendChatRoomMessage, null);
            _SendPrivateMessage = new RelayCommand(OpenPrivateMessage, null);
            _BanUser = new RelayCommand(DoBanUser, CanBanUser);
            _OpenModify = new RelayCommand(OpenModifyChatRoom, o => UserRank.HasValue);

            Services.GetService<ServerResponseNotifier>().Subscribe<MessageChatRoom>(MessageSubscription);
            Services.GetService<ServerResponseNotifier>().Subscribe<KickUser>(KickSubscription);
        }

        public override void OnActive()
        {
            // TODO:
            // add timer to periodically check to see if we're still in chatroom
            // or do that on the send or something

            GetStaffUpdate();
            RefreshUsers();

            _UpdateTimer = new Timer(5000);
            _UpdateTimer.Elapsed += (sender, e) =>
            {
                GetStaffUpdate();
                RefreshUsers();
            };
            _UpdateTimer.Start();
        }

        public override void OnRemove()
        {
            _UpdateTimer.Stop();
            _UpdateTimer = null;
            Services.GetService<ServerResponseNotifier>().Unsubscribe<MessageChatRoom>(MessageSubscription);
            Services.GetService<ServerResponseNotifier>().Unsubscribe<KickUser>(KickSubscription);
            Services.GetService<ChatClient>().Send(LeaveChatRoom.CreateCommand(ChatRoom.Id));
        }

        void GetStaffUpdate()
        {
            var client = Services.GetService<ChatClient>();
            client.Send(GetChatRoomStaff.CreateCommand(ChatRoom.Id), cmd =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var staff = cmd as GetChatRoomStaff;
                    _StaffCache.Clear();
                    staff.Staff.ForEach(s => _StaffCache.Add(s.User.Id, s.Rank));

                    if (_StaffCache.ContainsKey(client.User.Id))
                    {
                        UserRank = _StaffCache[client.User.Id];
                    }
                    else
                    {
                        UserRank = null;
                    }

                    _BanUser?.RaiseCanExecuteChanged();
                });
            });
        }

        void RefreshUsers()
        {
            Services.GetService<ChatClient>().Send(GetChatRoom.CreateCommand(ChatRoom.Id), cmd =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var get = cmd as GetChatRoom;
                    ChatRoom = get.ChatRoom;
                    Users = new ObservableCollection<User>(get.Users);
                });
            });
        }

        void KickSubscription(object sender, SeverResponseEventArgs e)
        {
            var kick = e.Command as KickUser;

            if (kick.ChatRoomId == ChatRoom.Id)
            {
                MainViewModel.RemoveActiveViewModel(this);
            }
        }

        void MessageSubscription(object sender, SeverResponseEventArgs e)
        {
            var chat = e.Command as MessageChatRoom;

            if (chat.ChatRoomId == ChatRoom.Id)
            {
                // TODO:
                // replace this was a proper observablecollection that can handle cross-threading with UI thread
                Application.Current.Dispatcher.Invoke(() =>
                    Messages.Add(chat.Message));
            }
        }

        bool CanBanUser(object o)
        {
            var user = o as User;

            if (user == null)
            {
                return false;
            }

            if (!UserRank.HasValue)
            {
                return false;
            }

            if (_StaffCache.ContainsKey(user.Id) && UserRank <= _StaffCache[user.Id])
            {
                return false;
            }

            return true;
        }

        void DoBanUser(object o)
        {
            var user = o as User;
            Services.GetService<ChatClient>().Send(ModifyBan.BanUser(ChatRoom.Id, user?.NickName), rtn =>
            {
                var response = rtn as BoolResponse;

                if (response != null)
                {
                    ServerMessage = response.Message;
                }
                else
                {
                    ServerMessage = "Did not receive a valid ban response.";
                }
            });
        }

        void SendChatRoomMessage(object o = null)
        {
            var client = Services.GetService<ChatClient>();
            client.Send((MessageChatRoom.CreateCommand(ChatRoom.Id, client.User.Id, Message)));
            Message = string.Empty;
        }

        void OpenModifyChatRoom(object o = null)
        {
            if (UserRank.HasValue)
            {
                var modifyVm = MainViewModel.ActiveViewModels.FirstOrDefault(
                    vm => vm is ModifyChatRoomViewModel && (vm as ModifyChatRoomViewModel).ChatRoom.Id == ChatRoom.Id)
                    ?? new ModifyChatRoomViewModel(MainViewModel, ChatRoom);

                MainViewModel.AddActiveViewModel(modifyVm, true);
            }
        }

        /// <summary>
        /// Open up a new window to private message the selected user
        /// </summary>
        /// <param name="receivingUser"></param>
        private void OpenPrivateMessage(object receivingUser)
        {
            var User = receivingUser as User;

            PrivateMessageViewModel PrivateViewModel;
            MainViewModel.PrivateMessages.TryGetValue(User.Id, out PrivateViewModel);

            //If there's no private window opened for this user, make a new one
            if (PrivateViewModel == null)
            {
                PrivateViewModel = new PrivateMessageViewModel(MainViewModel, User);
                MainViewModel.PrivateMessages.Add(User.Id, PrivateViewModel);
                MainViewModel.AddActiveViewModel(PrivateViewModel, true);
            }
        }
    }
}
