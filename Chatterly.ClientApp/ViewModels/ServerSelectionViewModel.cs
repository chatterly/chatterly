﻿using Chatterly.Client;
using Chatterly.Client.Database;
using Chatterly.Core;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Chatterly.ClientApp.ViewModels
{
    public class ServerSelectionViewModel : BaseViewModel
    {
        public MainViewModel MainViewModel
        {
            get
            {
                return _MainViewModel;
            }
        }
        readonly MainViewModel _MainViewModel;

        /// <summary>
        /// Command that connects to the selected server.
        /// </summary>
        public ICommand Connect
        {
            get
            {
                return _ConnectCommand;
            }
        }
        readonly RelayCommand _ConnectCommand;

        public ConnectionConfiguration SelectedConnection
        {
            get
            {
                return _SelectedConnection;
            }
            set
            {
                _SelectedConnection = value;
                RaisePropertyChanged();
                _ConnectCommand.RaiseCanExecuteChanged();
            }
        }
        ConnectionConfiguration _SelectedConnection;

        /// <summary>
        /// List of servers previously connected to.
        /// </summary>
        public List<ConnectionConfiguration> Connections { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ServerSelectionViewModel(MainViewModel mainViewModel)
        {
            _MainViewModel = mainViewModel;
            Name = "Server Manager";
            _ConnectCommand = new RelayCommand(ConnectToServer, CanConnectToServer);
        }

        private void ConnectToServer(object window)
        {
            try
            {
                var client = Services.GetService<ChatClient>();
                client.Config = SelectedConnection;
                if (client.Start())
                {
                    var authViewModel = MainViewModel.ViewModelOptions.FirstOrDefault(
                        vm => vm.GetType() == typeof(AuthenticationViewModel)) ?? new AuthenticationViewModel(MainViewModel);
                    MainViewModel.AddActiveViewModel(authViewModel, true);
                }
                else
                {
                    // raise some message to user
                }
            }
            catch
            {
                // raise some message to user
            }
        }

        private bool CanConnectToServer(object o)
        {
            return SelectedConnection != null;
        }

        public override void OnActive()
        {
            using (var db = new ClientContext())
            {
                Connections = db.ConnectionConfigurations.ToList();
            }
        }
    }
}
