﻿using System;

namespace Chatterly.ClientApp
{
    public abstract class BaseViewModel : ObservableObject, IViewModel
    {
        public Guid Id
        {
            get
            {
                return _Id;
            }
        }
        readonly Guid _Id = Guid.NewGuid();

        public virtual string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                if (value != _Name)
                {
                    _Name = value;
                    RaisePropertyChanged();
                }
            }
        }
        string _Name;

        public virtual void OnActive() { }
        public virtual void OnRemove() { }
    }
}
