﻿using Chatterly.Core.Database;
using Chatterly.Core.Tests.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Chatterly.Core.Tests
{
    [TestClass]
    public class LogManagerTest
    {
        [TestMethod]
        public void LogManager_Add()
        {
            var log = new LogManager(new TestDbContext());

            var type = LogType.Info;
            var msg = "Test message!";
            var time = DateTime.UtcNow;

            log.Add(type, msg);

            using (var db = new TestDbContext())
            {
                var entry = db.Logs.LastOrDefault(l => l.Message == msg);

                Assert.IsNotNull(entry, "Log must be saved to database.");
                Assert.AreNotEqual(0, entry.Id, "Id must be set by database.");
                Assert.AreEqual(type, entry.Type, "Type must be same.");
                Assert.AreEqual(msg, entry.Message, "Message must be same.");
                Assert.AreEqual(time.Ticks, entry.Time.Ticks, 100, "Time must be set to current UTC.");
            }
        }
    }
}
