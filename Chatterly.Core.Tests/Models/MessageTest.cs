﻿using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Tests.Models
{
    [TestClass]
    public class MessageTest
    {
        [TestMethod]
        public void Message_Model()
        {
            var id = 7;
            var userId = 11;
            var user = new User
            {
                Id = userId,
                NickName = "Test User",
                EncryptedPassword = "test password",
                Created = DateTime.UtcNow
            };
            var msg = "This is a test message.";
            var created = DateTime.UtcNow;

            var message = new Message
            {
                Id = id,
                UserId = userId,
                User = user,
                Content = msg,
                Time = created
            };

            // first, let's make sure that model has the expected primary key
            var keys = typeof(Message).GetProperty(nameof(message.Id)).GetCustomAttributes(typeof(KeyAttribute), true);

            Assert.AreEqual(keys.Length, 1, "Message must have only one primary key.");

            var key = keys[0] as KeyAttribute;

            Assert.IsNotNull(key, string.Format("{0} must be Message's only primary key.", nameof(message.Id)));

            Assert.AreEqual(id, message.Id, "Id Property must match.");
            Assert.AreEqual(userId, message.UserId, "UserId Property must match.");
            Assert.AreEqual(user, message.User, "User Property must match.");
            Assert.AreEqual(msg, message.Content, "Content Property must match.");
            Assert.AreEqual(created, message.Time, "Time Property must match.");

            var data = message.Serialize();

            var newMessage = new Message();
            newMessage.Deserialize(data);

            Assert.AreEqual(id, newMessage.Id, "Id Property must match after serialization and deserialization.");
            Assert.AreEqual(userId, newMessage.UserId, "UserId Property must match after serialization and deserialization.");

            // object won't be the same, so must perform property-wise comparison
            Assert.AreEqual(user.Id, newMessage.User.Id, "User.Id Property must match after serialization and deserialization.");
            Assert.AreEqual(user.NickName, newMessage.User.NickName,
                "User.NickName Property must match after serialization and deserialization.");
            Assert.AreEqual(user.EncryptedPassword, newMessage.User.EncryptedPassword,
                "User.EncryptedPassword Property must match after serialization and deserialization.");
            Assert.AreEqual(user.Created, newMessage.User.Created,
                "User.Created Property must match after serialization and deserialization.");

            Assert.AreEqual(msg, newMessage.Content, "Content Property must match after serialization and deserialization.");
            Assert.AreEqual(created, newMessage.Time, "Time Property must match after serialization and deserialization.");
        }
    }
}
