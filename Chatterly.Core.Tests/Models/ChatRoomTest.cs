﻿using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Tests.Models
{
    [TestClass]
    public class ChatRoomTest
    {
        [TestMethod]
        public void ChatRoom_Model()
        {
            var id = 7;
            var name = "Test Chatroom";
            var desc = "Test description.";
            var created = DateTime.UtcNow;

            var chatroom = new ChatRoom
            {
                Id = id,
                Name = name,
                Description = desc,
                Created = created
            };

            // first, let's make sure that model has the expected primary key
            var keys = typeof(ChatRoom).GetProperty(nameof(chatroom.Id)).GetCustomAttributes(typeof(KeyAttribute), true);

            Assert.AreEqual(keys.Length, 1, "ChatRoom must have only one primary key.");

            var key = keys[0] as KeyAttribute;

            Assert.IsNotNull(key, string.Format("{0} must be ChatRoom's only primary key.", nameof(chatroom.Id)));

            Assert.AreEqual(id, chatroom.Id, "Id Property must match.");
            Assert.AreEqual(name, chatroom.Name, "Name Property must match.");
            Assert.AreEqual(desc, chatroom.Description, "Description Property must match.");
            Assert.AreEqual(created, chatroom.Created, "Created Property must match.");

            var data = chatroom.Serialize();

            var newChatroom = new ChatRoom();
            newChatroom.Deserialize(data);

            Assert.AreEqual(id, newChatroom.Id, "Id Property must match after serialization and deserialization.");
            Assert.AreEqual(name, newChatroom.Name, "Name Property must match after serialization and deserialization.");
            Assert.AreEqual(desc, newChatroom.Description,
                "Description Property must match after serialization and deserialization.");
            Assert.AreEqual(created, newChatroom.Created,
                "Created Property must match after serialization and deserialization.");
        }
    }
}
