﻿using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Tests.Models
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void User_Model()
        {
            var id = 7;
            var nickname = "Test User";
            var password = "Test Password.";
            var created = DateTime.UtcNow;

            var user = new User
            {
                Id = id,
                NickName = nickname,
                EncryptedPassword = password,
                Created = created
            };

            // first, let's make sure that model has the expected primary key
            var keys = typeof(ChatRoom).GetProperty(nameof(user.Id)).GetCustomAttributes(typeof(KeyAttribute), true);

            Assert.AreEqual(keys.Length, 1, "User must have only one primary key.");

            var key = keys[0] as KeyAttribute;

            Assert.IsNotNull(key, string.Format("{0} must be User's only primary key.", nameof(user.Id)));

            Assert.AreEqual(id, user.Id, "Id Property must match.");
            Assert.AreEqual(nickname, user.NickName, "NickName Property must match.");
            Assert.AreEqual(password, user.EncryptedPassword, "EncryptedPassword Property must match.");
            Assert.AreEqual(created, user.Created, "Created Property must match.");

            var data = user.Serialize();

            var newChatroom = new User();
            newChatroom.Deserialize(data);

            Assert.AreEqual(id, newChatroom.Id, "Id Property must match after serialization and deserialization.");
            Assert.AreEqual(nickname, newChatroom.NickName, "NickName Property must match after serialization and deserialization.");
            Assert.AreEqual(password, newChatroom.EncryptedPassword,
                "EncryptedPassword Property must match after serialization and deserialization.");
            Assert.AreEqual(created, newChatroom.Created,
                "Created Property must match after serialization and deserialization.");
        }
    }
}
