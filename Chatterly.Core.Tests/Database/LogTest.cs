﻿using Chatterly.Core.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Tests.Database
{
    [TestClass]
    public class LogTest
    {
        [TestMethod]
        public void Log_DatabaseModel()
        {
            var id = 7;
            var type = LogType.Info;
            var message = "Test Message.";
            var time = DateTime.UtcNow;

            var log = new Log
            {
                Id = id,
                Type = type,
                Time = time,
                Message = message
            };

            // first, let's make sure that model has the expected primary key
            var keys = typeof(Log).GetProperty(nameof(log.Id)).GetCustomAttributes(typeof(KeyAttribute), true);

            Assert.AreEqual(keys.Length, 1, "Log must have only one primary key.");

            var key = keys[0] as KeyAttribute;

            Assert.IsNotNull(key, string.Format("{0} must be Log's only primary key.", nameof(log.Id)));

            Assert.AreEqual(id, log.Id, "Id Property must match.");
            Assert.AreEqual(type, log.Type, "Type Property must match.");
            Assert.AreEqual(time, log.Time, "Time Property must match.");
            Assert.AreEqual(message, log.Message, "Message Property must match.");
        }
    }
}
