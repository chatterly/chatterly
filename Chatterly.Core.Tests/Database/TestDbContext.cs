﻿using Chatterly.Core.Database;
using Microsoft.EntityFrameworkCore;

namespace Chatterly.Core.Tests.Database
{
    public class TestDbContext : ChatterlyContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./Chatterly.Core.Tests.db");
        }
    }
}
