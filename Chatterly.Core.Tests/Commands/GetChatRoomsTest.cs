﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;


namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class GetChatRoomsTest
    {
        [TestMethod()]
        public void GetChatRooms_Commands()
        {
            var chatroom = new ChatRoom { };
            var chatrooms = new List<GetChatRooms.ChatRoomMeta>()
            {            
                new GetChatRooms.ChatRoomMeta
                {
                    ChatRoom =  new ChatRoom
                    {
                        Id = 1,
                        Name = "Test Chatroom 1",
                        Description = "Test Description 1",
                        IsPrivate = false,
                    },
                    UserCount = 1,
                },
                new GetChatRooms.ChatRoomMeta
                {
                    ChatRoom = new ChatRoom
                    {
                        Id = 3,
                        Name = "Test Chatroom 3",
                        Description = "Test Description 3",
                        IsPrivate = true,
                    },
                    UserCount = 2,
                },
                new GetChatRooms.ChatRoomMeta
                {
                      ChatRoom = new ChatRoom
                    {
                        Id = 3,
                        Name = "Test Chatroom 3",
                        Description = "Test Description 3",
                        IsPrivate = false,
                    },
                       UserCount = 3,
                },
            };

            var cmd1 = new GetChatRooms
            {
                ChatRooms = chatrooms,
            };

            foreach (var pair in chatrooms.Zip(cmd1.ChatRooms, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.ChatRoom.Id, pair.Item2.ChatRoom.Id, "ChatRoomId property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.Name, pair.Item2.ChatRoom.Name, "ChatRoom Name property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.Description, pair.Item2.ChatRoom.Description, "ChatRoom Description property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.IsPrivate, pair.Item2.ChatRoom.IsPrivate, "ChatRoom isPrivate property must be the same.");
                Assert.AreEqual(pair.Item1.UserCount, pair.Item2.UserCount, "UserCount property must be the same.");
            }


            var payload = new Payload(cmd1);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);
    
            var newCmd = new GetChatRooms();
            newCmd.ParsePayload(newPayload);

            foreach (var pair in cmd1.ChatRooms.Zip(newCmd.ChatRooms, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.ChatRoom.Id, pair.Item2.ChatRoom.Id, "ChatRoomId property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.Name, pair.Item2.ChatRoom.Name, "ChatRoom Name property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.Description, pair.Item2.ChatRoom.Description, "ChatRoom Description property must be the same.");
                Assert.AreEqual(pair.Item1.ChatRoom.IsPrivate, pair.Item2.ChatRoom.IsPrivate, "ChatRoom isPrivate property must be the same.");
                Assert.AreEqual(pair.Item1.UserCount, pair.Item2.UserCount, "UserCount property must be the same.");
            }
        }      
    }
}