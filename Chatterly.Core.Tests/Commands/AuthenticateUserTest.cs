﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class AuthenticateUserTest
    {
        [TestMethod]
        public void AuthenticateUser_Command()
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                User.Encryptor = new RsaCrypto(rsa.ToXmlString(true));
            }

            var isAuth = false;
            var user = new User
            {
                Id = 3,
                NickName = "Test User",
                EncryptedPassword = "test pass",
                Created = DateTime.UtcNow
            };

            var encryptedPass = User.EncryptPassword(user.EncryptedPassword);

            var cmd = new AuthenticateUser
            {
                IsAuthenticated = isAuth,
                User = user
            };

            Assert.AreEqual(isAuth, cmd.IsAuthenticated, "IsAuthenticated property must be same.");
            Assert.AreEqual(user, cmd.User, "User property must be same.");

            var cmd1 = AuthenticateUser.CreateCommand(user.NickName, user.EncryptedPassword, false);

            Assert.AreEqual(cmd.IsAuthenticated, cmd1.IsAuthenticated, "IsAuthenticated property must be same.");
            Assert.AreEqual(cmd.User.NickName, cmd1.User.NickName, "User.NickName property must be same.");
            Assert.AreEqual(cmd.User.EncryptedPassword, cmd1.User.EncryptedPassword,
                "User.EncryptedPassword property must be same.");

            var cmd2 = AuthenticateUser.CreateCommand(user.NickName, user.EncryptedPassword);

            Assert.AreEqual(cmd.IsAuthenticated, cmd2.IsAuthenticated, "IsAuthenticated property must be same.");
            Assert.AreEqual(cmd.User.NickName, cmd2.User.NickName, "User.NickName property must be same.");
            Assert.AreEqual(cmd.User.EncryptedPassword,
                Encoding.UTF8.GetString(User.Encryptor.Decrypt(Convert.FromBase64String(cmd2.User.EncryptedPassword))),
                "User.EncryptedPassword property must be same.");

            // ensure that an encrypted password survives the (de)serialization process
            cmd.User.EncryptedPassword = User.EncryptPassword(cmd.User.EncryptedPassword);

            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);

            var newCmd = new AuthenticateUser();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.IsAuthenticated, newCmd.IsAuthenticated, "IsAuthenticated property must be same.");
            Assert.AreEqual(cmd.User.Id, newCmd.User.Id, "User.Id property must be same.");
            Assert.AreEqual(cmd.User.NickName, newCmd.User.NickName, "User.NickName property must be same.");
            Assert.AreEqual(cmd.User.Created, newCmd.User.Created, "User.Created property must be same.");
            Assert.AreEqual(cmd.User.EncryptedPassword, newCmd.User.EncryptedPassword, "User.EncryptedPassword property must be same.");
        }
    }
}
