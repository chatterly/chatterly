﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class GetChatRoomStaffTest
    {
        [TestMethod]
        public void GetChatRoomStaff_Test()
        {
            int chatRoomId = 1;

            var cmd = GetChatRoomStaff.CreateCommand(chatRoomId);

            var get = cmd as GetChatRoomStaff;

            Assert.AreEqual(chatRoomId, get.ChatRoomId, "Casting should work fine.");

            var staff = new List<ChatRoomStaff>
            {
                new ChatRoomStaff
                {
                    ChatRoomId = chatRoomId,
                    User = new User
                    {
                        NickName = "Test1"
                    }
                },
                new ChatRoomStaff
                {
                    ChatRoomId = chatRoomId,
                    User = new User
                    {
                        NickName = "Test2"
                    }
                },
                new ChatRoomStaff
                {
                    ChatRoomId = chatRoomId,
                    User = new User
                    {
                        NickName = "Test3"
                    }
                }
            };

            get.Staff = staff;

            Assert.AreEqual(staff.Count, get.Staff.Count);

            var payload = new Payload(get);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newGet = new GetChatRoomStaff();
            newGet.ParsePayload(newPayload);

            Assert.AreEqual(get.ChatRoomId, newGet.ChatRoomId, "ChatRoomId must survive deserialization process.");
            Assert.AreEqual(get.Staff.Count, newGet.Staff.Count, "Staff must survive deserialization process.");

            foreach (var pair in get.Staff.Zip(newGet.Staff, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.ChatRoomId, pair.Item2.ChatRoomId,
                    "Chatroom information must survive deserialization process.");
                Assert.AreEqual(pair.Item1.User.NickName, pair.Item2.User.NickName,
                    "User information must survive the deserialization process.");
            }
        }
    }
}
