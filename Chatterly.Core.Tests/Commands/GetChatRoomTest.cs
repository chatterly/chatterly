﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class GetChatRoomTest
    {
        [TestMethod()]
        public void GetChatRoom_Command()
        {

            // CreateCommand()
            var Chatroom = new ChatRoom()
            {
                Id = 1,
                Name = "Test Name",
                Created = DateTime.UtcNow,
                Description = "Test Description"
            };
          
            var cmd = GetChatRoom.CreateCommand(Chatroom.Id);
            Assert.AreEqual(Chatroom.Id, cmd.ChatRoom.Id, "Chatroom ID must be the same.");

            var user1 = new User()
            {
                Id = 1,
                NickName = "Test User",
            };

            // Test user list
            var users = new List<User>()
            {
                new User
                {
                     Id = 1,
                    NickName = "Test User1",
                },
                new User
                {
                    Id = 2,
                    NickName = "Test User2",
                },
                new User
                {
                    Id = 3,
                    NickName = "Test User3",
                }
            };           
            cmd.Users = users;


            // CreatePayloadData() / CreatePrimitiveData()
            var payload = new Payload
            {
                Messages = cmd.CreatePayloadData(),
                PrimitiveData = cmd.CreatePrimitiveData(),
            };


            // ParsePayload()
            var newCmd = new GetChatRoom();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(Chatroom.Id, newCmd.ChatRoom.Id, "ChatRoom ID Property must be the same.");
            foreach (var pair in users.Zip(newCmd.Users, (a,b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.NickName, pair.Item2.NickName, "");
                Assert.AreEqual(pair.Item1.Id, pair.Item2.Id, "");
            }
        }
    }
}
