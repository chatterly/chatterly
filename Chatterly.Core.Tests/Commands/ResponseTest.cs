﻿using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class ResponseTest
    {
        [TestMethod()]
        public void Response_Command()
        {
            var message = "Test Message";

            var cmd = new Response
            {
                Message = message
            };

            Assert.AreEqual(message, cmd.Message, "Message property must be the same.");

            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);

            // ParsePayLoad()
            var newCmd = new Response();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.Message, newCmd.Message, "Message property must be the same.");
        }

    }
}