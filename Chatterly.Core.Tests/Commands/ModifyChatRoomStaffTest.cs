﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class ModifyChatRoomStaffTest
    {
        [TestMethod()]
        public void ModifyChatRoomStaff_Command()
        {

            var chatroomid = 1;
            var nickname = "Test Nickname";
            var rank = ChatRoomRank.Owner;

            var cmd = new ModifyChatRoomStaff
            {
                ChatRoomId = chatroomid,
                Nickname = nickname,
                Rank = rank
            };

            Assert.AreEqual(chatroomid, cmd.ChatRoomId, "ChatRoomID property must be the same.");
            Assert.AreEqual(nickname, cmd.Nickname, "Nickname property must be the same.");
            Assert.AreEqual(rank, cmd.Rank, "Rank property must be the same");


            // CreateCommand()
            var cmd1 = ModifyChatRoomStaff.CreateCommand(chatroomid, nickname, rank) as ModifyChatRoomStaff;
            Assert.AreEqual(cmd.ChatRoomId, cmd1.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(cmd.Nickname, cmd1.Nickname, "Nickname property must be the same.");
            Assert.AreEqual(cmd.Rank, cmd1.Rank, "Rank Property must be the same.");


            // CreatePrimitiveData()
            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);


            // ParsePayload()
            var newCmd = new ModifyChatRoomStaff();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.ChatRoomId, newCmd.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(cmd.Nickname, newCmd.Nickname, "Nickname property must be the same.");
            Assert.AreEqual(cmd.Rank, newCmd.Rank, "Rank Property must be the same.");


            // ParsePayload() if rank = null
            var cmd2 = new ModifyChatRoomStaff
            {
                ChatRoomId = chatroomid,
                Nickname = nickname,
                Rank = null
            };

            var payload1 = new Payload(cmd2);
            data = (payload1 as IMessage).Serialize();
            payload1 = new Payload(data);

            var newCmd1 = new ModifyChatRoomStaff();
            newCmd1.ParsePayload(payload1);

            Assert.AreEqual(cmd2.ChatRoomId, newCmd1.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(cmd2.Nickname, newCmd1.Nickname, "Nickname property must be the same.");
            Assert.AreEqual(cmd2.Rank, newCmd1.Rank, "Rank Property must be the same.");
        }
    }
}