﻿using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chatterly.Core.Commands.Test
{
    [TestClass]
    public class CreateChatRoomTest
    {
        [TestMethod]
        public void CreateChatRoom_Command()
        {
            var name = "Test Chatroom";
            var desc = "Test Description";

            var cmd = CreateChatRoom.CreateCommand(name, desc) as CreateChatRoom;

            Assert.IsNotNull(cmd.ChatRoom, "Should have a ChatRoom object.");
            Assert.AreEqual(name, cmd.ChatRoom.Name, "Name property must be the same.");
            Assert.AreEqual(desc, cmd.ChatRoom.Description, "Description property must be the same.");

            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);


            var newCmd = new CreateChatRoom();
            newCmd.ParsePayload(payload);

            Assert.IsNotNull(newCmd.ChatRoom, "Should have a ChatRoom object.");
            Assert.AreEqual(cmd.ChatRoom.Name, newCmd.ChatRoom.Name, "Name property must be the same.");
            Assert.AreEqual(cmd.ChatRoom.Description, newCmd.ChatRoom.Description, "Description property must be the same.");
        }
    }
}