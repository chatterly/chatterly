﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class GetUsersTest
    {
        [TestMethod]
        public void GetUsers_Test()
        {
            string startsWith = "test";

            var cmd = GetUsers.CreateCommand(startsWith);

            var get = cmd as GetUsers;

            Assert.AreEqual(startsWith, get.StartsWith, "Casting should work fine.");

            var users = new List<User>
            {
                new User
                {
                    NickName = "Test1"
                },
                new User
                {
                    NickName = "Test2"
                },
                new User
                {
                    NickName = "Test3"
                }
            };

            get.Users = users;

            Assert.AreEqual(users.Count, get.Users.Count);

            var payload = new Payload(get);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newGet = new GetUsers();
            newGet.ParsePayload(newPayload);

            Assert.AreEqual(get.StartsWith, newGet.StartsWith, "StartsWith must survive deserialization process.");
            Assert.AreEqual(get.Users.Count, newGet.Users.Count, "Users must survive deserialization process.");

            foreach (var pair in get.Users.Zip(newGet.Users, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.NickName, pair.Item2.NickName,
                    "User information must survive deserialization process.");
            }
        }
    }
}
