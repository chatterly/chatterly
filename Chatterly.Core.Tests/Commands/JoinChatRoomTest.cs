﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class JoinChatRoomTest
    {
        [TestMethod()]
        public void JoinChatRoom_Command()
        {

            var joinedfalse = false;
            var joinedtrue = true;
            var chatroom = new ChatRoom()
            {
                Id = 1,
                Name = "Test ChatRoom"
            };

            var cmd = new JoinChatRoom()
            {
                ChatRoom = chatroom,
                Joined = joinedfalse
            };

            Assert.AreEqual(chatroom.Id, cmd.ChatRoom.Id, "ChatRoom ID property must be the same.");
            Assert.AreEqual(chatroom.Name, cmd.ChatRoom.Name, "ChatRoom property name must be the same.");
            Assert.AreEqual(joinedfalse, cmd.Joined, "Joined property must be the same.");

            var cmd1 = JoinChatRoom.CreateCommand(chatroom.Id);
            cmd1.Joined = true;

            Assert.AreEqual(chatroom.Id, cmd1.ChatRoom.Id, "ChatRoom ID property must be the same.");
            Assert.AreEqual(joinedtrue, cmd1.Joined, "Joined property must be the same.");

            var payload = new Payload
            {
                Messages = cmd1.CreatePayloadData(),
                PrimitiveData = cmd1.CreatePrimitiveData()
            };

            var newCmd = new JoinChatRoom();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd1.ChatRoom.Id, newCmd.ChatRoom.Id, "ChatRoom property must be the same.");
            Assert.AreEqual(cmd1.Joined, newCmd.Joined, "Joined property must be the same.");
        }
    }
}