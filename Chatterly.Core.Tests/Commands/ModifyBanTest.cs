﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class ModifyBanTest
    {
        [TestMethod]
        public void ModifyBan_Test()
        {
            var chatRoomId = 1;
            var nick = "Test nickname";

            var cmdBan = ModifyBan.BanUser(chatRoomId, nick);
            var ban = cmdBan as ModifyBan;

            Assert.AreEqual(chatRoomId, ban.ChatRoomId);
            Assert.AreEqual(nick, ban.Nickname);
            Assert.AreEqual(BanAction.Ban, ban.Action);

            var cmdUnban = ModifyBan.UnbanUser(chatRoomId, nick);
            var unban = cmdUnban as ModifyBan;

            Assert.AreEqual(chatRoomId, unban.ChatRoomId);
            Assert.AreEqual(nick, unban.Nickname);
            Assert.AreEqual(BanAction.Unban, unban.Action);

            var payload = new Payload(ban);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newCmd = new ModifyBan();
            newCmd.ParsePayload(newPayload);

            Assert.AreEqual(ban.ChatRoomId, newCmd.ChatRoomId);
            Assert.AreEqual(ban.Nickname, newCmd.Nickname);
            Assert.AreEqual(ban.Action, newCmd.Action);

            payload = new Payload(unban);
            data = (payload as IMessage).Serialize();
            newPayload = new Payload(data);

            newCmd = new ModifyBan();
            newCmd.ParsePayload(newPayload);

            Assert.AreEqual(unban.ChatRoomId, newCmd.ChatRoomId);
            Assert.AreEqual(unban.Nickname, newCmd.Nickname);
            Assert.AreEqual(unban.Action, newCmd.Action);
        }
    }
}
