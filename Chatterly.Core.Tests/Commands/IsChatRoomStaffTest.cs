﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class IsChatRoomStaffTest
    {
        [TestMethod()]
        public void IsChatRoomStaff_Command()
        {

            // Initial Vars
            var user = new User()
            {
                NickName = "Test name.",
                Id = 1,
            };

            var chatroom = new ChatRoom()
            {
                Id = 1,
                Name = "Test chatroom.",
            };

            var rank = ChatRoomRank.Owner;

            var cmd = new IsChatRoomStaff
            {
                Rank = ChatRoomRank.Owner,
                ChatRoomId = chatroom.Id,
                User = user,
            };

            Assert.AreEqual(rank, cmd.Rank, "Rank property must be the same.");
            Assert.AreEqual(user.Id, cmd.User.Id, "UserId property must be the same.");
            Assert.AreEqual(user.NickName, cmd.User.NickName, "NickName property must be the same.");
            Assert.AreEqual(chatroom.Id, cmd.ChatRoomId, "ChatRoomId property must be the same.");

            // CreateCommand()
            var cmd1 = IsChatRoomStaff.CreateCommand(chatroom.Id, user.NickName) as IsChatRoomStaff;
            cmd1.Rank = rank; cmd1.User.Id = user.Id;

            Assert.AreEqual(rank, cmd1.Rank, "Rank property must be the same.");
            Assert.AreEqual(user.Id, cmd1.User.Id,"UserId property must be the same.");
            Assert.AreEqual(user.NickName, cmd1.User.NickName, "NickName property must be the same.");
            Assert.AreEqual(chatroom.Id, cmd1.ChatRoomId, "ChatRoomId property must be the same.");

            // CreatePrimitiveData()
            var payload = new Payload
            {
                Messages = cmd1.CreatePayloadData(),
                PrimitiveData = cmd1.CreatePrimitiveData()
            };

            // ParsePayload()
            var newCmd = new IsChatRoomStaff();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(rank, newCmd.Rank, "Rank property must be the same.");
            Assert.AreEqual(user.Id, newCmd.User.Id, "UserId property must be the same.");
            Assert.AreEqual(user.NickName, newCmd.User.NickName, "NickName property must be the same.");
            Assert.AreEqual(chatroom.Id, newCmd.ChatRoomId, "ChatRoomId property must be the same.");

            // ParsePayload() if rank = null
            var cmd2 = new IsChatRoomStaff()
            {
                Rank = null,
                ChatRoomId = chatroom.Id,
                User = user,
            };

            var payload1 = new Payload
            {
                Messages = cmd2.CreatePayloadData(),
                PrimitiveData = cmd2.CreatePrimitiveData()
            };

            var newCmd1 = new IsChatRoomStaff();
            newCmd1.ParsePayload(payload1);

            Assert.AreEqual(cmd2.Rank, newCmd1.Rank, "Rank Property must be the same.");
            Assert.AreEqual(cmd2.User.Id, newCmd.User.Id, "UserId property must be the same.");
            Assert.AreEqual(cmd2.User.NickName, newCmd1.User.NickName, "Nickname property must be the same.");
            Assert.AreEqual(cmd2.ChatRoomId, newCmd1.ChatRoomId, "ChatRoomId property must be the same.");

        }
    }
}