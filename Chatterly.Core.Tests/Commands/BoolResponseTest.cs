﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class BoolResponseTest
    {
        [TestMethod()]
        public void BoolResponse_Command()
        {
            var message = "Test Message";
            var result = true;

            var cmd = new BoolResponse
            {
                Message = message,
                Result = result
                
            };

            Assert.AreEqual(message, cmd.Message, "Message property must be the same.");
            Assert.AreEqual(result, cmd.Result, "Result property must be the same.");

            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);

            var newCmd = new BoolResponse();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.Message, newCmd.Message, "Message property must be the same.");
            Assert.AreEqual(cmd.Result, cmd.Result, "Result property must be the same.");
        }
    }
}