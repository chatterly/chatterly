﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;


namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class MessageUserTest
    {
        [TestMethod()]
        public void MessageUser_Commands()
        {

            var FromUser = 2;
            var ToUser = 3;

            var content = "Test Content";
            var time = DateTime.UtcNow;

            var cmd = new Message()
            {
                Content = content,
                Time = time,
                UserId = FromUser,
            };

            var cmd1 = new MessageUser()
            {
                RecipientUserId = ToUser,
                Message = cmd,
            };
            
            Assert.AreEqual(ToUser, cmd1.RecipientUserId, "RecipientUserId property must be the same.");
            Assert.AreEqual(cmd.Content, cmd1.Message.Content, "Message property must be the same.");
            Assert.AreEqual(cmd.UserId, cmd1.Message.UserId, "UserId property must be the same.");
            Assert.AreEqual(cmd.Time, cmd1.Message.Time, "Time property must be the same.");


            // CreateCommand()
            var newcmd = MessageUser.CreateCommand(ToUser, content) as MessageUser;
            (newcmd as MessageUser).Message.UserId = cmd.UserId;

            Assert.AreEqual(ToUser, newcmd.RecipientUserId, "RecipientUserId property must be the same.");
            Assert.AreEqual(cmd.Content, newcmd.Message.Content, "Message property must be the same.");
            Assert.AreEqual(cmd.UserId, newcmd.Message.UserId, "UserId property must be the same.");

            // CreatePayloadData() / CreatePrimitiveData()
            var payload = new Payload
            {
                Messages = newcmd.CreatePayloadData(),
                PrimitiveData = newcmd.CreatePrimitiveData(),
            };


            // ParsePayload()
            var newcmd1 = new MessageUser();
            newcmd1.ParsePayload(payload);

            Assert.AreEqual(newcmd.RecipientUserId, newcmd1.RecipientUserId, "RecipientUserId property must be the same.");
            Assert.AreEqual(newcmd.Message.Content, newcmd1.Message.Content, "Message property must be the same.");
            Assert.AreEqual(newcmd.Message.UserId, newcmd1.Message.UserId, "UserId property must be the same.");
        }
    }
}