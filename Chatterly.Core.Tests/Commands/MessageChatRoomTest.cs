﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;


namespace Chatterly.Core.Commands.Test
{
    [TestClass()]
    public class MessageChatRoomTest
    {
        [TestMethod()]
        public void MessageChatRoom_Commands()
        {

            var chatroomid = 1;
            var content = "Test Content";
            var userid = 1;
            var time = DateTime.UtcNow;

            var cmd = new Message()
            {
                Content = content,
                Time = time,
                UserId = userid,
            };

            var cmd1 = new MessageChatRoom()
            {
                ChatRoomId = chatroomid,
                Message = cmd,
            };

            Assert.AreEqual(chatroomid, cmd1.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(cmd.Content, cmd1.Message.Content, "Message property must be the same.");
            Assert.AreEqual(cmd.UserId, cmd1.Message.UserId, "UserId property must be the same.");
            Assert.AreEqual(cmd.Time, cmd1.Message.Time, "Time property must be the same.");


            // CreateCommand()
            var newcmd = MessageChatRoom.CreateCommand(chatroomid, userid, content) as MessageChatRoom;
            Assert.AreEqual(chatroomid, newcmd.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(cmd.Content, newcmd.Message.Content, "Message property must be the same.");
            Assert.AreEqual(cmd.UserId, newcmd.Message.UserId, "UserId property must be the same.");

            // CreatePayloadData() / CreatePrimitiveData()
            var payload = new Payload
            {
                Messages = newcmd.CreatePayloadData(),
                PrimitiveData = newcmd.CreatePrimitiveData(),
            };


            // ParsePayload()
            var newcmd1 = new MessageChatRoom();
            newcmd1.ParsePayload(payload);

            Assert.AreEqual(newcmd.ChatRoomId, newcmd1.ChatRoomId, "ChatRoomId property must be the same.");
            Assert.AreEqual(newcmd.Message.Content, newcmd1.Message.Content, "Message property must be the same.");
            Assert.AreEqual(newcmd.Message.UserId, newcmd1.Message.UserId, "UserId property must be the same.");
        }
    }
}