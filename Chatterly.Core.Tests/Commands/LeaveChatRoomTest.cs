using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class LeaveChatRoomTest
    {
        [TestMethod]
        public void LeaveChatRoom_Test()
        {
            var chatRoomId = 1;
            var cmd = LeaveChatRoom.CreateCommand(chatRoomId);
            var leave = cmd as LeaveChatRoom;

            Assert.AreEqual(chatRoomId, leave.ChatRoom.Id);

            var chatRoomName = "Test name";
            var chatRoomDesc = "Test desc";

            leave.ChatRoom.Name = chatRoomName;
            leave.ChatRoom.Description = chatRoomDesc;

            var payload = new Payload(leave);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newLeave = new LeaveChatRoom();
            newLeave.ParsePayload(newPayload);

            Assert.AreEqual(leave.ChatRoom.Id, newLeave.ChatRoom.Id);
            Assert.AreEqual(leave.ChatRoom.Name, newLeave.ChatRoom.Name);
            Assert.AreEqual(leave.ChatRoom.Description, newLeave.ChatRoom.Description);
        }
    }
}
