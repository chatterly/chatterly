﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class ModifyChatRoomMembershipTest
    {
        [TestMethod]
        public void ModifyChatRoomMembership_Test()
        {
            var chatRoomId = 1;
            var nick = "Test nickname";

            var cmdAdd = ModifyChatRoomMembership.AddMember(chatRoomId, nick);
            var add = cmdAdd as ModifyChatRoomMembership;

            Assert.AreEqual(chatRoomId, add.ChatRoomId);
            Assert.AreEqual(nick, add.Nickname);
            Assert.AreEqual(ChatRoomMembershipAction.Add, add.Action);

            var cmdRemove = ModifyChatRoomMembership.RemoveMember(chatRoomId, nick);
            var remove = cmdRemove as ModifyChatRoomMembership;

            Assert.AreEqual(chatRoomId, remove.ChatRoomId);
            Assert.AreEqual(nick, remove.Nickname);
            Assert.AreEqual(ChatRoomMembershipAction.Remove, remove.Action);

            var payload = new Payload(add);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newCmd = new ModifyChatRoomMembership();
            newCmd.ParsePayload(newPayload);

            Assert.AreEqual(add.ChatRoomId, newCmd.ChatRoomId);
            Assert.AreEqual(add.Nickname, newCmd.Nickname);
            Assert.AreEqual(add.Action, newCmd.Action);

            payload = new Payload(remove);
            data = (payload as IMessage).Serialize();
            newPayload = new Payload(data);

            newCmd = new ModifyChatRoomMembership();
            newCmd.ParsePayload(newPayload);

            Assert.AreEqual(remove.ChatRoomId, newCmd.ChatRoomId);
            Assert.AreEqual(remove.Nickname, newCmd.Nickname);
            Assert.AreEqual(remove.Action, newCmd.Action);
        }
    }
}
