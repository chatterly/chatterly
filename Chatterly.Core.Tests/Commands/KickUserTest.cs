﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class KicksUserTest
    {
        [TestMethod]
        public void KickUser_Test()
        {
            var chatRoomId = 1;
            var nickname = "Test";

            var kick = new KickUser
            {
                ChatRoomId = chatRoomId,
                Nickname = nickname
            };

            Assert.AreEqual(chatRoomId, kick.ChatRoomId);
            Assert.AreEqual(nickname, kick.Nickname);

            var payload = new Payload(kick);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newKick = new KickUser();
            newKick.ParsePayload(newPayload);

            Assert.AreEqual(kick.ChatRoomId, newKick.ChatRoomId,
                "ChatRoomId must survive deserialization process.");
            Assert.AreEqual(kick.Nickname, newKick.Nickname,
                "ChatRoomId must survive deserialization process.");
        }
    }
}
