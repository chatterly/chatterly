﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Commands.Test
{

    public class BaseCommandTest : BaseCommand
    {
        public override void ParsePayload(Payload msg)
        {
            throw new NotImplementedException();
        }
    }


    [TestClass()]
    public class BaseCommand_Test
    {
        [TestMethod()]
        public void BaseCommand_Commands()
        {
            var guid = Guid.NewGuid();
            var cmd = new BaseCommandTest
            {
                Id = guid,
            };

            Assert.AreEqual(guid, cmd.Id, "GUID property must be the same.");
            Assert.AreEqual(0, cmd.CreatePrimitiveData().Length, "Return Primitive Data length must be the same.");
            Assert.AreEqual(0, cmd.CreatePayloadData().Length, "Return Imessage length must be the same.");
        }       
    }
}