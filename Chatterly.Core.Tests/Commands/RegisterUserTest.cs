﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Cryptography;
using System.Text;


namespace Chatterly.Core.Tests
{
    [TestClass]
    public class RegisterUserTest
    {
        [TestMethod]
        public void RegisterUser_Command()
        {

            using (var rsa = new RSACryptoServiceProvider())
            {
                User.Encryptor = new RsaCrypto(rsa.ToXmlString(true));
            }

            var isregistered = false;
            var nickname = "Test Nickname";
            var password = "Test Password";
            var errormessage = "Test Error";
            var encryptedPass = User.EncryptPassword(password);

            var cmd = new RegisterUser
            {
                //Id = 1,
                IsRegistered = isregistered,
                NickName = nickname,
                Password = password,
                ErrorMessage = errormessage
            };

            Assert.AreEqual(isregistered, cmd.IsRegistered, "IsRegistered property must be the same.");
            Assert.AreEqual(nickname, cmd.NickName, "Nickname property must be the same.");
            Assert.AreEqual(password, cmd.Password, "Password property must be the same.");

            var cmd1 = RegisterUser.CreateCommand(nickname, encryptedPass, false);
            Assert.AreEqual(cmd.IsRegistered, cmd1.IsRegistered, "IsRegistered property must be the same.");
            Assert.AreEqual(cmd.NickName, cmd1.NickName, "Nickname property must be the same.");
            Assert.AreEqual(encryptedPass, cmd1.Password, "Password property must be the same.");


            var cmd2 = RegisterUser.CreateCommand(nickname, password);

            Assert.AreEqual(cmd1.IsRegistered, cmd2.IsRegistered, "IsAuthenticated property must be same.");
            Assert.AreEqual(cmd1.NickName, cmd2.NickName, "User.NickName property must be same.");
            Assert.AreEqual(password,
                Encoding.UTF8.GetString(User.Encryptor.Decrypt(Convert.FromBase64String(cmd2.Password))),
                "Password property must be same.");

            cmd.Password = User.EncryptPassword(cmd.Password);

            // ensure the encrypted password survives the (de)serialization process
            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);

            var newCmd = new RegisterUser();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.IsRegistered, newCmd.IsRegistered, "IsAuthenticated property must be same.");
            Assert.AreEqual(cmd.NickName, newCmd.NickName, "User.NickName property must be same.");
            Assert.AreEqual(cmd.Password, newCmd.Password, "User.EncryptedPassword property must be same.");
            Assert.AreEqual(cmd.IsRegistered, newCmd.IsRegistered, "User.IsRegistered property must be same.");

        }
    }
}
