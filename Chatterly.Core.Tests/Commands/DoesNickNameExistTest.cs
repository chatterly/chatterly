﻿using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chatterly.Core.Commands.Tests
{
    [TestClass()]
    public class DoesNickNameExistTest
    {
        [TestMethod()]
        public void DoesNickNameExist_Command()
        {
            var nickname = "Test NickName";
            var nicknameDoesNotExist = false;

            var cmd = new DoesNickNameExist()
            {
                NickName = nickname
            };

            Assert.AreEqual(nickname, cmd.NickName, "Nickname property must be the same.");
            Assert.AreEqual(nicknameDoesNotExist, cmd.NickNameExists, "NickNameExists property must be the same.");

            var nicknameDoesExist = true;

            var cmd1 = DoesNickNameExist.CreateCommand(nickname) as DoesNickNameExist;
            cmd1.NickNameExists = true;

            Assert.AreEqual(cmd.NickName, cmd1.NickName, "Nickname property must be the same.");
            Assert.AreEqual(nicknameDoesExist, cmd1.NickNameExists, "NickNameExists property must be the same.");


            // ensure that the message survives the (de)serialization process
            var payload = new Payload(cmd);
            var data = (payload as IMessage).Serialize();
            payload = new Payload(data);

            var newCmd = new DoesNickNameExist();
            newCmd.ParsePayload(payload);

            Assert.AreEqual(cmd.NickName, newCmd.NickName, "NickName property must be same.");
            Assert.AreEqual(cmd.NickNameExists, newCmd.NickNameExists, "NickNameExists property must be the same.");
        }

    }
}