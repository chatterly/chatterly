﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class GetChatRoomBansTest
    {
        [TestMethod]
        public void GetChatRoomBans_Test()
        {
            int chatRoomId = 1;

            var cmd = GetChatRoomBans.CreateCommand(chatRoomId);

            var get = cmd as GetChatRoomBans;

            Assert.AreEqual(chatRoomId, get.ChatRoomId, "Casting should work fine.");

            var ipAddress = IPAddress.Loopback;

            var bans = new List<ChatRoomBan>
            {
                new ChatRoomBan
                {
                    ChatRoomId = chatRoomId,
                    IpAddress = ipAddress,
                    Time = DateTime.UtcNow,
                    User = new User
                    {
                        NickName = "Test1"
                    }
                },
                new ChatRoomBan
                {
                    ChatRoomId = chatRoomId,
                    IpAddress = ipAddress,
                    Time = DateTime.UtcNow,
                    User = new User
                    {
                        NickName = "Test2"
                    }
                },
                new ChatRoomBan
                {
                    ChatRoomId = chatRoomId,
                    IpAddress = ipAddress,
                    Time = DateTime.UtcNow,
                    User = new User
                    {
                        NickName = "Test3"
                    }
                }
            };

            get.Bans = bans;

            Assert.AreEqual(bans.Count, get.Bans.Count);

            var payload = new Payload(get);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newGet = new GetChatRoomBans();
            newGet.ParsePayload(newPayload);

            Assert.AreEqual(get.ChatRoomId, newGet.ChatRoomId, "ChatRoomId must survive deserialization process.");
            Assert.AreEqual(get.Bans.Count, newGet.Bans.Count, "Bans must survive deserialization process.");

            foreach (var pair in get.Bans.Zip(newGet.Bans, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.ChatRoomId, pair.Item2.ChatRoomId,
                    "Chatroom information must survive deserialization process.");
                Assert.AreEqual(pair.Item1.IpAddress, pair.Item2.IpAddress,
                    "IpAddress information must survive deserialization process.");
                Assert.AreEqual(pair.Item1.Time, pair.Item2.Time,
                    "Time information must survive deserialization process.");
                Assert.AreEqual(pair.Item1.User.NickName, pair.Item2.User.NickName,
                    "User information must survive the deserialization process.");
            }
        }
    }
}
