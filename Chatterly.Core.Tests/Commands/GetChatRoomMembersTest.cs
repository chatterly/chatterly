﻿using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Core.Tests.Commands
{
    [TestClass]
    public class GetChatRoomMembersTest
    {
        [TestMethod]
        public void GetChatRoomMembers_Test()
        {
            int chatRoomId = 1;

            var cmd = GetChatRoomMembers.CreateCommand(chatRoomId);

            var get = cmd as GetChatRoomMembers;

            Assert.AreEqual(chatRoomId, get.ChatRoomId, "Casting should work fine.");

            var members = new List<User>
            {
                new User
                {
                    NickName = "Test1"
                },
                new User
                {
                    NickName = "Test2"
                },
                new User
                {
                    NickName = "Test3"
                }
            };

            get.Members = members;

            Assert.AreEqual(members.Count, get.Members.Count);

            var payload = new Payload(get);
            var data = (payload as IMessage).Serialize();
            var newPayload = new Payload(data);

            var newGet = new GetChatRoomMembers();
            newGet.ParsePayload(newPayload);

            Assert.AreEqual(get.ChatRoomId, newGet.ChatRoomId, "ChatRoomId must survive deserialization process.");
            Assert.AreEqual(get.Members.Count, newGet.Members.Count, "Members must survive deserialization process.");

            foreach (var pair in get.Members.Zip(newGet.Members, (a, b) => Tuple.Create(a, b)))
            {
                Assert.AreEqual(pair.Item1.NickName, pair.Item2.NickName,
                    "User information must survive deserialization process.");
            }
        }
    }
}
