﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chatterly.Core.Tests
{
    [TestClass]
    public class ServicesTest
    {
        #region Dummy Classes

        public class Service1 { }

        public class Service2 { }

        #endregion Dummy Classes

        [TestMethod]
        public void Services_Test()
        {
            var serv1 = new Service1();
            var serv2 = new Service2();

            Services.RegisterService(serv1);

            Assert.AreEqual(serv1, Services.GetService<Service1>(), "Registered service must match.");

            var threw = false;

            try
            {
                Services.GetService<Service2>();
            }
            catch
            {
                threw = true;
            }

            Assert.IsTrue(threw, "Accessing an unregistered service must throw exception.");

            Services.RegisterService(serv2);

            Assert.AreEqual(serv1, Services.GetService<Service1>(), "Registered service must match.");
            Assert.AreEqual(serv2, Services.GetService<Service2>(), "Registered service must match.");

            var newServ1 = new Service1();

            Services.RegisterService(newServ1);

            Assert.AreNotEqual(serv1, Services.GetService<Service1>(), "Service must be overwritable.");
            Assert.AreEqual(newServ1, Services.GetService<Service1>(), "Overwritten, registered service must match.");
        }
    }
}
