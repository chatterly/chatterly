﻿using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace Chatterly.Core.Tests.Protocol
{
    [TestClass]
    public class BinaryHelperTest
    {
        /// <summary>
        /// Helper method to compare to byte arrays. Compares length and then performs an element-wise comparison.
        /// </summary>
        /// <param name="expected">Expected byte array.</param>
        /// <param name="actual">Actual byte array.</param>
        /// <param name="name">Name that should be included in the message.</param>
        void CompareBytes(byte[] expected, byte[] actual, string name)
        {
            Assert.AreEqual(expected.Length, actual.Length, string.Format("{0} length not identical.", name));

            for (int i = 0; i < expected.Length; ++i)
            {
                Assert.AreEqual(expected[i], actual[i], string.Format("{0} bytes not identical.", name));
            }
        }

        /// <summary>
        /// Test the <see cref="Chatterly.Core.Protocol.BinaryHelper.ToBytes"/> methods.
        /// </summary>
        [TestMethod]
        public void BinaryHelper_ToBytes()
        {
            var expectedGuid = Guid.NewGuid();

            var expectedBytes = expectedGuid.ToByteArray();

            var stream = new MemoryStream();

            var writer = new BinaryWriter(stream);

            writer.Write(expectedBytes);

            var actualBytesWriter = writer.ToBytes();
            var actualBytesStream = stream.ToBytes();

            CompareBytes(expectedBytes, actualBytesWriter, "Writer");
            CompareBytes(expectedBytes, actualBytesStream, "Stream");

            var actualGuid = new Guid(actualBytesWriter);

            Assert.AreEqual(expectedGuid, actualGuid, "Guid not reproduced.");
        }

        /// <summary>
        /// Test the <see cref="Chatterly.Core.Protocol.BinaryHelper.GetReader"/> methods.
        /// </summary>
        [TestMethod]
        public void BinaryHelper_GetReader()
        {
            var expectedGuid = Guid.NewGuid();

            var expectedBytes = expectedGuid.ToByteArray();

            var reader = expectedBytes.GetReader();

            var actualBytes = reader.ReadBytes(expectedBytes.Length);

            CompareBytes(expectedBytes, actualBytes, "Reader");

            var actualGuid = new Guid(actualBytes);

            Assert.AreEqual(expectedGuid, actualGuid, "Guid not reproduced.");
        }

        /// <summary>
        /// Test the <see cref="Chatterly.Core.Protocol.BinaryHelper.GetWriter"/> methods.
        /// </summary>
        [TestMethod]
        public void BinaryHelper_GetWriter()
        {
            var expectedGuid = Guid.NewGuid();

            var expectedBytes = expectedGuid.ToByteArray();

            var writer = BinaryHelper.GetWriter();

            writer.Write(expectedBytes);

            var actualBytes = writer.ToBytes();

            CompareBytes(expectedBytes, actualBytes, "Writer");

            var actualGuid = new Guid(actualBytes);

            Assert.AreEqual(expectedGuid, actualGuid, "Guid not reproduced.");
        }
    }
}
