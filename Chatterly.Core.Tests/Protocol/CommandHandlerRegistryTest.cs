﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Chatterly.Core.Tests.Protocol
{
    [TestClass]
    public class CommandHandlerRegistryTest
    {
        #region Dummy Classes

        public class DummyCommand1 : BaseCommand
        {
            public override void ParsePayload(Payload msg)
            {
            }
        }

        public class DummyCommand2 : BaseCommand
        {
            public override void ParsePayload(Payload msg)
            {
            }
        }

        public class DummyHandler1 : ICommandHandler
        {
            void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
            { }
        }

        public class DummyHandler2 : ICommandHandler
        {
            void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
            { }
        }
        #endregion Dummy Classes

        [TestMethod]
        public void ComandHandlerRegistry_RegisterAndGetHandlers()
        {
            var reg = new CommandHandlerRegistry();

            var dummy1 = new DummyHandler1();

            reg.RegisterHandler<DummyCommand1>(dummy1);

            CheckHandlers(new ICommandHandler[] { dummy1 }, reg.GetHandlers<DummyCommand1>());

            reg.RegisterHandler<DummyCommand2>(dummy1);

            CheckHandlers(new ICommandHandler[] { dummy1 }, reg.GetHandlers<DummyCommand1>());
            CheckHandlers(new ICommandHandler[] { dummy1 }, reg.GetHandlers<DummyCommand2>());

            var dummy2 = new DummyHandler2();

            reg.RegisterHandler(typeof(DummyCommand1), dummy2);

            CheckHandlers(new ICommandHandler[] { dummy1, dummy2 }, reg.GetHandlers<DummyCommand1>());
            CheckHandlers(new ICommandHandler[] { dummy1 }, reg.GetHandlers<DummyCommand2>());

            reg.RegisterHandler(typeof(DummyCommand2), dummy2);


            CheckHandlers(new ICommandHandler[] { dummy1, dummy2 }, reg.GetHandlers<DummyCommand1>());
            CheckHandlers(new ICommandHandler[] { dummy1, dummy2 }, reg.GetHandlers<DummyCommand2>());
        }

        void CheckHandlers(IEnumerable<ICommandHandler> expected, IEnumerable<ICommandHandler> actual)
        {
            var expectedList = new List<ICommandHandler>(expected);
            var actualList = new List<ICommandHandler>(actual);

            Assert.AreEqual(expectedList.Count, actualList.Count, "Number of handlers must be equal.");

            for (int i = 0; i < expectedList.Count; ++i)
            {
                Assert.AreSame(expectedList[i], actualList[i], "Expected and actual must bet he same element-wise.");
            }
        }
    }
}
