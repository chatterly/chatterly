﻿using Chatterly.Client.Database;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Core.Tests.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Chatterly.Core.Tests.Protocol
{
    [TestClass]
    public class CommunicatorTest
    {
        #region Dummy Classes

        public class DummyMessage : IMessage
        {
            public string Message { get; set; }

            public DummyMessage() { }

            public DummyMessage(string msg)
            {
                Message = msg;
            }

            byte[] IMessage.Serialize()
            {
                using (var writer = BinaryHelper.GetWriter())
                {
                    writer.Write(Message);
                    return writer.ToBytes();
                }
            }

            void IMessage.Deserialize(byte[] bytes)
            {
                using (var reader = bytes.GetReader())
                {
                    Message = reader.ReadString();
                }
            }
        }

        public class DummyCommand : BaseCommand
        {
            public string Message { get; set; }

            public IList<DummyMessage> Messages { get; set; } = new List<DummyMessage>();

            public static DummyCommand CreateCommand(string message, params DummyMessage[] messages)
            {
                return new DummyCommand
                {
                    Message = message,
                    Messages = messages.ToList()
                };
            }

            public override byte[] CreatePrimitiveData()
            {
                using (var writer = BinaryHelper.GetWriter())
                {
                    writer.Write(Message);
                    return writer.ToBytes();

                }
            }

            public override IMessage[] CreatePayloadData()
            {
                return Messages.ToArray();
            }

            public override void ParsePayload(Payload payload)
            {
                using (var reader = payload.PrimitiveData.GetReader())
                {
                    Message = reader.ReadString();
                }

                Messages = payload.Messages.Cast<DummyMessage>().ToList();
            }
        }

        public class DummyHandler : ICommandHandler
        {
            void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
            {
                communicator.Send(command);
            }
        }

        #endregion Dummy Classes

        static TcpListener Listener { get; set; }
        static TcpClient Comm1Client { get; set; }
        static Communicator Comm1 { get; set; }
        static Communicator Comm2 { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            var testDb = new TestDbContext();
            testDb.EnsureCreated();
            Services.RegisterService(new LogManager(testDb));

            var config1 = new ConnectionConfiguration
            {
                Address = IPAddress.Any,
                Port = 6998
            };

            var config2 = new ConnectionConfiguration
            {
                Address = IPAddress.Parse("127.0.0.1"),
                Port = 6998
            };

            Listener = new TcpListener(config1.ToIpEndpoint());
            Listener.Start();

            var accept = Task.Factory.StartNew(() =>
            {
                Comm2 = new Communicator(Listener.AcceptTcpClient());
                Comm2.Start();
            });

            Comm1Client = new TcpClient();
            Comm1 = new Communicator(Comm1Client);
            Comm1Client.Connect(config2.ToIpEndpoint());
            Comm1.Start();

            accept.Wait();
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            Listener.Stop();
            Comm1Client.Close();
            Comm1.Stop();
            Comm2.Stop();
        }

        [TestMethod]
        public void Communicator_SendReceiveCallbackHandler()
        {
            var reg = new CommandHandlerRegistry();
            reg.RegisterHandler<DummyCommand>(new DummyHandler());
            Services.RegisterService(reg);

            var expectedCmd = DummyCommand.CreateCommand("Message!", new DummyMessage("Dummy message 1"),
                new DummyMessage("Dummy message 2"), new DummyMessage("Dummy message 3"));

            DummyCommand actualCmd = null;
            var received = false;

            Comm1.Send(expectedCmd, c =>
            {
                actualCmd = c as DummyCommand;

                received = true;
            });

            int tries = 100;

            while (actualCmd == null && tries-- >= 0)
            {
                Thread.Sleep(25);
            }

            Assert.IsNotNull(actualCmd, "Received command must not be null.");

            tries = 100;

            while (!received && tries-- >= 0)
            {
                Thread.Sleep(25);
            }

            Assert.IsTrue(received, "Callback must be fully evaluated.");

            Assert.AreEqual(expectedCmd.Id, actualCmd.Id, "Command IDs must be equal.");
            Assert.AreEqual(expectedCmd.Message, actualCmd.Message, "Command primitive data must be equal.");
            Assert.AreEqual(expectedCmd.Messages.Count, actualCmd.Messages.Count, "Message lengths must be equal.");

            for (int i = 0; i < expectedCmd.Messages.Count; ++i)
            {
                Assert.AreEqual(expectedCmd.Messages[i].Message, actualCmd.Messages[i].Message, "Messages must be equal element-wise.");
            }

            // test basic properties and such
            Assert.AreEqual(Communicator.GUID_SIZE, 16, "Property must be representative of the correct Guid size.");
            Assert.AreNotEqual(Comm1.Session, Guid.Empty, "Communicator must have a set session.");
            Assert.IsNotNull(Comm1.IpAddress, "Communicator must have a set IP Address.");
            Assert.AreNotEqual(Comm1.IpAddress, IPAddress.None, "Communicator must have a set IP Address.");

            // test poll and disconnect behavior
            Assert.IsTrue(Comm1.Poll(), "Communicator must poll correctly.");
            Comm1Client.GetStream().Close();
            Comm1Client.Close();
            Assert.IsFalse(Comm1.Poll(), "Communicator must poll correctly after a disconnect event.");

        }
    }
}
