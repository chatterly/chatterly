﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chatterly.Core.Tests.Protocol
{
    [TestClass]
    public class PayloadTest
    {
        #region Dummy Classes

        public class DummyMessage : IMessage
        {
            public string Message { get; set; }

            byte[] IMessage.Serialize()
            {
                using (var writer = BinaryHelper.GetWriter())
                {
                    writer.Write(Message);
                    return writer.ToBytes();
                }
            }

            void IMessage.Deserialize(byte[] bytes)
            {
                using (var reader = bytes.GetReader())
                {
                    Message = reader.ReadString();
                }
            }

            public override bool Equals(object obj)
            {
                return (obj as DummyMessage)?.Message == Message;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public class DummyCommand : BaseCommand
        {
            public string Message { get; set; }

            public IList<DummyMessage> Messages { get; set; } = new List<DummyMessage>();

            public override byte[] CreatePrimitiveData()
            {
                using (var writer = BinaryHelper.GetWriter())
                {
                    writer.Write(Message);
                    return writer.ToBytes();

                }
            }

            public override IMessage[] CreatePayloadData()
            {
                return Messages.ToArray();
            }

            public override void ParsePayload(Payload msg)
            {
                using (var reader = msg.PrimitiveData.GetReader())
                {
                    Message = reader.ReadString();
                }

                Messages = msg.Messages.Cast<DummyMessage>().ToList();
            }
        }

        #endregion Dummy Classes

        [TestMethod]
        public void Payload_Test()
        {
            var cmd = new DummyCommand
            {
                Id = Guid.NewGuid(),
                Message = "This is a dummy command!"
            };

            cmd.Messages.Add(new DummyMessage
            {
                Message = "This is the first dummy message!"
            });

            cmd.Messages.Add(new DummyMessage
            {
                Message = "This is the second dummy message!"
            });

            cmd.Messages.Add(new DummyMessage
            {
                Message = "This is the third dummy message!"
            });

            var cmdType = cmd.GetType().FullName;
            var messages = cmd.CreatePayloadData();
            var primitive = cmd.CreatePrimitiveData();
            var session = Guid.NewGuid();
            var time = DateTime.UtcNow;

            var payload = new Payload
            {
                CommandType = cmdType,
                Messages = messages,
                PrimitiveData = primitive,
                Session = session,
                Time = time
            };

            // ensuring that properties are handled as expected
            Assert.AreEqual(cmdType, payload.CommandType, "CommandType property.");
            Assert.AreEqual(primitive, payload.PrimitiveData, "PrimitiveData property.");
            Assert.AreEqual(session, payload.Session, "Session property.");
            Assert.AreEqual(time, payload.Time, "Time property.");

            Assert.AreEqual(messages.Length, payload.Messages.Count(), "Messages property (length).");

            for (int i = 0; i < messages.Length; ++i)
            {
                Assert.AreEqual(messages[i], payload.Messages[i], "Messages property (element-wise).");
            }

            var data = (payload as IMessage).Serialize();

            var newPayload = new Payload(data);

            Assert.AreEqual(payload.CommandType, newPayload.CommandType, "Command type must be equal.");
            Assert.AreEqual(payload.Session, newPayload.Session, "Session type must be equal.");
            Assert.AreEqual(payload.Time, newPayload.Time, "Time type must be equal.");

            var newCmd = Activator.CreateInstance(Type.GetType(newPayload.CommandType)) as DummyCommand;

            Assert.IsNotNull(newCmd, "Command must be recreated with the proper type.");

            newCmd.ParsePayload(newPayload);

            // ID serialization is not handled by the payload or command and will not be tested here            
            Assert.AreEqual(cmd.Message, newCmd.Message, "Command primitive data must be equal.");
            Assert.AreEqual(cmd.Messages.Count, newCmd.Messages.Count, "Message lengths must be equal.");

            for (int i = 0; i < cmd.Messages.Count; ++i)
            {
                Assert.AreEqual(cmd.Messages[i].Message, newCmd.Messages[i].Message, "Messages must be equal element-wise.");
            }
        }
    }
}
