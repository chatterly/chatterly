﻿using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Chatterly.Core.Tests.Protocol
{
    [TestClass]
    public class ProtocolTypeManagerTest
    {
        #region Dummy Classes

        public class DummyMessage : IMessage
        {
            byte[] IMessage.Serialize()
            {
                throw new NotImplementedException();
            }

            void IMessage.Deserialize(byte[] bytes)
            { }
        }

        public class DummyCommand : ICommand
        {
            public Guid Id { get; set; }

            public byte[] CreatePrimitiveData()
            {
                throw new NotImplementedException();
            }

            public IMessage[] CreatePayloadData()
            {
                throw new NotImplementedException();
            }

            public void ParsePayload(Payload payload)
            {
            }
        }

        public class DummyHandler : ICommandHandler
        {
            void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
            { }
        }

        #endregion Dummy Classes

        [TestMethod]
        public void ProtocolTypeManager_Test()
        {
            var msg = typeof(DummyMessage);
            var cmd = typeof(DummyCommand);
            var handler = typeof(DummyHandler);

            Assert.AreEqual(msg, ProtocolTypeManager.GetType(msg.FullName), "Must pull in IMessage.");
            Assert.AreEqual(cmd, ProtocolTypeManager.GetType(cmd.FullName), "Must pull in ICommand.");
            Assert.AreEqual(handler, ProtocolTypeManager.GetType(handler.FullName),
                "Must pull in ICommandHandler.");
        }
    }
}
