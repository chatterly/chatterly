﻿using Chatterly.Core;
using Chatterly.Server.Database;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Chatterly.ServerApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            (new ServerContext()).EnsureCreated();
            var logMgr = new LogManager(new ServerContext());

            try
            {

                var args = new Dictionary<string, string>();

                for (int i = 0; i < e.Args.Length; i += 2)
                {
                    if (e.Args.Length >= i + 1)
                    {
                        args.Add(e.Args[i].ToLower(), e.Args[i + 1]);
                    }
                }

                if (args.ContainsKey("port"))
                {
                    logMgr.Add(Core.Database.LogType.Info, "Attempting to set port configuration.");
                    int port;
                    if (int.TryParse(args["port"], out port))
                    {
                        Config.Port = port;
                        logMgr.Add(Core.Database.LogType.Info, "Set port configuration.");
                    }
                }

                if (args.ContainsKey("key"))
                {
                    logMgr.Add(Core.Database.LogType.Info, "Setting crypto key configuration.");
                    Config.CryptoKey = args["key"];
                }
            }
            catch (Exception ex)
            {
                logMgr.Add("Failed to initialize ServerApp", ex);
            }
        }
    }
}
