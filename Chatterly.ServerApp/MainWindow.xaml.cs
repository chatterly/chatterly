﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Chatterly.Server;
using Chatterly.Server.CommandHandlers;
using Chatterly.Server.Database;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;

namespace Chatterly.ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            if (string.IsNullOrWhiteSpace(Config.CryptoKey))
            {
                var keyName = string.Format("{0}.Resources.Key.xml", GetType().Namespace);
                var keyStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(keyName);
                Config.CryptoKey = (new StreamReader(keyStream)).ReadToEnd();
            }
            User.Encryptor = new RsaCrypto(Config.CryptoKey);
            Config.CryptoKey = null;

            var cmdr = new CommandHandlerRegistry();
            cmdr.RegisterHandler<DoesNickNameExist>(new DoesNickNameExistHandler());
            cmdr.RegisterHandler<RegisterUser>(new RegisterUserHandler());
            cmdr.RegisterHandler<AuthenticateUser>(new AuthenticateUserHandler());
            cmdr.RegisterHandler<GetChatRoom>(new GetChatRoomHandler());
            cmdr.RegisterHandler<GetChatRooms>(new GetChatRoomsHandler());
            cmdr.RegisterHandler<MessageChatRoom>(new MessageChatRoomHandler());
            cmdr.RegisterHandler<MessageUser>(new MessageUserHandler());
            cmdr.RegisterHandler<JoinChatRoom>(new JoinChatRoomHandler());
            cmdr.RegisterHandler<LeaveChatRoom>(new LeaveChatRoomHandler());
            cmdr.RegisterHandler<IsChatRoomStaff>(new IsChatRoomStaffHandler());
            cmdr.RegisterHandler<GetChatRoomStaff>(new GetChatRoomStaffHandler());
            cmdr.RegisterHandler<ModifyChatRoomStaff>(new ModifyChatRoomStaffHandler());
            cmdr.RegisterHandler<CreateChatRoom>(new CreateChatRoomHandler());
            cmdr.RegisterHandler<ModifyBan>(new ModifyBanHandler());
            cmdr.RegisterHandler<KickUser>(new KickUserHandler());
            cmdr.RegisterHandler<ModifyChatRoomMembership>(new ModifyChatRoomMembershipHandler());
            cmdr.RegisterHandler<GetChatRoomMembers>(new GetChatRoomMembersHandler());
            cmdr.RegisterHandler<GetChatRoomBans>(new GetChatRoomBansHandler());
            cmdr.RegisterHandler<ModifyChatRoom>(new ModifyChatRoomHandler());
            cmdr.RegisterHandler<GetUsers>(new GetUsersHandler());
            Services.RegisterService(cmdr);

            Services.RegisterService(new LogManager(new ServerContext()));
            Services.RegisterService(new ChatServer(Config.Port));

            InitializeComponent();
            DataContext = this;
            Services.GetService<ChatServer>().Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            Services.GetService<ChatServer>().Stop();
        }
    }
}
