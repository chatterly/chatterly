﻿namespace Chatterly.ServerApp
{
    public static class Config
    {
        public static int Port { get; set; } = 7777;
        public static string CryptoKey { get; set; }
    }
}
