﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server;
using Chatterly.Server.CommandHandlers;
using Chatterly.Server.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Chatterly.ServerApp.Tests
{
    [TestClass]
    public class ServerInit
    {
        [ClassInitialize]
        public static void InitializeTestClass(TestContext context)
        {
            (new ServerContext()).EnsureCreated();
            Config.Port = 6999;
            new MainWindow();
        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            Services.GetService<ChatServer>().Stop();
        }

        [TestMethod]
        public void ServerInit_ServicesInitialized()
        {
            var cmdr = Services.GetService<CommandHandlerRegistry>();
            var chatServer = Services.GetService<ChatServer>();
            var logManager = Services.GetService<LogManager>();

            Assert.IsNotNull(cmdr, "CommandHandlerRegistry must be registered as a service.");
            Assert.IsNotNull(chatServer, "ChatServer must be registered as a service.");
            Assert.IsNotNull(logManager, "LogManager must be registered as a service.");
        }

        [TestMethod]
        public void ServerInit_CommandHandlersRegistered()
        {
            var cmdr = Services.GetService<CommandHandlerRegistry>();


            var doesUserExist = cmdr.GetHandlers<DoesNickNameExist>();
            var regUser = cmdr.GetHandlers<RegisterUser>();
            var authUser = cmdr.GetHandlers<AuthenticateUser>();
            var getChatRoom = cmdr.GetHandlers<GetChatRoom>();
            var getChatRooms = cmdr.GetHandlers<GetChatRooms>();
            var messageChatRoom = cmdr.GetHandlers<MessageChatRoom>();
            var joinChatRoom = cmdr.GetHandlers<JoinChatRoom>();
            var leaveChatRoom = cmdr.GetHandlers<LeaveChatRoom>();

            Assert.AreEqual(1, doesUserExist.Count(), "Only expecting one command handler for DoesNickNameExist.");
            Assert.AreEqual(1, regUser.Count(), "Only expecting one command handler for RegisterUser.");
            Assert.AreEqual(1, authUser.Count(), "Only expecting one command handler for AuthenticateUser.");
            Assert.AreEqual(1, getChatRoom.Count(), "Only expecting one command handler for GetChatRoom.");
            Assert.AreEqual(1, getChatRooms.Count(), "Only expecting one command handler for GetChatRooms.");
            Assert.AreEqual(1, messageChatRoom.Count(), "Only expecting one command handler for MessageChatRoom.");
            Assert.AreEqual(1, joinChatRoom.Count(), "Only expecting one command handler for JoinChatRoom.");
            Assert.AreEqual(1, leaveChatRoom.Count(), "Only expecting one command handler for LeaveChatRoom.");

            Assert.IsInstanceOfType(doesUserExist.First(), typeof(DoesNickNameExistHandler),
                "Expected DoesNickNameExistHandler for DoesNickNameExist.");
            Assert.IsInstanceOfType(regUser.First(), typeof(RegisterUserHandler),
                "Expected RegisterUserHandler for RegisterUser.");
            Assert.IsInstanceOfType(authUser.First(), typeof(AuthenticateUserHandler),
                "Expected AuthenticateUserHandler for AuthenticateUser.");
            Assert.IsInstanceOfType(getChatRoom.First(), typeof(GetChatRoomHandler),
                "Expected GetChatRoomHandler for GetChatRoom.");
            Assert.IsInstanceOfType(getChatRooms.First(), typeof(GetChatRoomsHandler),
                "Expected GetChatRoomsHandler for GetChatRooms.");
            Assert.IsInstanceOfType(messageChatRoom.First(), typeof(MessageChatRoomHandler),
                "Expected MessageChatRoomHandler for MessageChatRoom.");
            Assert.IsInstanceOfType(joinChatRoom.First(), typeof(JoinChatRoomHandler),
                "Expected JoinChatRoomHandler for JoinChatRoom.");
            Assert.IsInstanceOfType(leaveChatRoom.First(), typeof(LeaveChatRoomHandler),
                "Expected LeaveChatRoomHandler for LeaveChatRoom.");
        }

        [TestMethod]
        public void ServerInit_ServerRunning()
        {
            var server = Services.GetService<ChatServer>();
            Assert.IsTrue(server.IsRunning, "ChatServer should be running.");
        }
    }
}
