﻿using Chatterly.Core;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Chatterly.Server
{
    /// <summary>
    /// Handles server network communication.
    /// </summary>
    public class ChatServer
    {
        /// <summary>
        /// True if the server is running.
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <summary>
        /// Network end point to which we're listening for network communication.
        /// </summary>
        public IPEndPoint EndPoint
        {
            get
            {
                return _EndPoint;
            }
        }
        readonly IPEndPoint _EndPoint;

        /// <summary>
        /// Network listener.
        /// </summary>
        protected TcpListener Listener { get; set; }

        /// <summary>
        /// Object used to cancel tasks.
        /// </summary>
        protected CancellationTokenSource Cancel { get; set; }

        /// <summary>
        /// Task to listen for network communication.
        /// </summary>
        protected Task ListenTask { get; set; }

        /// <summary>
        /// Task to handle disconnected clients.
        /// </summary>
        protected Task CullTask { get; set; }

        /// <summary>
        /// Map of unique IDs to client communicators.
        /// </summary>
        public ConcurrentDictionary<Guid, Communicator> Clients
        {
            get
            {
                return _Clients;
            }
        }
        readonly ConcurrentDictionary<Guid, Communicator> _Clients = new ConcurrentDictionary<Guid, Communicator>();

        /// <summary>
        /// Create a chat server.
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public ChatServer(IPAddress ipAddress, int port)
        {
            _EndPoint = new IPEndPoint(ipAddress, port);
        }

        /// <summary>
        /// Create a chat server.
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public ChatServer(string ipAddress, int port)
            : this(IPAddress.Parse(ipAddress), port)
        { }

        /// <summary>
        /// Create a chat server that listens to any IP address.
        /// </summary>
        /// <param name="port"></param>
        public ChatServer(int port)
            : this(IPAddress.Any, port)
        { }

        /// <summary>
        /// Start the chat server.
        /// </summary>
        public void Start()
        {
            Cancel = new CancellationTokenSource();
            Listener = new TcpListener(EndPoint);
            Listener.Start();
            ListenTask = Task.Factory.StartNew(Listen_DoWork);
            CullTask = Task.Factory.StartNew(Cull_DoWork);
            IsRunning = true;
        }

        /// <summary>
        /// Stop the chat server.
        /// </summary>
        public void Stop()
        {
            try
            {
                Listener.Stop();
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("Error stopping server listener", e);
            }

            Cancel.Cancel();
            ListenTask = null;
            CullTask = null;
            Listener = null;
            Cancel = null;
            Clients.Clear();

            IsRunning = false;

            using (var db = new ServerContext())
            {
                db.UserSessions.Clear();
                db.ChatRoomSubscriptions.Clear();
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Log out the client associated with the given client ID.
        /// </summary>
        /// <param name="session"></param>
        protected void LogOutClient(Guid session)
        {
            using (var db = new ServerContext())
            {
                var sessions = db.UserSessions.Include(s => s.ChatRoomSubscriptions).Where(s => s.Session == session);

                if (sessions != null)
                {
                    foreach (var sess in sessions)
                    {
                        db.UserSessions.Remove(sess);
                    }

                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Broadcast a command to all clients.
        /// </summary>
        /// <param name="cmd">Command to broadcast.</param>
        /// <param name="approveSend">Optional predicate to selective broadcast to clients.</param>
        public void Broadcast(ICommand cmd, Predicate<Communicator> approveSend = null)
        {
            var remove = new List<Guid>();

            var clients = new List<Communicator>(Clients.Values);

            foreach (var client in clients)
            {
                if (approveSend == null || approveSend.Invoke(client))
                {
                    try
                    {
                        client.Send(cmd);
                    }
                    catch (Exception e)
                    {
                        Services.GetService<LogManager>().Add("Broadcast Error", e);
                        client.Stop();
                        remove.Add(client.Session);
                    }
                }
                else
                {
                    if (!client.Poll())
                    {
                        remove.Add(client.Session);
                    }
                }
            };

            foreach (var session in remove)
            {
                LogOutClient(session);
                Communicator throwAway;
                Clients.TryRemove(session, out throwAway);
            }
        }

        /// <summary>
        /// Handle the removal of disconnected clients.
        /// </summary>
        protected void Cull_DoWork()
        {
            while (CullTask != null && !(CullTask.IsCanceled || CullTask.IsFaulted))
            {
                if (Cancel != null && Cancel.Token.IsCancellationRequested)
                {
                    Cancel.Token.ThrowIfCancellationRequested();
                }

                try
                {
                    var clients = new List<Communicator>(Clients.Values);
                    var remove = new List<Guid>();

                    foreach (var client in clients)
                    {
                        if (!client.Poll())
                        {
                            remove.Add(client.Session);
                        }
                    }

                    foreach (var session in remove)
                    {
                        LogOutClient(session);
                        Communicator throwAway;
                        Clients.TryRemove(session, out throwAway);
                    }

                    Thread.Sleep(1000);
                }
                catch (OperationCanceledException)
                {
                    Services.GetService<LogManager>().Add(LogType.Info, "Client culler canceled.");
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("Error culling clients", e);
                }
            }
        }

        /// <summary>
        /// Handle incoming network communication.
        /// </summary>
        protected void Listen_DoWork()
        {
            while (ListenTask != null && !(ListenTask.IsCanceled || ListenTask.IsFaulted))
            {
                if (Cancel != null && Cancel.Token.IsCancellationRequested)
                {
                    Cancel.Token.ThrowIfCancellationRequested();
                }

                try
                {
                    var client = Listener.AcceptTcpClient();

                    if (client.Connected)
                    {
                        var ClientListener = new Communicator(client);
                        Clients.AddOrUpdate(ClientListener.Session, ClientListener, (a, b) => ClientListener);
                        ClientListener.Start();
                    }
                }
                catch (OperationCanceledException)
                {
                    Services.GetService<LogManager>().Add(LogType.Info, "Listener canceled.");
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("Error listening", e);

                    try
                    {
                        Listener.Stop();
                        Thread.Sleep(1000);
                        Listener.Start();
                    }
                    catch (Exception ex)
                    {

                        Services.GetService<LogManager>().Add("Error restarting Listener", ex);
                    }
                }
            }
        }
    }
}
