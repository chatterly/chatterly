﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chatterly.Server.CommandHandlers
{
    public class ModifyChatRoomHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var mod = command as ModifyChatRoom;
            var response = new BoolResponse
            {
                Id = command.Id,
                Result = false
            };

            if (mod == null)
            {
                response.Message = "Received an invalid ModifyChatRoom command.";
            }
            else
            {
                try
                {
                    using (var db = new ServerContext())
                    {
                        var user = db.GetUser(communicator.Session);
                        var staffRank = db.GetChatRoomStaffRank(mod.ChatRoom.Id, user.Id);

                        if (staffRank < ChatRoomRank.Admin)
                        {
                            response.Message = "Must be at least an Admin to modify a chatroom.";
                        }
                        else
                        {
                            var doesChatRoomExist = db.ChatRooms.Any(c => c.Id == mod.ChatRoom.Id);

                            if (!doesChatRoomExist)
                            {
                                response.Message = "The chatroom does not exist.";
                            }
                            else
                            {
                                db.ChatRooms.Update(mod.ChatRoom);
                                db.SaveChanges();
                                response.Result = true;
                                response.Message = "Chatroom updated!";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("ModifyChatHandler failure.", e);
                }
            }

            communicator.Send(response);
        }
    }
}
