﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    public class CreateChatRoomHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as CreateChatRoom;

            if (cmd != null)
            {
                try
                {
                    using (var db = new ServerContext())
                    {
                        var user = db.GetUser(communicator.Session);

                        if (user != null)
                        {
                            cmd = HandleCommand(db, user, cmd);
                        }
                        else
                        {
                            cmd.IsCreated = false;
                            cmd.Message = "Must be authenticated to create a chatroom.";
                        }
                    }
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("CreateChatRoomHandler Failure", e);

                }
            }
            else
            {

                cmd = new CreateChatRoom
                {
                    Id = command?.Id ?? Guid.Empty,
                    IsCreated = false,
                    Message = "A valid command object is required to create a chatroom."
                };
            }

            communicator.Send(cmd);
        }

        CreateChatRoom HandleCommand(ServerContext db, ServerUser user, CreateChatRoom cmd)
        {
            cmd.IsCreated = false;

            // will be returning at earliest opportunity
            if (cmd.ChatRoom == null)
            {
                cmd.Message = "Cannot create a chatroom without chatroom information.";
                return cmd;
            }

            var doesChatroomExist = db.ChatRooms.Any(c => c.Name == cmd.ChatRoom.Name);

            if (doesChatroomExist)
            {
                cmd.Message = string.Format("A chatroom with the name \"{0}\" already exists.",
                    cmd.ChatRoom.Name);
                return cmd;
            }

            cmd.ChatRoom.Created = DateTime.UtcNow;

            db.ChatRooms.Add(cmd.ChatRoom);
            db.ChatRoomStaff.Add(new ChatRoomStaff
            {
                ChatRoom = cmd.ChatRoom,
                UserId = user.Id,
                Rank = ChatRoomRank.Owner,
                Time = DateTime.UtcNow
            });
            db.SaveChanges();

            cmd.IsCreated = true;
            cmd.Message = "Chatroom created!";

            return cmd;
        }
    }
}
