﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="JoinChatRoom"/> request from the user.
    /// </summary>
    public class JoinChatRoomHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to join a chat room if it exists and the user is authenticated.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var join = command as JoinChatRoom;

            using (var db = new ServerContext())
            {
                var chatRoom = db.ChatRooms.FirstOrDefault(c => c.Id == join.ChatRoom.Id);

                if (chatRoom != null)
                {
                    join.ChatRoom = chatRoom;

                    var session = db.UserSessions.Include(s => s.ChatRoomSubscriptions)
                        .Where(s => s.Session == communicator.Session).OrderByDescending(s => s.Time).FirstOrDefault();

                    if (session != null)
                    {
                        var isBanned = (from b in db.ChatRoomBans
                                        where b.ChatRoomId == chatRoom.Id
                                        where b.UserId == session.UserId || b.IpAddressData == session.IpAddressData
                                        select b).Any();

                        if (isBanned)
                        {
                            communicator.Send(new BoolResponse
                            {
                                Id = join.Id,
                                Message = "You are banned from this chatroom.",
                                Result = false
                            });
                            return;
                        }

                        if (chatRoom.IsPrivate)
                        {
                            var isStaff = db.IsChatRoomStaff(chatRoom.Id, session.UserId);
                            var isMember = db.IsChatRoomMember(chatRoom.Id, session.UserId);

                            if (!(isStaff || isMember))
                            {
                                communicator.Send(new BoolResponse
                                {
                                    Id = join.Id,
                                    Message = "You are not a member of this chatroom.",
                                    Result = false
                                });
                                return;
                            }
                        }

                        session.ChatRoomSubscriptions.Add(new ChatRoomSubscription
                        {
                            ChatRoomId = chatRoom.Id,
                            Time = DateTime.UtcNow
                        });
                        db.SaveChanges();
                        join.Joined = true;
                        communicator.Send(join);
                        return;
                    }
                }
            }

            join.Joined = false;
            communicator.Send(join);
        }
    }
}
