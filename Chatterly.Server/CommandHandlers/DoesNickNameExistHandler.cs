﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// handles a <see cref="DoesNickNameExist"/> request from the client.
    /// </summary>
    public class DoesNickNameExistHandler : ICommandHandler
    {
        /// <summary>
        /// Determines if the given nickname is registered.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var query = command as DoesNickNameExist;

            using (var db = new ServerContext())
            {
                query.NickNameExists = db.DoesNickNameExist(query.NickName);
            }

            communicator.Send(query);
        }
    }
}
