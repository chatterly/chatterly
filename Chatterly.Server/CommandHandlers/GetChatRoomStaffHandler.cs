﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;

namespace Chatterly.Server.CommandHandlers
{
    public class GetChatRoomStaffHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as GetChatRoomStaff;

            using (var db = new ServerContext())
            {
                var staff = db.GetChatRoomStaff(cmd.ChatRoomId);

                if (staff != null)
                {
                    cmd.Staff = staff;
                }
            }

            communicator.Send(cmd);
        }
    }
}
