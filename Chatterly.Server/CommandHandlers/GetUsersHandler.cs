﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;

namespace Chatterly.Server.CommandHandlers
{
    public class GetUsersHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as GetUsers;

            using (var db = new ServerContext())
            {
                var users = db.GetUsers(cmd.StartsWith);

                if (users != null)
                {
                    cmd.Users = users;
                }
            }

            communicator.Send(cmd);
        }
    }
}
