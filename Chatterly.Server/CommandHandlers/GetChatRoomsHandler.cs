﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="GetChatRooms"/> request from the user.
    /// </summary>
    public class GetChatRoomsHandler : ICommandHandler
    {
        /// <summary>
        /// Gets information about all of the existing chat rooms.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var get = command as GetChatRooms;

            using (var db = new ServerContext())
            {
                get.ChatRooms = db.GetChatRooms();
            }

            communicator.Send(get);
        }
    }
}
