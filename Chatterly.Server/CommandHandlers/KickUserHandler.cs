﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    public class KickUserHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var kick = command as KickUser;
            var response = new BoolResponse
            {
                Id = command.Id
            };

            if (kick == null)
            {
                response.Result = false;
                response.Message = "Received an invalid KickUser Command.";
                Services.GetService<LogManager>().Add(LogType.Error, response.Message);
            }
            else
            {
                try
                {
                    using (var db = new ServerContext())
                    {
                        var kickingUser = db.GetUser(communicator.Session);
                        var user = db.GetUser(kick.Nickname);
                        var chatRoom = db.GetChatRoom(kick.ChatRoomId);
                        var chatRoomStaff = db.GetChatRoomStaff(kick.ChatRoomId);

                        var kickingRank = chatRoomStaff.FirstOrDefault(s => s.UserId == kickingUser.Id);
                        var userRank = chatRoomStaff.FirstOrDefault(s => s.UserId == user.Id);

                        if (kickingRank != null && (userRank == null || kickingRank.Rank > userRank.Rank))
                        {
                            var sub = (from s in db.ChatRoomSubscriptions
                                      join sess in db.UserSessions on s.UserSessionId equals sess.Id
                                      where sess.UserId == user.Id
                                      where s.ChatRoomId == kick.ChatRoomId
                                      select new
                                      {
                                          Subscription = s,
                                          Session = sess
                                      }).ToList();
                            db.ChatRoomSubscriptions.RemoveRange(sub.Select(s => s.Subscription));
                            db.SaveChanges();

                            var server = Services.GetService<ChatServer>();

                            foreach (var session in sub.Select(s => s.Session))
                            {
                                Communicator comm;
                                if (server.Clients.TryGetValue(session.Session, out comm))
                                {
                                    comm.Send(kick);
                                }
                            }

                            response.Message = string.Format("User {0} kicked from chatroom {1}",
                                kick.Nickname, chatRoom.Name);
                        }
                        else
                        {
                            response.Message = string.Format(
                                "Not authorized to kick user {0} out of chatroom {1}.",
                                user.NickName, chatRoom.Name);
                        }
                    }
                }
                catch (Exception e)
                {
                    response.Message = string.Format("Exception encountered when attempting to kick user {0}: {1}",
                        kick.Nickname, e.Message);
                    Services.GetService<LogManager>().Add(response.Message, e);
                }
            }

            communicator.Send(response);
        }
    }
}
