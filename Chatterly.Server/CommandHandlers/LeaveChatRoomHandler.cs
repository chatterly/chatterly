﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="LeaveChatRoom"/> request from the user.
    /// </summary>
    public class LeaveChatRoomHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to leave a chat room, if it exists, the user is authenticated, and the user has previously joined the room.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var leave = command as LeaveChatRoom;

            using (var db = new ServerContext())
            {
                var chatRoom = db.ChatRooms.FirstOrDefault(c => c.Id == leave.ChatRoom.Id);

                if (chatRoom != null)
                {
                    leave.ChatRoom = chatRoom;

                    var session = db.UserSessions.Include(s => s.ChatRoomSubscriptions)
                        .Where(s => s.Session == communicator.Session).OrderByDescending(s => s.Time).FirstOrDefault();

                    if (session != null)
                    {
                        session.ChatRoomSubscriptions.RemoveAll(s => s.ChatRoomId == chatRoom.Id);
                        db.SaveChanges();
                        leave.Left = true;
                        communicator.Send(leave);
                        return;
                    }
                }
            }

            leave.Left = false;
            communicator.Send(leave);
        }
    }
}
