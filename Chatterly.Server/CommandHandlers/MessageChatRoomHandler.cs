﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="MessageChatRoom"/> request from the client.
    /// </summary>
    public class MessageChatRoomHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to add a message to a chat room, if it exists, the user is authenticated, and the user has joined the room.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var msg = command as MessageChatRoom;

            using (var db = new ServerContext())
            {
                var user = db.Users.FirstOrDefault(u => u.Id == msg.Message.UserId);
                if (user == null)
                {
                    Services.GetService<LogManager>().Add(LogType.Info, "invalid user");
                    return;
                }

                if (db.ChatRooms.Count(c => c.Id == msg.ChatRoomId) != 1)
                {
                    Services.GetService<LogManager>().Add(LogType.Info, "invalid chat room");
                    return;
                }

                var isSubscribed = new Func<int, Guid, bool>((chatRoomId, session) =>
                    db.ChatRoomSubscriptions.Include(s => s.UserSession)
                        .Count(s => s.UserSession.Session == session && s.ChatRoomId == chatRoomId) == 1);

                if (isSubscribed(msg.ChatRoomId, communicator.Session))
                {
                    msg.Message.User = user;

                    db.Messages.Add(msg.Message);
                    db.ChatRoomMessages.Add(new ChatRoomMessage
                    {
                        ChatRoomId = msg.ChatRoomId,
                        Message = msg.Message
                    });
                    db.SaveChanges();

                    Services.GetService<ChatServer>().Broadcast(msg, comm => isSubscribed(msg.ChatRoomId, comm.Session));
                }
                else
                {
                    communicator.Send(new Response
                    {
                        Id = msg.Id,
                        Message = "Not in chat room."
                    });
                }
            }
        }
    }
}
