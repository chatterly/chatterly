﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;

namespace Chatterly.Server.CommandHandlers
{
    public class ModifyChatRoomMembershipHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var membership = command as ModifyChatRoomMembership;

            var response = new BoolResponse
            {
                Id = command.Id
            };

            if (membership == null)
            {
                response.Result = false;
                response.Message = "Received an invalid ModifyChatRoomMembership Command.";
                Services.GetService<LogManager>().Add(LogType.Error, response.Message);
            }
            else
            {
                try
                {
                    using (var db = new ServerContext())
                    {
                        var actingUser = db.GetUser(communicator.Session);
                        var user = db.GetUser(membership.Nickname);
                        var chatRoom = db.GetChatRoom(membership.ChatRoomId);
                        var isStaff = db.IsChatRoomStaff(membership.ChatRoomId, actingUser.Id);

                        if (isStaff)
                        {
                            if (membership.Action == ChatRoomMembershipAction.Add)
                            {
                                db.AddChatRoomMember(membership.ChatRoomId, user.Id);
                                response.Message = string.Format("User {0} added to chatroom {1}!",
                                    user.NickName, chatRoom.Name);
                            }
                            else if (membership.Action == ChatRoomMembershipAction.Remove)
                            {
                                db.RemoveChatRoomMember(membership.ChatRoomId, user.Id);
                                response.Message = string.Format("User {0} removed from chatroom {1}!",
                                    user.NickName, chatRoom.Name);

                                foreach (var kickHandler in Services.GetService<CommandHandlerRegistry>().GetHandlers<KickUser>())
                                {
                                    kickHandler.HandleCommand(communicator, new KickUser
                                    {
                                        ChatRoomId = membership.ChatRoomId,
                                        Nickname = membership.Nickname
                                    });
                                }
                            }

                            db.SaveChanges();
                            response.Result = true;
                        }
                        else
                        {
                            response.Message = string.Format(
                                "Not authorized to modify membership status of user {0} for chatroom {1}.",
                                user.NickName, chatRoom.Name);

                        }
                    }
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("ModifyChatRoomMembershipHandler failure.", e);
                    response.Message = "Exception encountered when attempting to modify membership settings : " + e.Message;
                }
            }

            communicator.Send(response);
        }
    }
}
