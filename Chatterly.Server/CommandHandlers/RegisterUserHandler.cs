﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="RegisterUser"/> request from the client.
    /// </summary>
    public class RegisterUserHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to register a nickname with authenticating information.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var reg = command as RegisterUser;

            using (var db = new ServerContext())
            {
                var userExists = db.Users.Count(u => u.NickName == reg.NickName) > 0;

                if (userExists)
                {
                    reg.IsRegistered = false;
                    reg.ErrorMessage = string.Format("User with nickname {0} already exists.", reg.NickName);
                    communicator.Send(reg);
                    return;
                }

                var user = new ServerUser
                {
                    NickName = reg.NickName,
                    Created = DateTime.UtcNow
                };

                user.PasswordHash = user.HashFromEncrpytedPassword(Convert.FromBase64String(reg.Password));

                db.Users.Add(user);
                db.SaveChanges();

                reg.IsRegistered = true;

                var authCmd = new AuthenticateUser
                {
                    User = new Core.Models.User
                    {
                        NickName = reg.NickName,
                        EncryptedPassword = reg.Password
                    }
                };

                foreach (var auth in Services.GetService<CommandHandlerRegistry>().GetHandlers<AuthenticateUser>())
                {
                    auth.HandleCommand(communicator, authCmd);
                }

                communicator.Send(reg);
            }
        }
    }
}
