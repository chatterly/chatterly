﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatterly.Server.CommandHandlers
{
    public class GetChatRoomBansHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as GetChatRoomBans;

            using (var db = new ServerContext())
            {
                var bans = db.GetChatRoomBans(cmd.ChatRoomId);

                if (bans != null)
                {
                    cmd.Bans = bans;
                }
            }

            communicator.Send(cmd);
        }
    }
}
