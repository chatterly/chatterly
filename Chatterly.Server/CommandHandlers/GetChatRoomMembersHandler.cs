﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;

namespace Chatterly.Server.CommandHandlers
{
    public class GetChatRoomMembersHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as GetChatRoomMembers;

            using (var db = new ServerContext())
            {
                var members = db.GetChatRoomMembers(cmd.ChatRoomId);

                if (members != null)
                {
                    cmd.Members = members;
                }
            }

            communicator.Send(cmd);
        }
    }
}
