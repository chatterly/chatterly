﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    public class ModifyBanHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var ban = command as ModifyBan;

            var response = new BoolResponse
            {
                Id = command.Id
            };

            if (ban == null)
            {
                response.Result = false;
                response.Message = "Received an invalid ModifyBan Command.";
                Services.GetService<LogManager>().Add(LogType.Error, response.Message);
            }
            else
            {
                try
                {
                    using (var db = new ServerContext())
                    {
                        var banningUser = db.GetUser(communicator.Session);
                        var user = db.GetUser(ban.Nickname);
                        var chatRoom = db.GetChatRoom(ban.ChatRoomId);
                        var chatRoomStaff = db.GetChatRoomStaff(ban.ChatRoomId);

                        var banningRank = chatRoomStaff.FirstOrDefault(s => s.UserId == banningUser.Id);
                        var userRank = chatRoomStaff.FirstOrDefault(s => s.UserId == user.Id);

                        if (banningRank != null && (userRank == null || banningRank.Rank > userRank.Rank))
                        {
                            if (ban.Action == BanAction.Ban)
                            {
                                db.BanUser(ban.ChatRoomId, user);
                                response.Message = string.Format("User {0} banned from chatroom {1}!",
                                    user.NickName, chatRoom.Name);

                                foreach (var kickHandler in Services.GetService<CommandHandlerRegistry>().GetHandlers<KickUser>())
                                {
                                    kickHandler.HandleCommand(communicator, new KickUser
                                    {
                                        ChatRoomId = ban.ChatRoomId,
                                        Nickname = ban.Nickname
                                    });
                                }
                            }
                            else if (ban.Action == BanAction.Unban)
                            {
                                db.UnbanUser(ban.ChatRoomId, user);
                                response.Message = string.Format("User {0} unbanned from chatroom {1}!",
                                    user.NickName, chatRoom.Name);
                            }

                            db.SaveChanges();
                            response.Result = true;
                        }
                        else
                        {
                            response.Message = string.Format(
                                "Not authorized to modify ban status of user {0} for chatroom {1}.",
                                user.NickName, chatRoom.Name);

                        }
                    }
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add("ModifyBanHandler failure.", e);
                    response.Message = "Exception encountered when attempting to modify ban settings : " + e.Message;
                }
            }

            communicator.Send(response);
        }
    }
}
