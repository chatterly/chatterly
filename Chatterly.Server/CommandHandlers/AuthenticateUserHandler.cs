﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles an <see cref="AuthenticateUser"/> request from the client.
    /// </summary>
    public class AuthenticateUserHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to authenticate a user.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            try
            {
                var auth = command as AuthenticateUser;
                auth.IsAuthenticated = false;

                try
                {
                    using (var db = new ServerContext())
                    {
                        var user = db.AuthenticateUser(auth.User, communicator.Session, communicator.IpAddress);

                        if (user != null)
                        {
                            auth.User = user.ToUser();
                            auth.IsAuthenticated = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    auth.IsAuthenticated = false;
                    Services.GetService<LogManager>().Add("Error authenticating", e);
                }

                communicator.Send(auth);
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("AuthenticateUserHandler Error", e);
            }
        }
    }
}
