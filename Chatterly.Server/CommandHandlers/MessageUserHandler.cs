﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Database;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="MessageChatRoom"/> request from the client.
    /// </summary>
    public class MessageUserHandler : ICommandHandler
    {
        /// <summary>
        /// Attempts to add a message to a chat room, if it exists, the user is authenticated, and the user has joined the room.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            Communicator receivingComm = null;
            var msg = command as MessageUser;

            try
            {

                using (var db = new ServerContext())
                {
                    //Sending user is connected
                    var User = (from s in db.UserSessions
                                join u in db.Users on s.UserId equals u.Id
                                where s.Session == communicator.Session
                                select u).FirstOrDefault();

                    if (User == null)
                    {
                        Services.GetService<LogManager>().Add(LogType.Info, "invalid sending user");
                        return;
                    }

                    //Receiving user is connected
                    var ReceivingUser = (from s in db.UserSessions
                                         where s.UserId == msg.RecipientUserId
                                         select s).FirstOrDefault();

                    if (ReceivingUser == null)
                    {
                        Services.GetService<LogManager>().Add(LogType.Info, "invalid receiving user");
                        return;
                    }

                    //Add real user info to the message
                    msg.Message.User = User.ToUser();
                    msg.Message.UserId = User.Id;

                    receivingComm = Services.GetService<ChatServer>().Clients[ReceivingUser.Session];

                    if (receivingComm == null)
                    {
                        throw new Exception(string.Format(
                            "Could not locate client communicator for user {0} and session {1}",
                            ReceivingUser.Id, ReceivingUser.Session));
                    }
                }
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("MessageUserHandler Failure", e);
            }

            //Simply route the message to the intended user
            receivingComm?.Send(msg);
        }
    }
}