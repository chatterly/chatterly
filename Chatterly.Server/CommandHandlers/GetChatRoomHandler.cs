﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Handles a <see cref="GetChatRoom"/> request from the client.
    /// </summary>
    public class GetChatRoomHandler : ICommandHandler
    {
        /// <summary>
        /// Gets information about a chat room, if it exists.
        /// </summary>
        /// <param name="communicator"></param>
        /// <param name="command"></param>
        void ICommandHandler.HandleCommand(Communicator communicator, ICommand command)
        {
            var get = command as GetChatRoom;

            try
            {
                using (var db = new ServerContext())
                {
                    get.ChatRoom = db.GetChatRoom(get.ChatRoom.Id);
                    get.Users = db.GetChatRoomUsers(get.ChatRoom.Id);
                    get.Members = db.GetChatRoomMembers(get.ChatRoom.Id);
                }
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("GetChatRoomHandler failed.", e);
            }

            communicator.Send(get);
        }
    }
}
