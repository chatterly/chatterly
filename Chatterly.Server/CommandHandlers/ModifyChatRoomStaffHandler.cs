﻿using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    /// <summary>
    /// Modify a user's staff position in a chatroom.
    /// Send back a <see cref="BoolResponse"/>.
    /// </summary>
    public class ModifyChatRoomStaffHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as ModifyChatRoomStaff;
            var response = new BoolResponse();
            response.Id = cmd.Id;

            try
            {
                using (var db = new ServerContext())
                {
                    var user = db.GetUser(communicator.Session);

                    if (user != null)
                    {
                        var modifierStaff = db.ChatRoomStaff
                            .Where(s => s.ChatRoomId == cmd.ChatRoomId && s.UserId == user.Id)
                            .FirstOrDefault();

                        if (modifierStaff != null)
                        {
                            var rank = modifierStaff.Rank;

                            var mod = (from u in db.Users
                                       from c in db.ChatRooms
                                       join s in db.ChatRoomStaff
                                       on new { id1 = u.Id, id2 = c.Id } equals new { id1 = s.UserId, id2 = s.ChatRoomId }
                                       into positions
                                       from staff in positions.DefaultIfEmpty()
                                       where u.NickName == cmd.Nickname
                                       where c.Id == cmd.ChatRoomId
                                       select new
                                       {
                                           Staff = staff,
                                           User = u,
                                           Chatroom = c
                                       }).FirstOrDefault();

                            if ((!cmd.Rank.HasValue || rank > cmd.Rank) && (mod.Staff == null || rank > mod.Staff.Rank))
                            {
                                if (mod.Staff == null)
                                {
                                    if (cmd.Rank.HasValue)
                                    {
                                        db.ChatRoomStaff.Add(new ChatRoomStaff
                                        {
                                            ChatRoomId = cmd.ChatRoomId,
                                            UserId = mod.User.Id,
                                            Rank = cmd.Rank.Value,
                                            Time = DateTime.UtcNow
                                        });

                                        response.Message = string.Format("{0} made {1} of chatroom {2}.",
                                            mod.User.NickName, cmd.Rank.Value, mod.Chatroom.Name);
                                    }
                                }
                                else if (cmd.Rank.HasValue)
                                {
                                    mod.Staff.Rank = cmd.Rank.Value;
                                    mod.Staff.Time = DateTime.UtcNow;
                                    db.Update(mod.Staff);

                                    response.Message = string.Format("{0} made {1} of chatroom {2}.",
                                            mod.User.NickName, cmd.Rank.Value, mod.Chatroom.Name);
                                }
                                else
                                {
                                    db.Remove(mod.Staff);

                                    response.Message = string.Format("{0} removed as {1} of chatroom {2}.",
                                            mod.User.NickName, mod.Staff.Rank, mod.Chatroom.Name);
                                }

                                db.SaveChanges();

                                if (!string.IsNullOrWhiteSpace(response.Message))
                                {
                                    response.Result = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("ModifyChatRoomHandler failure.", e);
            }

            if (string.IsNullOrWhiteSpace(response.Message))
            {
                response.Message = "Failed to update chatroom staff position for unknown reason.";
            }

            communicator.Send(response);
        }
    }
}
