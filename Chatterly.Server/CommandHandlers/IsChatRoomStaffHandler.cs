﻿using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.Database;
using System.Linq;

namespace Chatterly.Server.CommandHandlers
{
    public class IsChatRoomStaffHandler : ICommandHandler
    {
        public void HandleCommand(Communicator communicator, ICommand command)
        {
            var cmd = command as IsChatRoomStaff;

            using (var db = new ServerContext())
            {
                var staff = (from s in db.ChatRoomStaff
                             join u in db.Users on s.UserId equals u.Id
                             where u.NickName == cmd.User.NickName
                             where s.ChatRoomId == cmd.ChatRoomId
                             select new
                             {
                                 Rank = s.Rank,
                                 User = u
                             }).FirstOrDefault();

                if (staff != null)
                {
                    cmd.User = staff.User;
                    cmd.Rank = staff.Rank;
                }
            }

            communicator.Send(cmd);
        }
    }
}
