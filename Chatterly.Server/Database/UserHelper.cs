﻿using Chatterly.Core;
using Chatterly.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Extension class to make some stuff more convenient.
    /// </summary>
    public static class UserHelper
    {
        /// <summary>
        /// Creates a new <see cref="ServerUser"/> object from an existing <see cref="User"/> object.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static ServerUser ToServerUser(this User user)
        {
            var serverUser = new ServerUser();

            var props = user.GetType().GetProperties();

            foreach (var prop in props)
            {
                prop.SetValue(serverUser, prop.GetValue(user));
            }

            return serverUser;
        }

        /// <summary>
        /// Creates a new <see cref="User"/> object from an existing <see cref="ServerUser"/> object.
        /// </summary>
        /// <param name="serverUser"></param>
        /// <returns></returns>
        public static User ToUser(this ServerUser serverUser)
        {
            var user = new User();

            var props = user.GetType().GetProperties();

            foreach (var prop in props)
            {
                prop.SetValue(user, prop.GetValue(serverUser));
            }

            return user;
        }

        public static ServerUser AuthenticateUser(this ServerContext db, User user, Guid session, IPAddress ipAddress)
        {
            var serverUser = db.Users.FirstOrDefault(u => u.NickName == user.NickName);

            if (serverUser != null)
            {
                var authUser = user.ToServerUser();
                authUser.Salt = serverUser.Salt;
                var hash = authUser.HashFromEncrpytedPassword();

                if (serverUser.PasswordHash == hash)
                {
                    var sess = db.UserSessions.Where(s => s.Session == session);
                    db.UserSessions.RemoveRange(sess);

                    db.UserSessions.Add(new UserSession
                    {
                        UserId = serverUser.Id,
                        Session = session,
                        IpAddress = ipAddress,
                        Time = DateTime.UtcNow
                    });
                    db.SaveChanges();

                    return serverUser;
                }
            }

            return null;
        }

        public static bool DoesNickNameExist(this ServerContext db, string nickname)
        {
            return db.Users.Count(u => u.NickName == nickname) > 0;
        }

        /// <summary>
        /// Gets a <see cref="ServerUser"/> object if the user is authenticated with the given <see cref="Guid"/>.
        /// </summary>
        /// <param name="db">Active DB connection.</param>
        /// <param name="session">Session <see cref="Guid"/>.</param>
        /// <returns><see cref="ServerUser"/> object associated with the session ID or null.</returns>
        public static ServerUser GetUser(this ServerContext db, Guid session)
        {
            return (from s in db.UserSessions
                    join u in db.Users on s.UserId equals u.Id
                    where s.Session == session
                    orderby s.Id descending
                    select u).FirstOrDefault();
        }

        /// <summary>
        /// Gets a <see cref="ServerUser"/> object of the specified nickname.
        /// </summary>
        /// <param name="db">Active DB connection.</param>
        /// <param name="userId">User nickname.</param>
        /// <returns><see cref="ServerUser"/> object associated with the nickname or null.</returns>
        public static ServerUser GetUser(this ServerContext db, string nickname)
        {
            return (from u in db.Users
                    where u.NickName == nickname
                    select u).FirstOrDefault();
        }

        public static List<User> GetUsers(this ServerContext db, string startsWith)
        {
            return (from u in db.Users
                    where u.NickName.StartsWith(startsWith, StringComparison.CurrentCultureIgnoreCase)
                    select u).Select(u => u.ToUser()).ToList();
        }
    }
}
