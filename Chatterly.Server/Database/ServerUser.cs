﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Represents a user with information only relevant to the server.
    /// </summary>
    public class ServerUser : Core.Models.User
    {
        /// <summary>
        /// Hash of the password.
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Salt used in the hashing process.
        /// </summary>
        public string Salt { get; set; } = string.Empty;

        /// <summary>
        /// Generate a salt to user.
        /// </summary>
        /// <returns>Salt to use.</returns>
        public static string GenerateSalt()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var data = new byte[32];
                rng.GetBytes(data);
                return Convert.ToBase64String(data);
            }
        }

        /// <summary>
        /// Returns a hashed password from the given encrypted password.
        /// </summary>
        /// <returns>Hashed version of the password.</returns>
        public string HashFromEncrpytedPassword(byte[] password)
        {
            if (string.IsNullOrWhiteSpace(Salt))
            {
                Salt = GenerateSalt();
            }

            return HashPassword(Salt, Encoding.UTF8.GetString(Encryptor.Decrypt(password)));
        }

        /// <summary>
        /// Returns a hashed password from the given encrypted password. 
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public string HashFromEncrpytedPassword(string password)
        {
            return HashFromEncrpytedPassword(Convert.FromBase64String(password));
        }

        /// <summary>
        /// Returns a hashed password from the user's encrypted password.
        /// </summary>
        /// <returns></returns>
        public string HashFromEncrpytedPassword()
        {
            return HashFromEncrpytedPassword(EncryptedPassword);
        }

        /// <summary>
        /// Hash the password.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string HashPassword(string salt, string password)
        {
            var bytes = Encoding.UTF8.GetBytes(salt + password);
            var sha = new SHA256Managed();
            var hash = sha.ComputeHash(bytes);
            return string.Concat(hash.Select(h => h.ToString("X2")));
        }
    }
}
