﻿using Chatterly.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Represents a chat room message.
    /// </summary>
    public class ChatRoomMessage
    {
        /// <summary>
        /// ID of the chat room with which the message is associated.
        /// </summary>
        [Required]
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Navigation property to the chat room.
        /// </summary>
        [ForeignKey("ChatRoomId")]
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// ID of the message.
        /// </summary>
        [Required]
        public int MessageId { get; set; }

        /// <summary>
        /// Navigation property to the message.
        /// </summary>
        [ForeignKey("MessageId")]
        public Message Message { get; set; }
    }
}
