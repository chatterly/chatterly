﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Represents an authenticated user session.
    /// </summary>
    public class UserSession
    {
        /// <summary>
        /// ID of the authenticated user session.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ID of the authenticated user.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Client session associated with this authenticated user session.
        /// </summary>
        public Guid Session { get; set; }

        /// <summary>
        /// IP address data of the client.
        /// </summary>
        public byte[] IpAddressData { get; set; }

        /// <summary>
        /// Date and time that this authenticated user session was created.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// IP address of the client.
        /// Built from <see cref="IpAddressData"/>.
        /// </summary>
        [NotMapped]
        public IPAddress IpAddress
        {
            get
            {
                return IpAddressData == null ? null : new IPAddress(IpAddressData);
            }

            set
            {
                IpAddressData = value?.GetAddressBytes();
            }
        }

        /// <summary>
        /// Navigation property to the chat room subscriptions of this authenticated user session.
        /// </summary>
        public List<ChatRoomSubscription> ChatRoomSubscriptions { get; set; }
    }
}
