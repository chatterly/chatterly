﻿using Microsoft.EntityFrameworkCore;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Extension class to provide convenient operations.
    /// </summary>
    public static class DbExtension
    {
        /// <summary>
        /// Empty a table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbSet"></param>
        public static void Clear<T>(this DbSet<T> dbSet)
            where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
    }
}
