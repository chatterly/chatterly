﻿using Chatterly.Core.Database;
using Chatterly.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Server database context.
    /// </summary>
    public class ServerContext : ChatterlyContext
    {
        /// <summary>
        /// Lock to reduce multi-threaded errors.
        /// </summary>
        static readonly object _DbLock = new object();

        /// <summary>
        /// Chat room table.
        /// </summary>
        public DbSet<ChatRoom> ChatRooms { get; set; }

        /// <summary>
        /// Links chat rooms and users for a given staff position.
        /// </summary>
        public DbSet<ChatRoomStaff> ChatRoomStaff { get; set; }

        /// <summary>
        /// Users table.
        /// </summary>
        public DbSet<ServerUser> Users { get; set; }

        /// <summary>
        /// Messages table.
        /// </summary>
        public DbSet<Message> Messages { get; set; }

        /// <summary>
        /// Link table for messages and chat rooms.
        /// </summary>
        public DbSet<ChatRoomMessage> ChatRoomMessages { get; set; }

        /// <summary>
        /// Authenticated user session table.
        /// </summary>
        public DbSet<UserSession> UserSessions { get; set; }

        /// <summary>
        /// Link table for user sessions and chat rooms.
        /// </summary>
        public DbSet<ChatRoomSubscription> ChatRoomSubscriptions { get; set; }

        /// <summary>
        /// Table of chat room bans.
        /// </summary>
        public DbSet<ChatRoomBan> ChatRoomBans { get; set; }

        /// <summary>
        /// Table to link a user to a private chatroom.
        /// </summary>
        public DbSet<ChatRoomMember> ChatRoomMembers { get; set; }

        /// <summary>
        /// Configure the database connection.
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./Chatterly.Server.db");
        }

        /// <summary>
        /// Handle the model to table creation process.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChatRoomStaff>().HasKey(s => new { s.ChatRoomId, s.UserId });
            modelBuilder.Entity<ChatRoomMessage>().HasKey(l => new { l.ChatRoomId, l.MessageId });
            modelBuilder.Entity<ChatRoomSubscription>().HasKey(s => new { s.ChatRoomId, s.UserSessionId });
            modelBuilder.Entity<ChatRoomMember>().HasKey(m => new { m.ChatRoomId, m.UserId });

            modelBuilder.Entity<ChatRoomSubscription>()
                .HasOne(s => s.UserSession)
                .WithMany(s => s.ChatRoomSubscriptions)
                .OnDelete(DeleteBehavior.Cascade);
        }

        /// <summary>
        /// Ensure that the database is created and add a default chat room.
        /// </summary>
        public override void EnsureCreated()
        {
            lock (_DbLock)
            {
                base.EnsureCreated();

                if (!ChatRooms.Any())
                {
                    ChatRooms.Add(new ChatRoom
                    {
                        Name = "Development Chatroom",
                        Description = "Default chatroom!",
                        Created = DateTime.UtcNow
                    });
                    SaveChanges();
                }

                if (!Users.Any())
                {
                    var user = new ServerUser
                    {
                        NickName = "Admin",
                        Created = DateTime.UtcNow
                    };
                    user.Salt = ServerUser.GenerateSalt();
                    user.PasswordHash = ServerUser.HashPassword(user.Salt, "lol");

                    Users.Add(user);
                    SaveChanges();
                }

                if (!ChatRoomStaff.Any())
                {
                    ChatRoomStaff.Add(new ChatRoomStaff
                    {
                        ChatRoomId = 1,
                        UserId = 1,
                        Rank = ChatRoomRank.Owner,
                        Time = DateTime.UtcNow
                    });
                    SaveChanges();
                }
            }
        }
    }
}
