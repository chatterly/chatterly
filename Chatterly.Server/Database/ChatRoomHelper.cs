﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chatterly.Server.Database
{
    public static class ChatRoomHelper
    {
        public static ChatRoom GetChatRoom(this ServerContext db, int chatRoomId)
        {
            return db.ChatRooms.FirstOrDefault(c => c.Id == chatRoomId);
        }

        public static List<User> GetChatRoomUsers(this ServerContext db, int chatRoomId)
        {
            return (from sub in db.ChatRoomSubscriptions
                    join s in db.UserSessions on sub.UserSessionId equals s.Id
                    join u in db.Users on s.UserId equals u.Id
                    where sub.ChatRoomId == chatRoomId
                    select u).Cast<User>().ToList();
        }

        public static List<User> GetChatRoomMembers(this ServerContext db, int chatRoomId)
        {
            return (from m in db.ChatRoomMembers
                    join u in db.Users on m.UserId equals u.Id
                    where m.ChatRoomId == chatRoomId
                    select u).Select(u => u.ToUser()).ToList();
        }

        public static List<ChatRoomBan> GetChatRoomBans(this ServerContext db, int chatRoomId)
        {
            return (from b in db.ChatRoomBans
                    where b.ChatRoomId == chatRoomId
                    select b)
                    .Include(b => b.ChatRoom).Include(b => b.User)
                    .ToList();
        }

        public static List<GetChatRooms.ChatRoomMeta> GetChatRooms(this ServerContext db)
        {
            var chatRooms = db.ChatRooms.Select(c => new GetChatRooms.ChatRoomMeta
            {
                ChatRoom = c,
                UserCount = (from sub in db.ChatRoomSubscriptions
                             join s in db.UserSessions on sub.UserSessionId equals s.Id
                             where sub.ChatRoomId == c.Id
                             select sub).Count()
            }).ToList();

            return chatRooms;
        }

        public static List<ChatRoomStaff> GetChatRoomStaff(this ServerContext db, int chatRoomId)
        {
            return db.ChatRoomStaff.Include(s => s.User).Where(s => s.ChatRoomId == chatRoomId).ToList();
        }

        public static void BanUser(this ServerContext db, int chatRoomId, ServerUser user)
        {
            var session = db.UserSessions.OrderByDescending(s => s.Time).FirstOrDefault(s => s.UserId == user.Id);

            db.ChatRoomBans.Add(new ChatRoomBan
            {
                ChatRoomId = chatRoomId,
                UserId = user.Id,
                IpAddress = session?.IpAddress,
                Time = DateTime.UtcNow
            });
        }

        public static void UnbanUser(this ServerContext db, int chatRoomId, ServerUser user)
        {
            var session = db.UserSessions.OrderByDescending(s => s.Time).FirstOrDefault(s => s.UserId == user.Id);

            var remove = db.ChatRoomBans.Where(b => b.UserId == user.Id);
            db.ChatRoomBans.RemoveRange(remove);
        }

        public static bool IsChatRoomMember(this ServerContext db, int chatRoomId, int userId)
        {
            return db.ChatRoomMembers.Any(m => m.ChatRoomId == chatRoomId && m.UserId == userId);
        }

        public static bool IsChatRoomStaff(this ServerContext db, int chatRoomId, int userId)
        {
            return db.ChatRoomStaff.Any(m => m.ChatRoomId == chatRoomId && m.UserId == userId);
        }

        public static bool IsChatRoomStaff(this ServerContext db, int chatRoomId, int userId, ChatRoomRank rank)
        {
            return db.ChatRoomStaff.Any(m => m.ChatRoomId == chatRoomId && m.UserId == userId && m.Rank == rank);
        }

        public static ChatRoomRank? GetChatRoomStaffRank(this ServerContext db, int chatRoomId, int userId)
        {
            return db.ChatRoomStaff.FirstOrDefault(m => m.ChatRoomId == chatRoomId && m.UserId == userId)?.Rank;
        }

        public static void AddChatRoomMember(this ServerContext db, int chatRoomId, int userId)
        {
            db.ChatRoomMembers.Add(new ChatRoomMember
            {
                ChatRoomId = chatRoomId,
                UserId = userId,
                Time = DateTime.UtcNow
            });
        }

        public static void RemoveChatRoomMember(this ServerContext db, int chatRoomId, int userId)
        {
            var remove = db.ChatRoomMembers.Where(m => m.ChatRoomId == chatRoomId && m.UserId == userId);
            db.ChatRoomMembers.RemoveRange(remove);
        }
    }
}
