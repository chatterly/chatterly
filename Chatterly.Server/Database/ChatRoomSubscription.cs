﻿using Chatterly.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chatterly.Server.Database
{
    /// <summary>
    /// Represents a chat room subscription.
    /// </summary>
    public class ChatRoomSubscription
    {
        /// <summary>
        /// Id of the chat room to which the user is subscribed.
        /// </summary>
        [Required]
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Navigation property to the chat room.
        /// </summary>
        [ForeignKey("ChatRoomId")]
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// ID of the authenticated user session.
        /// </summary>
        [Required]
        public int UserSessionId { get; set; }

        /// <summary>
        /// Navigation property to the user session.
        /// </summary>
        [ForeignKey("UserSessionId")]
        public UserSession UserSession { get; set; }

        /// <summary>
        /// Date and time that the subscription was made.
        /// </summary>
        public DateTime Time { get; set; }
    }
}
