﻿using Chatterly.Core.Crypto;
using Chatterly.Core.Protocol;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Represents a generic user.
    /// </summary>
    public class User : IMessage
    {
        /// <summary>
        /// Encryption algorithm used to encrypt and decrypt passwords.
        /// </summary>
        public static IEncryptor Encryptor { get; set; }

        /// <summary>
        /// ID of the user.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Nickname / alias used by the user.
        /// </summary>
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// Encrpyted password.
        /// </summary>
        [NotMapped]
        public string EncryptedPassword { get; set; }

        /// <summary>
        /// Date and time at which the user was registered.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public User() { }

        /// <summary>
        /// Automatically reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Data representing the user.</param>
        public User(byte[] bytes)
        {
            Deserialize(bytes);
        }

        /// <summary>
        /// Encrypt the password and return a base 64 string.
        /// </summary>
        /// <param name="password">Password string.</param>
        /// <returns>Base 64 string.</returns>
        public static string EncryptPassword(string password)
        {
            return Convert.ToBase64String(Encryptor.Encrypt(Encoding.UTF8.GetBytes(password)));
        }

        /// <summary>
        /// Serialize the user.
        /// </summary>
        /// <returns>Data representing the user.</returns>
        public byte[] Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Id);
                writer.Write(NickName ?? string.Empty);
                writer.Write(EncryptedPassword ?? string.Empty);
                writer.Write(Created.ToBinary());
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Reconstruct the user object.
        /// </summary>
        /// <param name="bytes">Data from which the object is to be reconstructed.</param>
        public void Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                Id = reader.ReadInt32();
                NickName = reader.ReadString();
                EncryptedPassword = reader.ReadString();
                Created = DateTime.FromBinary(reader.ReadInt64());
            }
        }

        public override string ToString()
        {
            return NickName;
        }
    }
}
