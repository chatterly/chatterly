﻿using Chatterly.Core.Protocol;
using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Represents a chat room.
    /// </summary>
    public class ChatRoom : IMessage
    {
        /// <summary>
        /// ID of the chat room.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Name of the chat room.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of the chat room.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Flag to determine if the chatroom is private.
        /// </summary>
        public bool IsPrivate { get; set; } = false;

        /// <summary>
        /// Date and time that the chat room was created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public ChatRoom() { }

        /// <summary>
        /// Automatically reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Data representing the user.</param>
        public ChatRoom(byte[] bytes)
        {
            Deserialize(bytes);
        }

        /// <summary>
        /// Serialize the chat room.
        /// </summary>
        /// <returns>Data describing this object.</returns>
        public byte[] Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Id);
                writer.Write(Name ?? string.Empty);
                writer.Write(Description ?? string.Empty);
                writer.Write(Created.ToBinary());
                writer.Write(IsPrivate);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Serialization data.</param>
        public void Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                Id = reader.ReadInt32();
                Name = reader.ReadString();
                Description = reader.ReadString();
                Created = DateTime.FromBinary(reader.ReadInt64());
                IsPrivate = reader.ReadBoolean();
            }
        }
    }
}
