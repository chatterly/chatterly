﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Represents a user who can go into the associated private chatroom.
    /// </summary>
    public class ChatRoomMember
    {
        /// <summary>
        /// ID of the chatroom from which the user is banned.
        /// </summary>
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Navigation property to the chat room.
        /// </summary>
        [ForeignKey("ChatRoomId")]
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// ID of the banned user.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Navigation property to the user.
        /// </summary>
        [ForeignKey("UserId")]
        public User User { get; set; }

        /// <summary>
        /// Date/time when the ban was processed.
        /// </summary>
        public DateTime Time { get; set; }
    }
}
