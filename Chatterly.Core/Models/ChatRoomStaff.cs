﻿using Chatterly.Core.Protocol;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Ranks of chatroom staff positions.
    /// </summary>
    public enum ChatRoomRank
    {
        /// <summary>
        /// Owner of the chatroom.
        /// </summary>
        Owner = 100,
        /// <summary>
        /// Administrator of the chatroom.
        /// </summary>
        Admin = 50,
        /// <summary>
        /// Moderator of the chatroom.
        /// </summary>
        Mod = 10
    }

    /// <summary>
    /// Appoints a user to a staff position in a chatroom.
    /// </summary>
    public class ChatRoomStaff : IMessage
    {
        /// <summary>
        /// ID of the chatroom to which this staff position applies.
        /// </summary>
        [ForeignKey("ChatRoom")]
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Navigation property to the chatroom.
        /// </summary>
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// ID of the user to which this staff position applies.
        /// </summary>
        [ForeignKey("User")]
        public int UserId { get; set; }

        /// <summary>
        /// Navigation property to the user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Rank of this staff position.
        /// </summary>
        public ChatRoomRank Rank { get; set; }

        /// <summary>
        /// Time that this staff position was created.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public ChatRoomStaff() { }

        /// <summary>
        /// Automatically reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Data representing the user.</param>
        public ChatRoomStaff(byte[] bytes)
        {
            Deserialize(bytes);
        }

        /// <summary>
        /// Reconstruct this object from serialization data.
        /// </summary>
        /// <param name="bytes"></param>
        public void Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
                UserId = reader.ReadInt32();
                Rank = (ChatRoomRank)Enum.Parse(typeof(ChatRoomRank), reader.ReadString());
                Time = DateTime.FromBinary(reader.ReadInt64());

                var length = reader.ReadInt32();
                if (length > 0)
                {
                    ChatRoom = new ChatRoom(reader.ReadBytes(length));
                }

                length = reader.ReadInt32();
                if (length > 0)
                {
                    User = new User(reader.ReadBytes(length));
                }
            }
        }

        /// <summary>
        /// Reduce this object to a descriptive data set.
        /// </summary>
        /// <returns></returns>
        public byte[] Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                writer.Write(UserId);
                writer.Write(Rank.ToString());
                writer.Write(Time.ToBinary());

                var bytes = ChatRoom?.Serialize() ?? new byte[0];

                writer.Write(bytes.Length);
                writer.Write(bytes);

                bytes = User?.Serialize() ?? new byte[0];

                writer.Write(bytes.Length);
                writer.Write(bytes);

                return writer.ToBytes();
            }
        }
    }
}
