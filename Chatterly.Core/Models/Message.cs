﻿using Chatterly.Core.Protocol;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Represents a chat message.
    /// </summary>
    public class Message : IMessage
    {
        /// <summary>
        /// ID of this message.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ID of the user who sent this message,
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Navigation property to the sending user.
        /// </summary>
        [ForeignKey("UserId")]
        public User User { get; set; }

        /// <summary>
        /// Content of this message (i.e. the message itself).
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Date and time that this message was sent.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public Message()
        { }

        /// <summary>
        /// Automatically reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes"></param>
        public Message(byte[] bytes)
        {
            Deserialize(bytes);
        }

        /// <summary>
        /// Serialize this object.
        /// </summary>
        /// <returns>Data describing this object.</returns>
        public byte[] Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Id);
                writer.Write(UserId);
                writer.Write(Content ?? string.Empty);
                writer.Write(Time.ToBinary());

                var bytes = User?.Serialize() ?? new byte[0];

                writer.Write(bytes.Length);
                writer.Write(bytes);

                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Serialization data.</param>
        public void Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                Id = reader.ReadInt32();
                UserId = reader.ReadInt32();
                Content = reader.ReadString();
                Time = DateTime.FromBinary(reader.ReadInt64());

                var length = reader.ReadInt32();
                if (length > 0)
                {
                    User = new User(reader.ReadBytes(length));
                }
            }
        }
    }
}
