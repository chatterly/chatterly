﻿using Chatterly.Core.Protocol;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Chatterly.Core.Models
{
    /// <summary>
    /// Represents a ban of a user from a chatroom.
    /// </summary>
    public class ChatRoomBan : IMessage
    {
        /// <summary>
        /// ID of the ban record.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ID of the chatroom from which the user is banned.
        /// </summary>
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Navigation property to the chat room.
        /// </summary>
        [ForeignKey("ChatRoomId")]
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// ID of the banned user.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Navigation property to the user.
        /// </summary>
        [ForeignKey("UserId")]
        public User User { get; set; }

        /// <summary>
        /// IP address of the user when the ban was processed.
        /// </summary>
        public byte[] IpAddressData { get; set; }

        /// <summary>
        /// Date/time when the ban was processed.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// IP address of the user when the ban was processed.
        /// </summary>
        [NotMapped]
        public IPAddress IpAddress
        {
            get
            {
                return new IPAddress(IpAddressData);
            }

            set
            {
                IpAddressData = value?.GetAddressBytes();
            }
        }

        public byte[] Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                writer.Write(UserId);
                writer.Write(IpAddressData ?? new byte[4]);
                writer.Write(Time.ToBinary());

                var bytes = ChatRoom?.Serialize() ?? new byte[0];

                writer.Write(bytes.Length);
                writer.Write(bytes);

                bytes = User?.Serialize() ?? new byte[0];

                writer.Write(bytes.Length);
                writer.Write(bytes);

                return writer.ToBytes();
            }
        }

        public void Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
                UserId = reader.ReadInt32();
                IpAddressData = reader.ReadBytes(4);
                Time = DateTime.FromBinary(reader.ReadInt64());

                var length = reader.ReadInt32();
                if (length > 0)
                {
                    ChatRoom = new ChatRoom(reader.ReadBytes(length));
                }

                length = reader.ReadInt32();
                if (length > 0)
                {
                    User = new User(reader.ReadBytes(length));
                }
            }
        }
    }
}
