﻿using System.IO;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Class to make working with binary data and stream somewhat more convenient.
    /// </summary>
    public static class BinaryHelper
    {
        /// <summary>
        /// Converts a stream to a byte array.
        /// </summary>
        /// <param name="stream">Stream to convert</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToBytes(this Stream stream)
        {
            if (stream is MemoryStream)
            {
                return (stream as MemoryStream).ToArray();
            }

            using (MemoryStream mem = new MemoryStream())
            {
                stream.CopyTo(mem);
                return mem.ToArray();
            }
        }

        /// <summary>
        /// Converts a binary writer to a byte array.
        /// </summary>
        /// <param name="writer">Binary writer to convert.</param>
        /// <returns>Byte array</returns>
        public static byte[] ToBytes(this BinaryWriter writer)
        {
            return writer.BaseStream.ToBytes();
        }

        /// <summary>
        /// Get a binary reader to read a byte array.
        /// </summary>
        /// <param name="bytes">Byte array we want to read.</param>
        /// <returns>Binary reader.</returns>
        public static BinaryReader GetReader(this byte[] bytes)
        {
            return new BinaryReader(new MemoryStream(bytes));
        }

        /// <summary>
        /// Get a binary writer for a given stream.
        /// </summary>
        /// <param name="stream">Stream to which we want to write.</param>
        /// <returns>Binary writer.</returns>
        public static BinaryWriter GetWriter(this Stream stream)
        {
            return new BinaryWriter(stream);
        }

        /// <summary>
        /// Creates a new binary writer.
        /// </summary>
        /// <returns>Binary writer.</returns>
        public static BinaryWriter GetWriter()
        {
            return (new MemoryStream()).GetWriter();
        }
    }
}
