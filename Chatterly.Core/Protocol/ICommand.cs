﻿using System;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Represents a command to be sent such that the recipient executes some action.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// Unique identifier so we can associate a response with a request.
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        byte[] CreatePrimitiveData();

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        IMessage[] CreatePayloadData();

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        void ParsePayload(Payload msg);
    }
}
