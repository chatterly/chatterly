﻿namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Represents some network message.
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Serialize the object such that it can be sent across the network.
        /// </summary>
        /// <returns>Data that describes the object.</returns>
        byte[] Serialize();

        /// <summary>
        /// Reconstructs the object from serialization data.
        /// </summary>
        /// <param name="bytes">Serialization data.</param>
        void Deserialize(byte[] bytes);
    }
}