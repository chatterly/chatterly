﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Associated command and command handlers.
    /// </summary>
    public class CommandHandlerRegistry
    {
        /// <summary>
        /// Mapping of command types to command handlers.
        /// </summary>
        ConcurrentDictionary<Type, List<ICommandHandler>> Handlers
        {
            get
            {
                return _Handlers;
            }
        }
        readonly ConcurrentDictionary<Type, List<ICommandHandler>> _Handlers = new ConcurrentDictionary<Type, List<ICommandHandler>>();

        /// <summary>
        /// Ensure that the type is a valid <see cref="ICommand"/> implementation.
        /// </summary>
        /// <param name="t">Type to check.</param>
        /// <exception cref="ArgumentException">Provided type does not implement <see cref="ICommand"/>.</exception>
        void EnsureType(Type t)
        {
            if (!typeof(ICommand).IsAssignableFrom(t))
            {
                throw new ArgumentException("Provided type does not implement ICommand.");
            }
        }

        /// <summary>
        /// Ensure that the mapping is ready for this type.
        /// </summary>
        /// <param name="t">Type</param>
        void EnsureCollection(Type t)
        {
            if (!_Handlers.ContainsKey(t))
            {
                _Handlers.TryAdd(t, new List<ICommandHandler>());
            }
        }

        /// <summary>
        /// Register the given <see cref="ICommandHandler"/> for the given <see cref="ICommand"/> type.
        /// </summary>
        /// <typeparam name="Command"><see cref="ICommand"/> type.</typeparam>
        /// <param name="handler"><see cref="ICommandHandler"/> we want to run for the given <see cref="ICommand"/> type.</param>
        public void RegisterHandler<Command>(ICommandHandler handler)
            where Command : ICommand
        {
            var commandType = typeof(Command);
            RegisterHandler(commandType, handler);
        }

        /// <summary>
        /// Register the given <see cref="ICommandHandler"/> for the given <see cref="ICommand"/> type.
        /// </summary>
        /// <param name="commandType"><see cref="ICommand"/> type.</param>
        /// <param name="handler"><see cref="ICommandHandler"/> we want to run for the given <see cref="ICommand"/> type.</param>
        public void RegisterHandler(Type commandType, ICommandHandler handler)
        {
            EnsureType(commandType);
            EnsureCollection(commandType);

            _Handlers[commandType].Add(handler);
        }

        /// <summary>
        /// Get the registered <see cref="ICommandHandler"/> instances for the given <see cref="ICommand"/> type.
        /// </summary>
        /// <typeparam name="Command"><see cref="ICommand"/> type.</typeparam>
        /// <returns>List of <see cref="ICommandHandler"/> objects for the given <see cref="ICommand"/> type.</returns>
        public IEnumerable<ICommandHandler> GetHandlers<Command>()
            where Command : ICommand
        {
            var commandType = typeof(Command);

            return GetHandlers(commandType);
        }

        /// <summary>
        /// Get the registered <see cref="ICommandHandler"/> instances for the given <see cref="ICommand"/> type.
        /// </summary>
        /// <param name="commandType"><see cref="ICommand"/> type.</param>
        /// <returns>List of <see cref="ICommandHandler"/> objects for the given <see cref="ICommand"/> type.</returns>
        public IEnumerable<ICommandHandler> GetHandlers(Type commandType)
        {
            EnsureType(commandType);
            EnsureCollection(commandType);

            return _Handlers[commandType];
        }
    }
}
