﻿using Chatterly.Core.Database;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Class through which all network communication occurs.
    /// </summary>
    public class Communicator
    {
        /// <summary>
        /// Number of bytes that comprise a GUID.
        /// </summary>
        public static readonly int GUID_SIZE = 16;

        /// <summary>
        /// Client object.
        /// </summary>
        protected TcpClient Client { get; private set; }

        /// <summary>
        /// Convenience property to access the client's network stream.
        /// </summary>
        protected NetworkStream Stream
        {
            get
            {
                return Client.GetStream();
            }
        }

        /// <summary>
        /// Unique identifier for this client.
        /// </summary>
        public Guid Session
        {
            get
            {
                return _Session;
            }
        }
        readonly Guid _Session = Guid.NewGuid();

        /// <summary>
        /// IP address to which we want to connect.
        /// </summary>
        public IPAddress IpAddress
        {
            get { return ((IPEndPoint)Client.Client.RemoteEndPoint).Address; }
        }

        /// <summary>
        /// Task to listen for communication.
        /// </summary>
        protected Task Listener { get; set; }

        /// <summary>
        /// Method by which tasks can be canceled.
        /// </summary>
        protected CancellationTokenSource Cancel { get; set; }

        /// <summary>
        /// Mapping of command IDs to callbacks.
        /// </summary>
        protected ConcurrentDictionary<Guid, Action<ICommand>> Callbacks
        {
            get
            {
                return _Callbacks;
            }
        }
        readonly ConcurrentDictionary<Guid, Action<ICommand>> _Callbacks = new ConcurrentDictionary<Guid, Action<ICommand>>();

        /// <summary>
        /// Construct a communicator with the given client.
        /// </summary>
        /// <param name="client"></param>
        public Communicator(TcpClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Start the listener.
        /// </summary>
        public void Start()
        {
            Cancel = new CancellationTokenSource();
            Listener = Task.Factory.StartNew(Listen_DoWork);
        }

        /// <summary>
        /// Stop the listener.
        /// </summary>
        public void Stop()
        {
            Cancel.Cancel();
            Listener = null;
        }

        /// <summary>
        /// Determine if the connection is still good.
        /// </summary>
        /// <returns>True if the connection is still good.</returns>
        public bool Poll()
        {
            try
            {
                if (Client.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buff = new byte[1];
                    if (Client.Client.Receive(buff, SocketFlags.Peek) == 0)
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Send a command to the recipient.
        /// </summary>
        /// <param name="cmd">Command to send.</param>
        /// <param name="callback">Optional callback to execute if a response is received.</param>
        public void Send(ICommand cmd, Action<ICommand> callback = null)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd", "Communicator.Send requires a valid ICommand object.");
            }

            if (cmd.Id == Guid.Empty)
            {
                cmd.Id = Guid.NewGuid();
            }

            byte[] primitiveData = null;
            IMessage[] messageData = null;

            try
            {
                primitiveData = cmd.CreatePrimitiveData();
            }
            catch (Exception e)
            {
                throw new Exception("Communicator.Send: ICommand failed to create primitive data.", e);
            }

            try
            {
                messageData = cmd.CreatePayloadData();
            }
            catch (Exception e)
            {
                throw new Exception("Communicator.Send: ICommand failed to create payload data.", e);
            }

            var payload = new Payload
            {
                CommandType = cmd.GetType().FullName,
                Time = DateTime.UtcNow,
                Session = Session,
                PrimitiveData = primitiveData,
                Messages = messageData
            };

            byte[] buffer = null;

            try
            {
                buffer = (payload as IMessage).Serialize();
            }
            catch (Exception e)
            {
                throw new Exception("Communicator.Send: Payload serialization failed.", e);
            }

            if (callback != null)
            {
                Callbacks.TryAdd(cmd.Id, callback);
            }

            using (var writer = BinaryHelper.GetWriter())
            {
                var idData = cmd.Id.ToByteArray();

                try
                {
                    writer.Write(buffer.Length + idData.Length);
                    writer.Write(buffer);
                    writer.Write(idData);
                }
                catch (Exception e)
                {
                    throw new Exception("Communicator.Send: Failed to write to memory stream.", e);
                }

                byte[] data = null;

                try
                {
                    data = writer.ToBytes();
                }
                catch (Exception e)
                {
                    throw new Exception("Communicator.Send: Failed to convert memory stream to byte array.", e);
                }

                try
                {
                    Stream.Write(data, 0, data.Length);
                    Debug.WriteLine(string.Format("Sent command {0}: {1}", cmd.GetType().Name, cmd.Id));
                }
                catch (Exception e)
                {
                    throw new Exception("Communicator.Send: Failed to write data to network stream.", e);
                }
            }
        }

        /// <summary>
        /// Rebuilds a command with the given payload data.
        /// </summary>
        /// <param name="payload">Payload data.</param>
        /// <returns>Rebuilt command.</returns>
        protected virtual ICommand RebuildCommand(Payload payload)
        {
            var commandType = ProtocolTypeManager.GetType(payload.CommandType);

            if (commandType == null)
            {
                Services.GetService<LogManager>().Add(LogType.Error,
                    string.Format("Could not rebuild command. Unknown type: {0}", payload.CommandType));
                return null;
            }

            var command = Activator.CreateInstance(commandType) as ICommand;

            if (command == null)
            {
                Services.GetService<LogManager>().Add(LogType.Error, "Could not rebuild command.");
                return null;
            }

            command.ParsePayload(payload);

            return command;
        }

        /// <summary>
        /// Handles the command appropriately.
        /// </summary>
        /// <param name="command">Command to handle.</param>
        protected virtual void HandleCommand(ICommand command)
        {
            var handlers = Services.GetService<CommandHandlerRegistry>()?.GetHandlers(command.GetType());

            if (handlers == null)
            {
                Services.GetService<LogManager>().Add(LogType.Warning, "Communicator.HandleCommand: " +
                    "No ICommandHandlers registered for command type " + command.GetType().FullName);
            }
            else
            {
                foreach (var handler in handlers)
                {
                    handler.HandleCommand(this, command);
                }
            }
        }

        /// <summary>
        /// Listens for incoming network communication.
        /// </summary>
        protected void Listen_DoWork()
        {
            Thread.Sleep(25);
            try
            {
                while (!(Listener.IsCanceled || Listener.IsFaulted))
                {
                    if (Cancel.Token.IsCancellationRequested)
                    {
                        Cancel.Token.ThrowIfCancellationRequested();
                    }

                    try
                    {
                        if (Client.Connected && Client.Available > 0)
                        {
                            var sizeBuffer = new byte[sizeof(int)];
                            Stream.Read(sizeBuffer, 0, sizeBuffer.Length);
                            var size = BitConverter.ToInt32(sizeBuffer, 0);

                            var buffer = new byte[size];

                            Stream.Read(buffer, 0, buffer.Length);

                            var stream = new MemoryStream(buffer);

                            // compensate for the Guid auto-wrote to the end of every message
                            var payloadBuffer = new byte[buffer.Length - GUID_SIZE];

                            stream.Read(payloadBuffer, 0, payloadBuffer.Length);

                            var payload = new Payload(payloadBuffer);
                            var command = RebuildCommand(payload);

                            if (command != null)
                            {
                                var reader = new BinaryReader(stream);
                                command.Id = new Guid(reader.ReadBytes(GUID_SIZE));

                                Debug.WriteLine(string.Format("Received command {0}: {1}", command.GetType().Name, command.Id));

                                HandleCommand(command);

                                Action<ICommand> callback;
                                if (Callbacks.TryGetValue(command.Id, out callback))
                                {
                                    callback?.Invoke(command);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Services.GetService<LogManager>().Add("In-While Communicator Listener Error", e);
                        Thread.Sleep(1000);
                    }

                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                Services.GetService<LogManager>().Add("Around-While Communicator Listener Error", e);
            }
        }
    }
}