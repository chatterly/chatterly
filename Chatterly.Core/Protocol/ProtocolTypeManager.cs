﻿using System;
using System.Collections.Concurrent;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Finds types related to the protocol system that may need to be instantiated through reflection and creates a map
    /// of their name to their type.
    /// </summary>
    public class ProtocolTypeManager
    {
        /// <summary>
        /// Mapping of type name to the type.
        /// </summary>
        static readonly ConcurrentDictionary<string, Type> ProtocolTypes =
            new ConcurrentDictionary<string, Type>();

        /// <summary>
        /// Finds the relevant types and create the mapping.
        /// </summary>
        static ProtocolTypeManager()
        {
            // find all relevant protocol types in all assemblies and index by full name
            foreach (var assm in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assm.GetTypes())
                {
                    var isMessage = typeof(IMessage).IsAssignableFrom(type);
                    var isCommand = typeof(ICommand).IsAssignableFrom(type);
                    var isHandler = typeof(ICommandHandler).IsAssignableFrom(type);

                    if (isMessage || isCommand || isHandler)
                    {
                        ProtocolTypes.TryAdd(type.FullName, type);
                    }
                }
            }
        }

        /// <summary>
        /// Get the type based on its full name.
        /// </summary>
        /// <param name="fullName">Full name of the type.</param>
        /// <returns><see cref="Type"/> object associated with the given full name.</returns>
        public static Type GetType(string fullName)
        {
            Type type = null;

            if (ProtocolTypes.TryGetValue(fullName, out type))
            {
                return type;
            }

            return null;
        }
    }
}
