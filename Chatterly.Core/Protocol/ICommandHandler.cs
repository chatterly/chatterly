﻿namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Represents the interface by which <see cref="ICommand"/> objects are handled.
    /// </summary>
    public interface ICommandHandler
    {
        /// <summary>
        /// Handles the given <see cref="ICommand"/> command and responds to the <see cref="Communicator"/>.
        /// </summary>
        /// <param name="communicator">Communicator from which the command was sent to which we need to respond.</param>
        /// <param name="command">Command we need to handle.</param>
        void HandleCommand(Communicator communicator, ICommand command);
    }
}
