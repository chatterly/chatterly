﻿using System;
using System.Collections.Generic;

namespace Chatterly.Core.Protocol
{
    /// <summary>
    /// Object that facilitates transmission of a <see cref="ICommand"/> over the network.
    /// </summary>
    public class Payload : IMessage
    {
        /// <summary>
        /// Unique session identifier to ID this client/server.
        /// </summary>
        public Guid Session { get; set; }

        /// <summary>
        /// Time command was sent.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Type of the command to execute. Must be an implementation of <see cref="ICommand"/>.
        /// </summary>
        public string CommandType { get; set; }

        /// <summary>
        /// Primitive data that was attached directly to the command.
        /// </summary>
        public byte[] PrimitiveData { get; set; }

        /// <summary>
        /// Messages in tow.
        /// </summary>
        public IMessage[] Messages { get; set; }

        /// <summary>
        /// Empty constructor!
        /// </summary>
        public Payload() { }

        /// <summary>
        /// Automatically reconstruct the object from serialization data.
        /// </summary>
        /// <param name="bytes">Serialization data.</param>
        public Payload(byte[] bytes)
        {
            (this as IMessage).Deserialize(bytes);
        }

        /// <summary>
        /// Automatically populate <see cref="Payload"/> object with necessary command data.
        /// </summary>
        /// <param name="cmd"><see cref="ICommand"/> object.</param>
        public Payload(ICommand cmd)
        {
            CommandType = cmd.GetType().FullName;
            PrimitiveData = cmd.CreatePrimitiveData();
            Messages = cmd.CreatePayloadData();
            Time = DateTime.UtcNow;
        }

        /// <summary>
        /// Serialize the object.
        /// </summary>
        /// <returns>Data that describes the object.</returns>
        byte[] IMessage.Serialize()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Session.ToByteArray());
                writer.Write(Time.ToBinary());
                writer.Write(CommandType);

                try
                {
                    writer.Write(PrimitiveData.Length);
                    writer.Write(PrimitiveData);
                }
                catch (Exception e)
                {
                    throw new Exception("Payload.Serialize failed to write PrimitiveData to memory stream.", e);
                }
                try
                {
                    foreach (var message in Messages)
                    {
                        if (message == null)
                        {
                            writer.Write(0);
                        }
                        else
                        {
                            var bytes = message.Serialize();
                            writer.Write(bytes.Length);
                            writer.Write(message.GetType().Assembly.GetName().Name);
                            writer.Write(message.GetType().FullName);
                            writer.Write(bytes);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Payload.Serialize failed to write Messages to memory stream.", e);
                }

                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Reconstructs the object from serialization data.
        /// </summary>
        /// <param name="bytes">Serialization data.</param>
        void IMessage.Deserialize(byte[] bytes)
        {
            using (var reader = bytes.GetReader())
            {
                Session = new Guid(reader.ReadBytes(Communicator.GUID_SIZE));
                Time = DateTime.FromBinary(reader.ReadInt64());
                CommandType = reader.ReadString();

                var dataLength = reader.ReadInt32();
                PrimitiveData = reader.ReadBytes(dataLength);

                var messages = new List<IMessage>();

                try
                {
                    while (reader.BaseStream.Position < reader.BaseStream.Length)
                    {
                        var length = reader.ReadInt32();

                        if (length > 0)
                        {
                            var assmName = reader.ReadString();
                            var typeName = reader.ReadString();

                            // this will only work if the assembly is currently loaded
                            var type = ProtocolTypeManager.GetType(typeName);

                            var message = Activator.CreateInstance(type) as IMessage;
                            message.Deserialize(reader.ReadBytes(length));

                            messages.Add(message);
                        }
                        else
                        {
                            messages.Add(null);
                        }
                    }
                }
                catch (Exception e)
                {
                    Services.GetService<LogManager>().Add(
                        string.Format("Exception encountered when processing messages for command type {0}", CommandType),
                        e);
                }

                Messages = messages.ToArray();
            }
        }
    }
}
