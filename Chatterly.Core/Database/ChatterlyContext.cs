﻿using Microsoft.EntityFrameworkCore;

namespace Chatterly.Core.Database
{
    /// <summary>
    /// Base database context.
    /// </summary>
    public abstract class ChatterlyContext : DbContext
    {
        /// <summary>
        /// Log table.
        /// </summary>
        public DbSet<Log> Logs { get; set; }

        /// <summary>
        /// Allow for custom behavior when performing the database's existential check and when creating the database.
        /// </summary>
        public virtual void EnsureCreated()
        {
            Database.EnsureCreated();
        }
    }
}
