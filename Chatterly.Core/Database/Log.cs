﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Chatterly.Core.Database
{
    /// <summary>
    /// Type of log entries.
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// General log entry.
        /// </summary>
        Info,

        /// <summary>
        /// Some error in logic or behavior.
        /// </summary>
        Error,

        /// <summary>
        /// A program exception.
        /// </summary>
        Exception,

        /// <summary>
        /// Some warning in logic or behavior.
        /// </summary>
        Warning
    }

    /// <summary>
    /// Represents a database log entry.
    /// </summary>
    public class Log
    {
        /// <summary>
        /// ID of the log entry.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Type of the log entry.
        /// </summary>
        [Required]
        public LogType Type { get; set; }

        /// <summary>
        /// Date and time of the log entry.
        /// </summary>
        [Required]
        public DateTime Time { get; set; }

        /// <summary>
        /// Contents of the log entry.
        /// </summary>
        [Required]
        public string Message { get; set; }
    }
}
