﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to message a chat room.
    /// </summary>
    public sealed class MessageChatRoom : BaseCommand
    {
        /// <summary>
        /// ID of the chat room we want to message.
        /// </summary>
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Message that we want to send.
        /// </summary>
        public Message Message { get; set; }

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="chatRoomId">ID of the chat room we want to message.</param>
        /// <param name="userId">Our user ID.</param>
        /// <param name="message">Message we want to send.</param>
        /// <returns>Command.</returns>
        public static MessageChatRoom CreateCommand(int chatRoomId, int userId, string message)
        {
            return new MessageChatRoom
            {
                ChatRoomId = chatRoomId,
                Message = new Message
                {
                    Content = message,
                    Time = DateTime.UtcNow,
                    UserId = userId
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { Message };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
            }

            Message = msg.Messages[0] as Message;

        }
    }
}
