﻿using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    public enum BanAction
    {
        Ban,
        Unban
    }

    public class ModifyBan : BaseCommand
    {
        public string Nickname { get; set; }

        public int ChatRoomId { get; set; }

        public BanAction Action { get; set; }

        public static ICommand BanUser(int chatRoomId, string nickname)
        {
            return new ModifyBan
            {
                Nickname = nickname,
                ChatRoomId = chatRoomId,
                Action = BanAction.Ban
            };
        }

        public static ICommand UnbanUser(int chatRoomId, string nickname)
        {
            return new ModifyBan
            {
                Nickname = nickname,
                ChatRoomId = chatRoomId,
                Action = BanAction.Unban
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Nickname);
                writer.Write(ChatRoomId);
                writer.Write(Action.ToString());
                return writer.ToBytes();
            }
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                Nickname = reader.ReadString();
                ChatRoomId = reader.ReadInt32();
                var actionName = reader.ReadString();

                BanAction temp;
                if (Enum.TryParse(actionName, out temp))
                {
                    Action = temp;
                }
                else
                {
                    throw new Exception("ModifyBan ParsePayload did not receive a valid BanAction value.");
                }
            }
        }
    }
}
