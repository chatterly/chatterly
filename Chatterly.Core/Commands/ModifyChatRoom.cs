﻿using Chatterly.Core.Commands;
using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatterly.Core.Commands
{
    public class ModifyChatRoom : BaseCommand
    {
        public ChatRoom ChatRoom { get; set; }

        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { ChatRoom };
        }

        public override void ParsePayload(Payload msg)
        {
            ChatRoom = msg.Messages[0] as ChatRoom;
        }
    }
}
