﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to register a user.
    /// </summary>
    public sealed class RegisterUser : BaseCommand
    {
        /// <summary>
        /// Nickname we want to register.
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// Password with which we want to authenticate.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Flag to report back if the user was registered.
        /// </summary>
        public bool IsRegistered { get; set; }

        /// <summary>
        /// Error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="nickname">Nickname to register.</param>
        /// <param name="password">Password for the user.</param>
        /// <param name="encryptPass">Automatically encrypt password. Defaults to true.</param>
        /// <returns>Command.</returns>
        public static RegisterUser CreateCommand(string nickname, string password, bool encryptPass = true)
        {
            return new RegisterUser
            {
                NickName = nickname,
                Password = encryptPass ? User.EncryptPassword(password) : password
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(NickName);
                writer.Write(Password);
                writer.Write(IsRegistered);
                writer.Write(ErrorMessage ?? string.Empty);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                NickName = reader.ReadString();
                Password = reader.ReadString();
                IsRegistered = reader.ReadBoolean();
                ErrorMessage = reader.ReadString();
            }
        }
    }
}
