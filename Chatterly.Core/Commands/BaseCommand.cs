﻿using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Adds convince to creating <see cref="ICommand"/> implementations.
    /// </summary>
    public abstract class BaseCommand : ICommand
    {
        /// <summary>
        /// Unique ID of this <see cref="ICommand"/> object instance.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public virtual byte[] CreatePrimitiveData()
        {
            return new byte[0];
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public virtual IMessage[] CreatePayloadData()
        {
            return new IMessage[0];
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public abstract void ParsePayload(Payload msg);
    }
}
