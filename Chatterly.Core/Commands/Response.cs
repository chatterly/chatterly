﻿using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Response to be used when no command needs to be sent back.
    /// This will trigger a callback, no events based on command type other than <see cref="Response"/>.
    /// </summary>
    public class Response : BaseCommand
    {
        /// <summary>
        /// Response message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Message);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                Message = reader.ReadString();
            }
        }
    }
}
