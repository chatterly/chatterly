﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System.Collections.Generic;
using System.Linq;

namespace Chatterly.Core.Commands
{
    public class GetChatRoomStaff : BaseCommand
    {
        public int ChatRoomId { get; set; }

        public List<ChatRoomStaff> Staff { get; set; }

        public static ICommand CreateCommand(int chatRoomId)
        {
            return new GetChatRoomStaff
            {
                ChatRoomId = chatRoomId
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                return writer.ToBytes();
            }
        }

        public override IMessage[] CreatePayloadData()
        {
            return Staff?.ToArray() ?? new IMessage[0];
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
            }

            Staff = msg.Messages.Cast<ChatRoomStaff>().ToList();
        }
    }
}
