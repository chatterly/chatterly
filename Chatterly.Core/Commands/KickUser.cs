﻿using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    public class KickUser : BaseCommand
    {
        public int ChatRoomId { get; set; }
        public string Nickname { get; set; } = string.Empty;

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                writer.Write(Nickname);
                return writer.ToBytes();
            }
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
                Nickname = reader.ReadString();
            }
        }
    }
}
