﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Fetch a staff position of a user for a chatroom.
    /// </summary>
    public class IsChatRoomStaff : BaseCommand
    {
        /// <summary>
        /// ID of the chatroom of interest.
        /// </summary>
        public int ChatRoomId { get; set; }

        /// <summary>
        /// User of interest.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Rank of the user in that chatroom. If no staff position, will be NULL.
        /// </summary>
        public ChatRoomRank? Rank { get; set; }

        public static ICommand CreateCommand(int chatRoomId, string nickname)
        {
            return new IsChatRoomStaff
            {
                ChatRoomId = chatRoomId,
                User = new User
                {
                    NickName = nickname
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                writer.Write(Rank == null ? string.Empty : Rank.ToString());
                return writer.ToBytes();
            }
        }

        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { User };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
                var rank = reader.ReadString();

                Rank = null;

                if (!string.IsNullOrWhiteSpace(rank))
                {
                    ChatRoomRank parseRank;
                    if (Enum.TryParse(rank, out parseRank))
                    {
                        Rank = parseRank;
                    }
                }
            }

            User = msg.Messages[0] as User;
        }
    }
}
