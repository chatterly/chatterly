﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chatterly.Core.Protocol;
using Chatterly.Core.Models;

namespace Chatterly.Core.Commands
{
    public class GetChatRoomMembers : BaseCommand
    {
        public int ChatRoomId { get; set; }

        public List<User> Members { get; set; }

        public static ICommand CreateCommand(int chatRoomId)
        {
            return new GetChatRoomMembers
            {
                ChatRoomId = chatRoomId
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                return writer.ToBytes();
            }
        }

        public override IMessage[] CreatePayloadData()
        {
            return Members?.ToArray() ?? new IMessage[0];
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
            }

            Members = msg.Messages.Cast<User>().ToList();
        }
    }
}
