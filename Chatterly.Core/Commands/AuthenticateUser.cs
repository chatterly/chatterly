﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to facilitate user authentication.
    /// </summary>
    public sealed class AuthenticateUser : BaseCommand
    {
        /// <summary>
        /// Flag used to determine if the user was authenticated.
        /// </summary>
        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// User object.
        /// From client to server, used to relay authentication information.
        /// From server to client, if authenticated, contains the information of the authenticated user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Creates an authentication command object.
        /// </summary>
        /// <param name="nickname">Nickname with which to attempt authentication.</param>
        /// <param name="password">Password with which to attempt authentication.</param>
        /// <param name="encryptPass">
        /// Flag to determine if the password should be automatically encrypted. Defaults to true.
        /// </param>
        /// <returns>Authentication command object.</returns>
        public static AuthenticateUser CreateCommand(string nickname, string password, bool encryptPass = true)
        {
            return new AuthenticateUser
            {
                User = new User
                {
                    NickName = nickname,
                    EncryptedPassword = encryptPass ? User.EncryptPassword(password) : password
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(IsAuthenticated);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { User };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                IsAuthenticated = reader.ReadBoolean();
            }

            User = msg.Messages[0] as User;
        }
    }
}
