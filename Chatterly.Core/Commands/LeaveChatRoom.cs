﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to leave a chat room.
    /// </summary>
    public sealed class LeaveChatRoom : BaseCommand
    {
        /// <summary>
        /// Flag to report back whether the user left the chat room.
        /// </summary>
        public bool Left { get; set; }

        /// <summary>
        /// Chat room object to make the request and to report back chat room data.
        /// </summary>
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="chatRoomId">ID of the chat room we want to leave.</param>
        /// <returns>Command.</returns>
        public static LeaveChatRoom CreateCommand(int chatRoomId)
        {
            return new LeaveChatRoom
            {
                ChatRoom = new ChatRoom
                {
                    Id = chatRoomId
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Left);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { ChatRoom };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                Left = reader.ReadBoolean();
            }

            ChatRoom = msg.Messages[0] as ChatRoom;
        }
    }
}
