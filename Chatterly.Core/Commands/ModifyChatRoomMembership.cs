﻿using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    public enum ChatRoomMembershipAction
    {
        Add,
        Remove
    }

    public class ModifyChatRoomMembership : BaseCommand
    {
        public int ChatRoomId { get; set; }

        public string Nickname { get; set; }

        public ChatRoomMembershipAction Action { get; set; }

        public static ICommand AddMember(int chatRoomId, string nickname)
        {
            return new ModifyChatRoomMembership
            {
                Nickname = nickname,
                ChatRoomId = chatRoomId,
                Action = ChatRoomMembershipAction.Add
            };
        }

        public static ICommand RemoveMember(int chatRoomId, string nickname)
        {
            return new ModifyChatRoomMembership
            {
                Nickname = nickname,
                ChatRoomId = chatRoomId,
                Action = ChatRoomMembershipAction.Remove
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Nickname);
                writer.Write(ChatRoomId);
                writer.Write(Action.ToString());
                return writer.ToBytes();
            }
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                Nickname = reader.ReadString();
                ChatRoomId = reader.ReadInt32();
                var actionName = reader.ReadString();

                ChatRoomMembershipAction temp;
                if (Enum.TryParse(actionName, out temp))
                {
                    Action = temp;
                }
                else
                {
                    throw new Exception(
                        "ModifyChatRoomMembership ParsePayload did not receive a valid ChatRoomMembershipAction value.");
                }
            }
        }
    }
}
