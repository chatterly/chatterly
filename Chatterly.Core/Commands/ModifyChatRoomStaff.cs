﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Used to modify a chatroom staff position for a given chatroom and user.
    /// If the user does not have a position with the chatroom, the position will be created (if allowed).
    /// If the user does have a position with the chatroom, the position will be modified (if allowed).
    /// If the Rank is null and the user does have a position with the chatroom, the position will be removed (if allowed).
    /// </summary>
    public class ModifyChatRoomStaff : BaseCommand
    {
        /// <summary>
        /// ID of the chatroom of interest.
        /// </summary>
        public int ChatRoomId { get; set; }

        /// <summary>
        /// Nickname of the user of interest.
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// Rank to which the user should be set.
        /// </summary>
        public ChatRoomRank? Rank { get; set; }

        public static ICommand CreateCommand(int chatRoomId, string nickname, ChatRoomRank? rank)
        {
            return new ModifyChatRoomStaff
            {
                ChatRoomId = chatRoomId,
                Nickname = nickname,
                Rank = rank
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                writer.Write(Nickname);
                writer.Write(Rank.HasValue ? Rank.Value.ToString() : string.Empty);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
                Nickname = reader.ReadString();

                var rank = reader.ReadString();

                Rank = null;

                if (!string.IsNullOrWhiteSpace(rank))
                {
                    ChatRoomRank parseRank;
                    if (Enum.TryParse(rank, out parseRank))
                    {
                        Rank = parseRank;
                    }
                }
            }
        }
    }
}
