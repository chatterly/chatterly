﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System.Collections.Generic;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to get data for a specific chat room.
    /// </summary>
    public sealed class GetChatRoom : BaseCommand
    {
        /// <summary>
        /// Chat room object used to make the request and receive the data.
        /// </summary>
        public ChatRoom ChatRoom { get; set; }

        /// <summary>
        /// Collection of users current subscribed to the chat room.
        /// </summary>
        public ICollection<User> Users { get; set; } = new User[0];

        /// <summary>
        /// Collection of users who are current members of the chatroom.
        /// </summary>
        public ICollection<User> Members { get; set; } = new User[0];

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="chatRoomId">ID of the chat room in which we're interested.</param>
        /// <returns>Command.</returns>
        public static GetChatRoom CreateCommand(int chatRoomId)
        {
            return new GetChatRoom
            {
                ChatRoom = new ChatRoom
                {
                    Id = chatRoomId
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Users.Count);

                foreach (var user in Users)
                {
                    var data = user.Serialize();
                    writer.Write(data.Length);
                    writer.Write(data);
                }

                writer.Write(Members.Count);

                foreach (var user in Members)
                {
                    var data = user.Serialize();
                    writer.Write(data.Length);
                    writer.Write(data);
                }

                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return new[] { ChatRoom };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                var userCount = reader.ReadInt32();

                var users = new List<User>();

                for (int i = 0; i < userCount; ++i)
                {
                    var size = reader.ReadInt32();
                    var data = reader.ReadBytes(size);
                    users.Add(new User(data));
                }

                Users = users;

                var memberCount = reader.ReadInt32();

                var members = new List<User>();

                for (int i = 0; i < memberCount; ++i)
                {
                    var size = reader.ReadInt32();
                    var data = reader.ReadBytes(size);
                    members.Add(new User(data));
                }

                Members = members;
            }

            ChatRoom = msg.Messages[0] as ChatRoom;
        }
    }
}
