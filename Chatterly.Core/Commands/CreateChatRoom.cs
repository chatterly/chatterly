﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    public class CreateChatRoom : BaseCommand
    {
        public ChatRoom ChatRoom { get; set; } = new ChatRoom();

        public bool IsCreated { get; set; } = false;

        public string Message { get; set; } = string.Empty;

        public static ICommand CreateCommand(string name, string description, bool isPrivate = false)
        {
            return new CreateChatRoom
            {
                ChatRoom = new ChatRoom
                {
                    Name = name,
                    Description = description,
                    IsPrivate = isPrivate
                }
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(IsCreated);
                writer.Write(Message);
                return writer.ToBytes();
            }
        }

        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { ChatRoom };
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                IsCreated = reader.ReadBoolean();
                Message = reader.ReadString();
            }

            ChatRoom = msg.Messages[0] as ChatRoom;
        }
    }
}
