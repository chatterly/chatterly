﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chatterly.Core.Commands
{
    public class GetChatRoomBans : BaseCommand
    {
        public int ChatRoomId { get; set; }

        public List<ChatRoomBan> Bans { get; set; }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(ChatRoomId);
                return writer.ToBytes();
            }
        }

        public static ICommand CreateCommand(int chatRoomId)
        {
            return new GetChatRoomBans
            {
                ChatRoomId = chatRoomId
            };
        }

        public override IMessage[] CreatePayloadData()
        {
            return Bans?.ToArray() ?? new IMessage[0];
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                ChatRoomId = reader.ReadInt32();
            }

            Bans = msg.Messages.Cast<ChatRoomBan>().ToList();
        }
    }
}
