﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to message a user.
    /// </summary>
    public sealed class MessageUser : BaseCommand
    {
        /// <summary>
        /// ID of the user we want to message.
        /// </summary>
        public int RecipientUserId { get; set; }

        /// <summary>
        /// Message that we want to send.
        /// </summary>
        public Message Message { get; set; }

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="recipientUserId">ID of the user we want to message.</param>
        /// <param name="message">Message we want to send.</param>
        /// <returns>Command.</returns>
        public static MessageUser CreateCommand(int recipientUserId, string message)
        {
            return new MessageUser
            {
                RecipientUserId = recipientUserId,
                Message = new Message
                {
                    Content = message,
                    Time = DateTime.UtcNow,
                }
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(RecipientUserId);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return new IMessage[] { Message };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                RecipientUserId = reader.ReadInt32();
            }

            Message = msg.Messages[0] as Message;

        }
    }
}