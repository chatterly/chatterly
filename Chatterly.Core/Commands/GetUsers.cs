﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chatterly.Core.Protocol;
using System.Collections.ObjectModel;
using Chatterly.Core.Models;

namespace Chatterly.Core.Commands
{
    public class GetUsers : BaseCommand
    {
        public string StartsWith { get; set; }

        public List<User> Users { get; set; }

        public static ICommand CreateCommand(string startsWith = null)
        {
            return new GetUsers
            {
                StartsWith = startsWith
            };
        }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(string.IsNullOrWhiteSpace(StartsWith) ? string.Empty : StartsWith);
                return writer.ToBytes();
            }
        }

        public override IMessage[] CreatePayloadData()
        {
            return Users?.ToArray() ?? new IMessage[0];
        }

        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                StartsWith = reader.ReadString();
            }

            Users = msg.Messages.Cast<User>().ToList();
        }
    }
}
