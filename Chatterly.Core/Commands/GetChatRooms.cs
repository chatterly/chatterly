﻿using Chatterly.Core.Models;
using Chatterly.Core.Protocol;
using System.Collections.Generic;
using System.Linq;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to get all of the existing chat rooms and some related data.
    /// </summary>
    public sealed class GetChatRooms : BaseCommand
    {
        /// <summary>
        /// Class to describe the results.
        /// </summary>
        public class ChatRoomMeta : IMessage
        {
            /// <summary>
            /// Chat room data.
            /// </summary>
            public ChatRoom ChatRoom { get; set; }

            /// <summary>
            /// Number of subscribed users.
            /// </summary>
            public int UserCount { get; set; }

            /// <summary>
            /// Serialize the metadata.
            /// </summary>
            /// <returns>Serialized data.</returns>
            byte[] IMessage.Serialize()
            {
                using (var writer = BinaryHelper.GetWriter())
                {
                    var room = ChatRoom.Serialize();
                    writer.Write(room.Length);
                    writer.Write(room);
                    writer.Write(UserCount);
                    return writer.ToBytes();
                }
            }

            /// <summary>
            /// Reconstruct this object from serialized data.
            /// </summary>
            /// <param name="bytes">Serialized data.</param>
            void IMessage.Deserialize(byte[] bytes)
            {
                using (var reader = bytes.GetReader())
                {
                    var length = reader.ReadInt32();

                    ChatRoom = new ChatRoom();

                    if (length > 0)
                    {
                        ChatRoom.Deserialize(reader.ReadBytes(length));
                    }

                    UserCount = reader.ReadInt32();
                }
            }
        }

        /// <summary>
        /// Existing chat rooms.
        /// </summary>
        public IEnumerable<ChatRoomMeta> ChatRooms { get; set; }

        /// <summary>
        /// Create a collection of <see cref="IMessage"/> objects to serialize.
        /// </summary>
        /// <returns><see cref="IMessage"/> objects to serialize.</returns>
        public override IMessage[] CreatePayloadData()
        {
            return ChatRooms?.ToArray() ?? new IMessage[] { };
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            ChatRooms = msg.Messages.Cast<ChatRoomMeta>().ToList();
        }
    }
}
