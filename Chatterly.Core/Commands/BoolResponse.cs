﻿using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Response to relay some binary result and associated message.
    /// </summary>
    public class BoolResponse : BaseCommand
    {
        /// <summary>
        /// Result.
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// Message.
        /// </summary>
        public string Message { get; set; }

        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(Result);
                writer.Write(Message);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                Result = reader.ReadBoolean();
                Message = reader.ReadString();
            }
        }
    }
}
