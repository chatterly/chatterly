﻿using Chatterly.Core.Protocol;

namespace Chatterly.Core.Commands
{
    /// <summary>
    /// Command to determine if a nickname is registered.
    /// </summary>
    public class DoesNickNameExist : BaseCommand
    {
        /// <summary>
        /// Nickname to check.
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// Flag to communicate whether the nickname is registered.
        /// </summary>
        public bool NickNameExists { get; set; }

        /// <summary>
        /// Create the command.
        /// </summary>
        /// <param name="nickname">Nickname to check.</param>
        /// <returns>Command.</returns>
        public static ICommand CreateCommand(string nickname)
        {
            return new DoesNickNameExist
            {
                NickName = nickname
            };
        }

        /// <summary>
        /// Convert the attached data to a byte array.
        /// </summary>
        /// <returns>Data buffer.</returns>
        public override byte[] CreatePrimitiveData()
        {
            using (var writer = BinaryHelper.GetWriter())
            {
                writer.Write(NickName);
                writer.Write(NickNameExists);
                return writer.ToBytes();
            }
        }

        /// <summary>
        /// Rebuild an object using payload data.
        /// </summary>
        /// <param name="msg">Payload object with which this object will be rebuilt.</param>
        public override void ParsePayload(Payload msg)
        {
            using (var reader = msg.PrimitiveData.GetReader())
            {
                NickName = reader.ReadString();
                NickNameExists = reader.ReadBoolean();
            }
        }
    }
}
