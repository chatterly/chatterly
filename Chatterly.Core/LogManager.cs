﻿using Chatterly.Core.Database;
using System;
using System.Text;

namespace Chatterly.Core
{
    /// <summary>
    /// Object to handle logging.
    /// </summary>
    public class LogManager
    {
        /// <summary>
        /// Lock to reduce multi-threaded errors.
        /// </summary>
        protected static readonly object _Lock = new object();

        /// <summary>
        /// Reference to the database connection.
        /// </summary>
        protected ChatterlyContext _Db;

        /// <summary>
        /// Construct a log manager.
        /// </summary>
        /// <param name="db">Database context.</param>
        public LogManager(ChatterlyContext db)
        {
            _Db = db;
        }

        /// <summary>
        /// Add a log entry to the database.
        /// </summary>
        /// <param name="type">Type of this log entry.</param>
        /// <param name="message">Message to log.</param>
        public void Add(LogType type, string message)
        {
            lock (_Lock)
            {
                var log = new Log
                {
                    Type = type,
                    Time = DateTime.UtcNow,
                    Message = message
                };

                _Db.Logs.Add(log);
                _Db.SaveChanges();
            }
        }

        /// <summary>
        /// Log an exception. Unravels all layers of inner exceptions and builds a single log entry.
        /// </summary>
        /// <param name="message">Message to describe the situation.</param>
        /// <param name="e">Top-level exception.</param>
        public void Add(string message, Exception e)
        {
            if (e == null)
            {
                throw new ArgumentNullException("e", "LogManager.Add expects a valid Exception object.");
            }

            lock (_Lock)
            {
                var error = new StringBuilder(message);

                while (e != null)
                {
                    error.Append(" - (")
                        .Append(e.GetType().Name)
                        .Append(") ")
                        .Append(e.Message);

                    e = e.InnerException;
                }

                Add(LogType.Exception, error.ToString());
            }
        }
    }
}
