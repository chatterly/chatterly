﻿namespace Chatterly.Core.Crypto
{
    /// <summary>
    /// Interface to describe an algorithm to encrypt and decrypt data.
    /// </summary>
    public interface IEncryptor
    {
        /// <summary>
        /// Encrypt the given data.
        /// </summary>
        /// <param name="data">Data to encrypt.</param>
        /// <returns>Encrypted data.</returns>
        byte[] Encrypt(byte[] data);

        /// <summary>
        /// Decrypt the given data.
        /// </summary>
        /// <param name="data">Data to decrypt.</param>
        /// <returns>Decrypted data.</returns>
        byte[] Decrypt(byte[] data);
    }
}
