﻿using System.Security.Cryptography;

namespace Chatterly.Core.Crypto
{
    /// <summary>
    /// RSA cryptography.
    /// </summary>
    public class RsaCrypto : IEncryptor
    {
        /// <summary>
        /// Crypto provider.
        /// </summary>
        readonly RSACryptoServiceProvider _Rsa;

        /// <summary>
        /// Construct with the given key data.
        /// </summary>
        /// <param name="keyXml">XML key data.</param>
        public RsaCrypto(string keyXml = null)
        {
            _Rsa = new RSACryptoServiceProvider();

            if (!string.IsNullOrWhiteSpace(keyXml))
            {
                SetKeyXml(keyXml);
            }
        }

        /// <summary>
        /// Set the key data.
        /// </summary>
        /// <param name="keyXml">XML key data.</param>
        public void SetKeyXml(string keyXml)
        {
            _Rsa.FromXmlString(keyXml);
        }

        /// <summary>
        /// Encrypt the given data.
        /// </summary>
        /// <param name="data">Data to encrypt.</param>
        /// <returns>Encrypted data.</returns>
        public byte[] Encrypt(byte[] data)
        {
            return _Rsa.Encrypt(data, true);
        }

        /// <summary>
        /// Decrypt the given data.
        /// </summary>
        /// <param name="data">Data to decrypt.</param>
        /// <returns>Decrypted data.</returns>
        public byte[] Decrypt(byte[] data)
        {
            return _Rsa.Decrypt(data, true);
        }
    }
}
