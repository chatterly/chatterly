﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Chatterly.Core
{
    /// <summary>
    /// Registry of services that are used throughout the application.
    /// </summary>
    public class Services
    {
        /// <summary>
        /// Mapping of type to instance.
        /// </summary>
        readonly static IDictionary<Type, object> _Services = new ConcurrentDictionary<Type, object>();

        /// <summary>
        /// Register a service.
        /// </summary>
        /// <typeparam name="T">Type of the service being registered.</typeparam>
        /// <param name="service">Object instance being registered.</param>
        public static void RegisterService<T>(T service)
            where T : class
        {
            _Services[service.GetType()] = service;
        }

        /// <summary>
        /// Get the registered service.
        /// </summary>
        /// <typeparam name="T">Type of service in which we're interested.</typeparam>
        /// <returns>Object instance of the desired type.</returns>
        public static T GetService<T>()
            where T : class
        {
            return _Services[typeof(T)] as T;
        }
    }
}
