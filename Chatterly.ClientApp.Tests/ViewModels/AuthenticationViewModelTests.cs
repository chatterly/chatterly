﻿using Chatterly.Client;
using Chatterly.Client.Database;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Chatterly.Server.CommandHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using Chatterly.Core.Models;
using System.Text;
using System.Net;


namespace Chatterly.ClientApp.ViewModels.Tests
{
    [TestClass]
    public class AuthenticationViewModelTests
    {
        static Process ServerProcess;
        static AuthenticationViewModel Model;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            //Start Server
            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");
            ServerProcess = Process.Start(serverPath);

            //Navigate to a state where AuthenticationViewModel can be tested
            MainViewModel Main = new MainViewModel();
            ServerSelectionViewModel ServerSelect = (ServerSelectionViewModel)Main.ActiveViewModel;
            ServerSelect.SelectedConnection = ServerSelect.Connections[0];
            ServerSelect.Connect.Execute(null);

            //Server selected, now set authentication model
            Model = (AuthenticationViewModel)Main.ActiveViewModel;
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            try
            {
                ServerProcess.Kill();
                ServerProcess.Close();
                ServerProcess.Dispose();
            }
            catch { }
        }

        bool LoginError = false;
        [TestMethod]
        public void AuthenticationViewModelTestProperties()
        {
            //Nickname should be empty
            Assert.IsNull(Model.Nickname);

            //Set the nickname property; was it was set properly?
            string TestNick = "TestNick";
            Model.Nickname = TestNick;
            Assert.AreEqual(TestNick, Model.Nickname, "Nickname property must be the same.");

            //Password should be empty
            Assert.IsNull(Model.Password);

            //Set the password property; was it was set properly?
            string TestPass = "TestPass";
            Model.Password = TestPass;
            Assert.AreEqual(TestPass, Model.Password, "Password property must be the same.");

            //Set the nickname to null; commands should not be execuatable
            Model.Nickname = null;
            Assert.IsFalse(Model.Login.CanExecute(null));
            Assert.IsFalse(Model.Register.CanExecute(null));

            //Set the password to null; commands should not be execuatable
            Model.Nickname = TestNick;
            Model.Password = null;
            Assert.IsFalse(Model.Login.CanExecute(null));
            Assert.IsFalse(Model.Register.CanExecute(null));
            Assert.IsFalse(Model.LoginOrRegister.CanExecute(null));

            //The password and nickname properties are filled, should be able to execute commands
            Model.Password = TestPass;
            Assert.IsTrue(Model.Login.CanExecute(null));
            Assert.IsTrue(Model.Register.CanExecute(null));
            Assert.IsTrue(Model.LoginOrRegister.CanExecute(null));
        }

        [TestMethod Timeout(10000)]
        public void AuthenticationViewModelTestLoginFail()
        {
            LoginError = false;
            Assert.IsFalse(LoginError);

            //Login should fail with incorrect credentials
            Model.ShowErrorEvent += AuthModel_EventFired;
            Model.Nickname = "IncorrectUser";
            Model.Password = "IncorrectPassword";
            //Password must be in a certain format (encrypted)
            Model.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes(Model.Password)));

            Model.Login.Execute(null);

            //If event executes, there should be a login error (timeout occurs otherwise)
            while (!LoginError) { }
            Assert.IsTrue(LoginError);
        }

        [TestMethod]
        public void AuthenticationViewModelTestRegister()
        {
            LoginError = false;
            Assert.IsFalse(LoginError);
            Model.ShowErrorEvent += AuthModel_EventFired;

            Model.Password = "RandomPassword";

            //Password must be in a certain format (encrypted)
            Model.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes(Model.Password)));

            //Ensure using a unique username that isn't in the database
            for (int i = 0; ; ++i)
            {
                Model.Nickname = string.Format("User{0}", i);

                Model.Register.Execute(null);

                int MaxSecondWait = 5;
                while (!LoginError && MaxSecondWait > 0)
                {
                    if (Model.MainViewModel.ActiveViewModel is ChatRoomSelectionViewModel)
                    {
                        break;
                    }
                    System.Threading.Thread.Sleep(1000);
                    MaxSecondWait--;
                }

                if (LoginError)
                {
                    LoginError = false;
                    continue;
                }
                else if (MaxSecondWait == 0)
                {
                    Assert.Fail("Register Command Timeout");
                }
                break;
            }
        }

        //Method to attach to Login/Register error event
        private void AuthModel_EventFired(object sender, EventArgs e)
        {
            LoginError = !LoginError;
        }
    }
}