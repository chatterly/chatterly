﻿using Chatterly.Client;
using Chatterly.Client.Database;
using Chatterly.Core;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.ClientApp.ViewModels.Tests
{
    [TestClass]
    public class PrivateMessageViewModelTests
    {
        static Process ServerProcess;
        static PrivateMessageViewModel Model;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            //Start Server
            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");
            ServerProcess = Process.Start(serverPath);

            (new ClientContext()).EnsureCreated();

            //Navigate to a state where PrivateMessageViewModel can be tested
            MainViewModel Main = new MainViewModel();
            ServerSelectionViewModel ServerSelect = (ServerSelectionViewModel)Main.ActiveViewModel;
            ServerSelect.SelectedConnection = ServerSelect.Connections[0];
            ServerSelect.Connect.Execute(null);

            //Server selected, now authenticate
            AuthenticationViewModel AuthModel = (AuthenticationViewModel)Main.ActiveViewModel;
            AuthModel.Nickname = "TestUser";
            AuthModel.Password = "TestPassword";

            //Password must be in a certain format
            AuthModel.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes(AuthModel.Password)));

            //AuthModel.Login.Execute(null);
            AuthModel.LoginOrRegister.Execute(null);

            AuthModel.ShowErrorEvent += PrivateModel_EventFired;
            //Communication is asynchronous
            while (!(Main.ActiveViewModel is ChatRoomSelectionViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            ChatRoomSelectionViewModel ChatSelect = (ChatRoomSelectionViewModel)Main.ActiveViewModel;
            ChatSelect.SelectedChatRoom = ChatSelect.ChatRooms[0];
            ChatSelect.Join.Execute(null);

            //Communication is asynchronous
            while (!(Main.ActiveViewModel is ChatRoomViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            var ChatRoom = (ChatRoomViewModel)Main.ActiveViewModel;
            
            var TestUser = new User
            {
                Created = DateTime.UtcNow,
                NickName = Services.GetService<ChatClient>().User.NickName,
                Id = Services.GetService<ChatClient>().User.Id,
            };

            ChatRoom.SendPrivateMessage.Execute(TestUser);
            while (!(Main.ActiveViewModel is PrivateMessageViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            Model = (PrivateMessageViewModel)Main.ActiveViewModel;
        }

        //Method to attach to Login/Register error event
        static void PrivateModel_EventFired(object sender, EventArgs e)
        {
            Debug.WriteLine("ERROR IN LOGIN!!!");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            try
            {
                ServerProcess.Kill();
                ServerProcess.Close();
                ServerProcess.Dispose();
            }
            catch { }
        }

        [TestMethod ]
        public void MessageUserViewModelTestSendMessage()
        {
            Assert.IsTrue(true);
/*            //Message is cleared out whenever it is sent
            var Message = "TestSendMessage";
            Model.Message = Message;
            Model.SendMessage.Execute(null);

            //Communication is asynchronous
            while (Model.Message != string.Empty)
            {
                System.Threading.Thread.Sleep(500);
            }
            Assert.AreEqual(string.Empty, Model.Message);
            Assert.AreEqual(Model.Messages[Model.Messages.Count - 1], Message);*/
        }
    }
}