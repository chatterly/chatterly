﻿using Chatterly.Client.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;

namespace Chatterly.ClientApp.ViewModels.Tests
{
    [TestClass]
    public class ServerSelectionViewModelTests
    {
        static Process ServerProcess;
        static ServerSelectionViewModel Model;

        [ClassInitialize Timeout(20000)]
        public static void Initialize(TestContext context)
        {
            //Start Server
            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");
            ServerProcess = Process.Start(serverPath);

            (new ClientContext()).EnsureCreated();

            //Navigate to a state where ServerSelectionViewModel can be tested
            MainViewModel Main = new MainViewModel();

            Model = (ServerSelectionViewModel)Main.ActiveViewModel;
            //Upon becoming the ActiveViewModel, the Connections property is populated.
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            try
            {
                ServerProcess.Kill();
                ServerProcess.Close();
                ServerProcess.Dispose();
            }
            catch { }
        }

        [TestMethod]
        public void ServerSelectionViewModelTestProperties()
        {
            //Check that the SelectedConnection property can be get/set
            Model.SelectedConnection = null;
            Assert.IsNull(Model.SelectedConnection);
            Model.SelectedConnection = new Client.Database.ConnectionConfiguration();
            Assert.IsNotNull(Model.SelectedConnection);

            //Check that the Connections property can be accessed (private set)
            var NotNull = Model.Connections;
            Assert.IsNotNull(NotNull);
        }

        [TestMethod]
        public void ServerSelectionViewModelTestConnect()
        {
            //Test that a connection to the server can be opened
            Model.SelectedConnection = Model.Connections[0];
            Model.Connect.Execute(null);

            //Communication is asynchronous
            while (!(Model.MainViewModel.ActiveViewModel is AuthenticationViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }
        }
    }
}