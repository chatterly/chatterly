﻿using Chatterly.Client.Database;
using Chatterly.Core.Crypto;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Chatterly.ClientApp.ViewModels.Tests
{
    [TestClass]
    public class ChatRoomViewModelTests
    {
        static Process ServerProcess;
        static ChatRoomViewModel Model;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            //Start Server
            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");
            ServerProcess = Process.Start(serverPath);

            (new ClientContext()).EnsureCreated();

            //Navigate to a state where ChatRoomViewModel can be tested
            (new ClientContext()).EnsureCreated();
            MainViewModel Main = new MainViewModel();
            ServerSelectionViewModel ServerSelect = (ServerSelectionViewModel)Main.ActiveViewModel;
            ServerSelect.SelectedConnection = ServerSelect.Connections[0];
            ServerSelect.Connect.Execute(null);

            //Server selected, now authenticate
            AuthenticationViewModel AuthModel = (AuthenticationViewModel)Main.ActiveViewModel;
            AuthModel.Nickname = "TestUser";
            AuthModel.Password = "TestPassword";

            //Password must be in a certain format
            AuthModel.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes(AuthModel.Password)));

            //AuthModel.Login.Execute(null);
            AuthModel.LoginOrRegister.Execute(null);

            AuthModel.ShowErrorEvent += AuthModel_EventFired;
            //Communication is asynchronous
            while (!(Main.ActiveViewModel is ChatRoomSelectionViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            ChatRoomSelectionViewModel ChatSelect = (ChatRoomSelectionViewModel)Main.ActiveViewModel;
            ChatSelect.SelectedChatRoom = ChatSelect.ChatRooms[0];
            ChatSelect.Join.Execute(null);
            
            //Communication is asynchronous
            while (!(Main.ActiveViewModel is ChatRoomViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            Model = (ChatRoomViewModel)Main.ActiveViewModel;
        }

        //Method to attach to Login/Register error event
        static void AuthModel_EventFired(object sender, EventArgs e)
        {
            Debug.WriteLine("ERROR IN LOGIN!!!");
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            try
            {
                ServerProcess.Kill();
                ServerProcess.Close();
                ServerProcess.Dispose();
            }
            catch { }
        }

        [TestMethod]
        [Timeout(10000)]
        public void ChatRoomViewModelTestProperties()
        {
            //Check that ChatRoom property can be set/get
            var TempRoom = Model.ChatRoom;
            Model.ChatRoom = null;
            Assert.IsNull(Model.ChatRoom);
            Model.ChatRoom = TempRoom;
            Assert.IsNotNull(Model.ChatRoom);

            //Check can get Users
            Assert.IsNotNull(Model.Users);

            //Check can get Messages
            Assert.IsNotNull(Model.Messages);

            //Check Message property can be set/get
            var Message = "TestMessage";
            Model.Message = Message;
            Assert.AreEqual(Message, Model.Message);
        }

        [TestMethod Timeout(10000)]
        public void ChatRoomViewModelTestSendMessage()
        {
            //Message is cleared out whenever it is sent
            var Message = "TestSendMessage";
            Model.Message = Message;
            Model.SendMessage.Execute(null);

            //Communication is asynchronous
            while (Model.Message != string.Empty)
            {
                System.Threading.Thread.Sleep(500);
            }
            Assert.AreEqual(string.Empty, Model.Message);
        }
    }
}