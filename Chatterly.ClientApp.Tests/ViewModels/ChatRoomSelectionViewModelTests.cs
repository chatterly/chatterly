﻿using Chatterly.Client.Database;
using Chatterly.Core.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Chatterly.ClientApp.ViewModels.Tests
{
    [TestClass]
    public class ChatroomSelectionViewModelTests
    {
        static Process ServerProcess;
        static ChatRoomSelectionViewModel Model;

        [ClassInitialize Timeout(20000)]
        public static void Initialize(TestContext context)
        {
            //Start Server
            var serverPath = Path.Combine(context.TestDir, "..", "..", "Chatterly.ServerApp", "bin", "Debug",
                "Chatterly.ServerApp.exe");
            ServerProcess = Process.Start(serverPath);

            (new ClientContext()).EnsureCreated();

            //Navigate to a state where ChatRoomSelectionViewModel can be tested
            (new ClientContext()).EnsureCreated();
            MainViewModel Main = new MainViewModel();
            ServerSelectionViewModel ServerSelect = (ServerSelectionViewModel)Main.ActiveViewModel;
            ServerSelect.SelectedConnection = ServerSelect.Connections[0];
            ServerSelect.Connect.Execute(null);

            //Server selected, now authenticate
            AuthenticationViewModel AuthModel = (AuthenticationViewModel)Main.ActiveViewModel;
            AuthModel.Nickname = "TestUser";
            AuthModel.Password = "TestPassword";

            //Password must be in a certain format
            AuthModel.Password = Convert.ToBase64String(User.Encryptor.Encrypt(
                    Encoding.UTF8.GetBytes(AuthModel.Password)));

            AuthModel.LoginOrRegister.Execute(null);

            //Communication is asynchronous
            while (!(Main.ActiveViewModel is ChatRoomSelectionViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }

            //Get fully functional ChatRoomSelectionViewModel for testing
            Model = (ChatRoomSelectionViewModel)Main.ActiveViewModel;
        }

        [ClassCleanup]
        public static void CleanupTest()
        {
            try
            {
                ServerProcess.Kill();
                ServerProcess.Close();
                ServerProcess.Dispose();
            }
            catch { }
        }

        [TestMethod]
        public void ChatroomSelectionViewModelTestProperties()
        {
            //Check that the ChatRooms property can be get/set
            Model.ChatRooms = null;
            Assert.IsNull(Model.ChatRooms);
            Model.ChatRooms = new System.Collections.Generic.List<Core.Commands.GetChatRooms.ChatRoomMeta> {
            new Core.Commands.GetChatRooms.ChatRoomMeta()};
            Assert.IsNotNull(Model.ChatRooms);

            //Check that the SelectedChatRoom property can be get/set
            Assert.IsNull(Model.SelectedChatRoom);
            Model.SelectedChatRoom = new Core.Commands.GetChatRooms.ChatRoomMeta();
            Assert.IsNotNull(Model.SelectedChatRoom);
        }

        [TestMethod]
        public void ChatroomSelectionViewModelTestRefresh()
        {
            //Check that the Refresh command will populate the list of chatrooms from the server
            Model.ChatRooms = null;
            Assert.IsNull(Model.ChatRooms);
            Model.Refresh.Execute(null);

            //Communication is asynchronous
            while (Model.ChatRooms == null)
            {
                System.Threading.Thread.Sleep(500);
            }
            Assert.IsNotNull(Model.ChatRooms);
        }

        [TestMethod]
        [Timeout(10000)]
        public void ChatroomSelectionViewModelTestJoin()
        {
            //Check that the join command will switch the view to the selected Chat Room
            Assert.IsFalse(Model.MainViewModel.ActiveViewModel is ChatRoomViewModel);
            Model.SelectedChatRoom = Model.ChatRooms[0];
            Model.Join.Execute(null);

            //Communication is asynchronous
            while (!(Model.MainViewModel.ActiveViewModel is ChatRoomViewModel))
            {
                System.Threading.Thread.Sleep(500);
            }
            Assert.IsTrue(Model.MainViewModel.ActiveViewModel is ChatRoomViewModel);
        }
    }
}