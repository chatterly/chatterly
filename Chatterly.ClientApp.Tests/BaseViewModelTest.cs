﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace Chatterly.ClientApp.Tests
{
    [TestClass]
    public class BaseViewModelTest
    {
        readonly ManualResetEvent _WaitEvent = new ManualResetEvent(false);

        class FakeVm : BaseViewModel
        { }

        [TestMethod, Timeout(1000)]
        public void BaseViewModel_Properties()
        {
            var name = "Some name!";

            var vm = new FakeVm();
            vm.Name = name;

            Assert.AreNotEqual(Guid.Empty, vm.Id, "Expecting a constructed GUID.");
            Assert.AreEqual(name, vm.Name, "Expecting the set name.");

            vm.PropertyChanged += (sender, e) =>
            {
                Assert.AreEqual(nameof(vm.Name), e.PropertyName,
                    "Expected changed property event for Name.");
                _WaitEvent.Set();
            };

            name = "Changed!";
            vm.Name = name;

            _WaitEvent.WaitOne();

            Assert.AreEqual(name, vm.Name, "Expecting the set name.");
        }
    }
}
