﻿using Chatterly.Client;
using Chatterly.Client.CommandHandlers;
using Chatterly.Client.Database;
using Chatterly.ClientApp.ViewModels;
using Chatterly.Core;
using Chatterly.Core.Commands;
using Chatterly.Core.Protocol;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading;

namespace Chatterly.ClientApp.Tests
{
    [TestClass]
    public class MainViewModelTest
    {
        readonly ManualResetEvent _WaitEvent = new ManualResetEvent(false);

        class FakeVm1 : BaseViewModel
        { }

        class FakeVm2 : BaseViewModel
        { }

        static MainViewModel MainViewModel { get; set; }

        [ClassInitialize]
        public static void InitializeTestClass(TestContext context)
        {
            (new ClientContext()).EnsureCreated();
            MainViewModel = new MainViewModel();
        }

        [TestMethod]
        public void MainViewModel_ServicesInitialized()
        {
            var cmdr = Services.GetService<CommandHandlerRegistry>();
            var chatClient = Services.GetService<ChatClient>();
            var serverResponse = Services.GetService<ServerResponseNotifier>();
            var logManager = Services.GetService<LogManager>();
            var chatRoomManager = Services.GetService<ChatRoomManager>();

            Assert.IsNotNull(cmdr, "CommandHandlerRegistry must be registered as a service.");
            Assert.IsNotNull(chatClient, "ChatClient must be registered as a service.");
            Assert.IsNotNull(serverResponse, "ServerResponseNotifier must be registered as a service.");
            Assert.IsNotNull(logManager, "LogManager must be registered as a service.");
            Assert.IsNotNull(chatRoomManager, "ChatRoomManager must be registered as a service.");
        }

        [TestMethod]
        public void MainViewModel_CommandHandlersRegistered()
        {
            var cmdr = Services.GetService<CommandHandlerRegistry>();


            var regUser = cmdr.GetHandlers<RegisterUser>();
            var authUser = cmdr.GetHandlers<AuthenticateUser>();
            var getChatRooms = cmdr.GetHandlers<GetChatRooms>();
            var messageChatRoom = cmdr.GetHandlers<MessageChatRoom>();

            Assert.AreEqual(1, regUser.Count(), "Only expecting one command handler for RegisterUser.");
            Assert.AreEqual(1, authUser.Count(), "Only expecting one command handler for AuthenticateUser.");
            Assert.AreEqual(1, getChatRooms.Count(), "Only expecting one command handler for GetChatRooms.");
            Assert.AreEqual(1, messageChatRoom.Count(), "Only expecting one command handler for MessageChatRoom.");

            Assert.IsInstanceOfType(regUser.First(), typeof(RegisterUserHandler),
                "Expected RegisterUserHandler for RegisterUser.");
            Assert.IsInstanceOfType(authUser.First(), typeof(AuthenticateUserHandler),
                "Expected AuthenticateUserHandler for AuthenticateUser.");
            Assert.IsInstanceOfType(getChatRooms.First(), typeof(GetChatRoomsHandler),
                "Expected GetChatRoomsHandler for GetChatRooms.");
            Assert.IsInstanceOfType(messageChatRoom.First(), typeof(MessageChatRoomHandler),
                "Expected MessageChatRoomHandler for MessageChatRoom.");
        }

        [TestMethod]
        public void MainViewModel_ViewModelOptions()
        {
            var viewModels = MainViewModel.ViewModelOptions.ToList();

            Assert.IsInstanceOfType(viewModels[0], typeof(ServerSelectionViewModel),
                "First view model was expected to be ServerSelectionViewModel.");
        }

        [TestMethod]
        public void MainViewModel_AddRemoveActiveViewModels()
        {
            var fake1 = new FakeVm1();
            var fake2 = new FakeVm2();

            MainViewModel.AddActiveViewModel(fake1, true);

            var fake1Contained = MainViewModel.ActiveViewModels.Contains(fake1);
            var fake1Found = MainViewModel.ActiveViewModels.FirstOrDefault(vm => vm.Id == fake1.Id);

            Assert.IsTrue(fake1Contained, "View Model is expected to be in active collection.");
            Assert.AreSame(fake1, fake1Found, "First View Model object expected to be in active collection.");
            Assert.AreSame(fake1, MainViewModel.ActiveViewModel, "First View Model expected to be the active View Model.");

            MainViewModel.AddActiveViewModel(fake2);

            var fake2Contained = MainViewModel.ActiveViewModels.Contains(fake2);
            var fake2Found = MainViewModel.ActiveViewModels.FirstOrDefault(vm => vm.Id == fake2.Id);

            Assert.AreSame(fake1, MainViewModel.ActiveViewModel, "First View Model expected to be the active View Model.");
            Assert.IsTrue(fake2Contained, "Second View Model is expected to be in active collection.");
            Assert.AreSame(fake2, fake2Found, "Second View Model object expected to be in active collection.");

            MainViewModel.RemoveActiveViewModel(fake2);
            fake2Contained = MainViewModel.ActiveViewModels.Contains(fake2);
            Assert.IsFalse(fake2Contained, "Second View Model is expected to not be in active collection.");
            Assert.AreSame(fake1, MainViewModel.ActiveViewModel, "First View Model expected to be the active View Model.");

            MainViewModel.RemoveActiveViewModel(fake1);
            fake1Contained = MainViewModel.ActiveViewModels.Contains(fake1);
            Assert.IsFalse(fake1Contained, "First View Model is expected to not be in active collection.");
            Assert.AreNotSame(fake1, MainViewModel.ActiveViewModel, "First View Model expected to not be the active View Model.");
        }

        [TestMethod]
        public void MainViewModel_AddRemoveActiveViewModelCommand()
        {

            var fake1 = new FakeVm1();

            MainViewModel.AddActiveViewModelCommand.Execute(fake1);

            var fake1Contained = MainViewModel.ActiveViewModels.Contains(fake1);
            var fake1Found = MainViewModel.ActiveViewModels.FirstOrDefault(vm => vm.Id == fake1.Id);

            Assert.IsTrue(fake1Contained, "View Model is expected to be in active collection.");
            Assert.AreSame(fake1, fake1Found, "View Model object expected to be in active collection.");
            Assert.AreSame(fake1, MainViewModel.ActiveViewModel, "View Model expected to be the active View Model.");

            MainViewModel.RemoveActiveViewModelCommand.Execute(fake1);
            fake1Contained = MainViewModel.ActiveViewModels.Contains(fake1);
            Assert.IsFalse(fake1Contained, "View Model is expected to not be in active collection.");
            Assert.AreNotSame(fake1, MainViewModel.ActiveViewModel, "View Model expected to not be the active View Model.");
        }

        [TestMethod, Timeout(1000)]
        public void MainViewModel_PropertyChanged()
        {
            _WaitEvent.Reset();

            MainViewModel.PropertyChanged += (sender, e) =>
            {
                Assert.AreEqual(nameof(MainViewModel.ActiveViewModel), e.PropertyName,
                    "Expected changed property event for ActiveViewModel.");

                _WaitEvent.Set();
            };

            MainViewModel.ActiveViewModel = new FakeVm2();

            _WaitEvent.WaitOne();
        }

        [TestMethod, Timeout(1000)]
        public void MainViewModel_CollectionChanged()
        {
            _WaitEvent.Reset();

            MainViewModel.ActiveViewModels.CollectionChanged += (sender, e) => _WaitEvent.Set();

            MainViewModel.AddActiveViewModel(new FakeVm2());

            _WaitEvent.WaitOne();
        }
    }
}
